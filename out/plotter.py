#!/usr/bin/python3

from enum import Enum
from math import sqrt
from numpy import ndarray, array, arange, resize, reshape
from matplotlib.backend_bases import NavigationToolbar2
import matplotlib.pyplot as pyplot
from matplotlib.widgets import Button, RadioButtons


class GraphType (Enum):
    agent_bargraph = 1
    agent_all_bargraph = 2
    agent_distribution = 3
    agent_all_distribution = 4
    inter_group = 5
    intra_group = 6

PARAMETERS = {}
RADIO_BUTTONS_GRAPHTYPE =\
    { \
        "bargraph":GraphType.agent_bargraph, \
        "bargraph (all)":GraphType.agent_all_bargraph, \
        "histogram":GraphType.agent_distribution, \
        "histogram (all)":GraphType.agent_all_distribution, \
        "intra_group_cooperation ":GraphType.intra_group, \
        "inter_group_cooperation ":GraphType.inter_group \
    }

class Plotter:
    def __init__(self, filename):
        self._filename = filename
        self._pre_gen_stats = {}
        self._post_gen_stats = {}
        self._diff_gen_stats = {}
        self._current_gen = 2
        self._current_group = 0
        self._current_graphtype = GraphType.agent_bargraph
        self.rbtoggle_graph = None
        f, self._axis = pyplot.subplots ()
        self.load_parameters ()
        self.__read_file()

    def load_parameters (self) :
        try:
            params_file = open ("../params.ini")
            for line in params_file:
                line = line.split ()
                if ( len ( line ) > 1) :
                    data = []
                    for x in line[1:]:
                        try:
                            data+=[eval(x)]
                        except NameError:
                            data+= [x]

                    if ( len ( data ) == 1):
                        data = data[0]
                    PARAMETERS[line[0]] = data
        except:
            raise

    def read_file(self):
        #print ( "Reading file '{0}'".format (self._filename), end = " ... ")
        ## clear existing stats
        self._pre_gen_stats = {}
        self._post_gen_stats = {}
        pre_gen = {}
        ## extract stats from file
        try:
            f_out = open( self._filename )
            cur_gen = 0
            n_line = 0
            for line in f_out:
                values = [ eval ( i ) for i in line.split () ]
                if len (values) == 2:
                    cur_gen = values[0]
                    self._pre_gen_stats[cur_gen] = {"iNumResources" : values[1] }
                    self._post_gen_stats[cur_gen] = {"iNumResources" : values[1] }
                    pre_gen.clear ()
                    continue
                else:
                    try :
                        pre_gen[values[0]]
                        try:
                            self._post_gen_stats[cur_gen][values[0]] = { \
                                    "red" : values[1], \
                                    "green": values[2], \
                                    "blue": values[3], \
                                    "energy": values[4], \
                                    "collected": values[5], \
                                    "coop" : values[6], \
                                    "agents" : array ( values[7:] ) }
                        except:
                           pass
                    except KeyError:
                        pre_gen[values[0]] = True
                        try:
                            self._pre_gen_stats[cur_gen][values[0]] = { \
                                    "red" : values[1], \
                                    "green": values[2], \
                                    "blue": values[3], \
                                    "energy": values[4], \
                                    "collected": values[5], \
                                    "coop" : values[6], \
                                    "agents" : array ( values[7:] ) }
                        except:
                           pass
                n_line += 1
            #print ("DONE!")
        except:
            #print ( "INCOMPLETE!")
            raise

    def create_inter_group_linegraph( self, group=-1 ):
        print ( "Plotting Inter-group Cooperation in all generations ", end=" ... ")

        if type ( group ) != int :
            print ( "INCOMPLETE! " )
            return
        if self._post_gen_stats == None :
            print ( "INCOMPLETE!" )
            return

        # extract cooperation values from the dictionary
        coop = {}
        for gen_idx in self._post_gen_stats.keys() :
            for group_idx in self._post_gen_stats[ gen_idx ].keys() :
                if group_idx == "iNumResources":
                    continue
                try:
                    coop[ group_idx ].append ( tuple( [self._post_gen_stats[ gen_idx ][ group_idx ][ "collected" ], gen_idx ] ) )
                except KeyError:
                    coop[ group_idx ] = [ tuple( [self._post_gen_stats[ gen_idx ][ group_idx ][ "collected" ], gen_idx ] ) ]

        self._axis.set_title( "Inter-group cooperation" )
        # plot them
        for group_idx in coop.keys():
            val = coop[ group_idx ]
            data_values = list ( zip ( *val ) )
            gen = data_values[ 1 ][ 0 ]
            group_colour = (\
                    self._post_gen_stats [gen][group_idx]["red"]/255.0, \
                    self._post_gen_stats[gen][group_idx]["green"]/255.0, \
                    self._post_gen_stats[gen][group_idx]["blue"]/255.0 )
            size = self._pre_gen_stats[ 0 ][ group_idx ][ "agents" ].size
            self._axis.plot ( data_values[ 1 ], data_values[ 0 ], color=group_colour, label=("Group {0}: {1} initial agents".format (group_idx,  size )) )
        self._axis.set_xlabel( "Generations" )
        self._axis.set_ylabel( "Unique resources Collected" )
        self._axis.legend( loc="upper right" )
        print ( "DONE!" )

    def create_intra_group_linegraph( self, group=-1 ):
        print ( "Plotting Inter-group Cooperation in all generations ", end=" ... ")

        if type ( group ) != int :
            print ( "INCOMPLETE! " )
            return
        if self._post_gen_stats == None :
            print ( "INCOMPLETE!" )
            return

        # extract cooperation values from the dictionary
        coop = {}
        for gen_idx in self._post_gen_stats.keys() :
            for group_idx in self._post_gen_stats[ gen_idx ].keys() :
                if group_idx == "iNumResources":
                    continue
                cooperation_idx = self._post_gen_stats[ gen_idx ][ group_idx ][ "coop" ] / ((float)(self._pre_gen_stats[ gen_idx ][ group_idx ][ "agents" ].size))
                try:
                    coop[ group_idx ].append ( tuple( [cooperation_idx, gen_idx ] ) )
                except KeyError:
                    coop[ group_idx ] = [ tuple( [cooperation_idx, gen_idx ] ) ]

        self._axis.set_title( "Intra-group cooperation" )
        # plot them
        for group_idx in coop.keys():
            val = coop[ group_idx ]
            data_values = list ( zip ( *val ) )
            gen = data_values[ 1 ][ 0 ]
            group_colour = (\
                    self._post_gen_stats [gen][group_idx]["red"]/255.0, \
                    self._post_gen_stats[gen][group_idx]["green"]/255.0, \
                    self._post_gen_stats[gen][group_idx]["blue"]/255.0 )
            size = self._pre_gen_stats[ 0 ][ group_idx ][ "agents" ].size
            self._axis.plot ( data_values[ 1 ], data_values[ 0 ], color=group_colour, label=("Group {0}: {1} initial agents".format (group_idx,  size )) )
        self._axis.set_xlabel( "Generations" )
        self._axis.set_ylabel( "Ratio of Cooperative Agents" )
        self._axis.legend( loc="upper right" )
        print ( "DONE!" )
        
    def create_agent_energy_histogram ( self , gen=0, group=-1, num_bins=10 ):
        gen = gen % ( len (self._post_gen_stats.keys () ) )
        print ("Plotting Agent Energy Distribution per Group : generation {0} ".format (gen) , end = " ... " )

        if type ( gen ) != int :
            print ( "INCOMPLETE!" )
            return None
        if self._pre_gen_stats == None or self._post_gen_stats == None:
            print ( "INCOMPLETE!" )
            return None
        try:
            self._post_gen_stats[gen]
        except KeyError:
            print ( "INCOMPLETE!" )
            return None

        if group != -1:
            group = group % ( len (self._post_gen_stats[gen].keys () ) - 1)
        idx = arange (0, 30, 1)
        rect = []
        width = 1.0/( len (self._post_gen_stats[gen].keys() ) - 1)

        self._axis.set_title( ("Generation {0}: Distribution of Agent Energies".format (gen)) )

        values = []
        colours = []
        labels = []
        for groupIdx in self._post_gen_stats[gen].keys ():
            if (groupIdx == "iNumResources"):
                continue
            if (groupIdx != group and group != -1 ):
                continue

            group_colour = (\
                    (self._post_gen_stats [gen][groupIdx]["red"])/255.0, \
                    (self._post_gen_stats[gen][groupIdx]["green"])/255.0, \
                    (self._post_gen_stats[gen][groupIdx]["blue"])/255.0 )

            if len (self._post_gen_stats[gen][groupIdx]["agents"]) == 0:
                continue
            values.append ( self._post_gen_stats[gen][groupIdx]["agents"] )
            colours.append(group_colour )
            labels.append (("Group {0}".format(groupIdx)))

        if len(values ) == len(colours ) == len ( labels ) != 0:
            self._axis.hist(values, bins=num_bins, alpha=0.25, \
                    color=colours, histtype="bar", normed=False, \
                    label=labels, cumulative=False )

        self._axis.legend( loc="upper right" )
        print ( "DONE!" )



    def create_agent_energy_bargraph ( self , gen=0, num_bins=10, group=-1 ):
        gen = gen % ( len (self._post_gen_stats.keys () ) )
        print ("Plotting Agent Energy per Group : generation {0} ".format (gen) , end = " ... " )

        if type ( gen ) != int :
            print ( "INCOMPLETE! 1" )
            return None
        if self._post_gen_stats == None or self._pre_gen_stats == None :
            print ( "INCOMPLETE! 2" )
            return None
        try:
            self._post_gen_stats[gen]
        except KeyError:
            print ( "INCOMPLETE! 3" )
            print ( gen )
            return None

        if group != -1:
            group = group % ( len (self._post_gen_stats[gen].keys () ) - 1 )
        idx = arange (0, PARAMETERS["iNumSweepers"], 1)
        rect = []
        width = 1.0/( len (self._post_gen_stats[gen].keys() ) - 1)

        self._axis.set_title( ("Generation {0}: Agent Energies per group".format (gen)) )

        for groupIdx in self._post_gen_stats[gen].keys ():
            if ( groupIdx == "iNumResources" ):
                continue
            if (groupIdx != group and group != -1 ):
                continue

            group_colour = (\
                    self._post_gen_stats [gen][groupIdx]["red"]/255.0, \
                    self._post_gen_stats[gen][groupIdx]["green"]/255.0, \
                    self._post_gen_stats[gen][groupIdx]["blue"]/255.0 )

            self._post_gen_stats[gen][groupIdx]["agents"].resize (PARAMETERS["iNumSweepers"])
            self._axis.bar (idx +groupIdx * width, self._post_gen_stats[gen][groupIdx]["agents"], \
                    width, align='center', color=group_colour, label=("Group {0}".format(groupIdx)), \
                    alpha=0.45 )
            self._pre_gen_stats[gen][groupIdx]["agents"].resize (PARAMETERS["iNumSweepers"])
            self._axis.bar (idx +groupIdx * width, self._pre_gen_stats[gen][groupIdx]["agents"], \
                    width, fill=False, align='center', color= group_colour, label=("Group {0} E_0".format(groupIdx)), \
                    alpha=1.0 )

        self._axis.set_xlabel( "Agents" )
        self._axis.set_ylabel( "Energy" )
        self._axis.legend( loc="upper right" )
        print ( "DONE!" )

    def display_graph ( self ) :
        #pyplot.cla ()
        self._axis.clear ()
        if self._current_graphtype == GraphType.agent_bargraph:
            self.create_agent_energy_bargraph (gen=self._current_gen, group=self._current_group)
        elif self._current_graphtype == GraphType.agent_all_bargraph:
            self.create_agent_energy_bargraph (gen=self._current_gen)
        elif self._current_graphtype == GraphType.agent_distribution:
            self.create_agent_energy_histogram(gen=self._current_gen, group=self._current_group)
        elif self._current_graphtype == GraphType.agent_all_distribution:
            self.create_agent_energy_histogram(gen=self._current_gen)
        elif self._current_graphtype == GraphType.inter_group:
            self.create_inter_group_linegraph( group=-1 )
        elif self._current_graphtype == GraphType.intra_group:
            self.create_intra_group_linegraph( group=-1 )

    __read_file = read_file

    ## Handle plotter events
    def next_generation (self, event ) :
        self._current_gen += 1
        self.display_graph ()
        pyplot.draw ()

    def previous_generation (self, event ):
        if ( self._current_gen > 0) :
            self._current_gen -= 1
        self.display_graph ()
        pyplot.draw ()

    def next_group (self, event ):
        self._current_group += 1
        self.display_graph ()
        pyplot.draw ()

    def previous_group ( self , event ):
        if ( self._current_group > 0) :
            self._current_group -= 1
        self.display_graph ()
        pyplot.draw ()

    def toggle_graph_type ( self, label ) :
        if RADIO_BUTTONS_GRAPHTYPE[label] == self._current_graphtype:
            return
        self._current_graphtype = RADIO_BUTTONS_GRAPHTYPE[label]
        self.display_graph ()
        pyplot.draw ()

    def init_buttons (self):
        axprev_gen = pyplot.axes ( [0.7, 0.05, 0.1, 0.075] )
        axnext_gen = pyplot.axes ( [0.81, 0.05, 0.1, 0.075] )
        axprev_group = pyplot.axes ( [0.47, 0.05, 0.1, 0.075] )
        axnext_group = pyplot.axes ( [0.59, 0.05, 0.1, 0.075] )

        bnext_gen = Button (axnext_gen, ">|")
        bprev_gen = Button (axprev_gen, "|<")
        bnext_group = Button (axnext_group, ">")
        bprev_group = Button (axprev_group, "<")
        labels = tuple ( RADIO_BUTTONS_GRAPHTYPE.keys() )
        self.rbtoggle_graph = RadioButtons ( pyplot.axes([0.05, 0.0, 0.15, 0.15]) , labels )
        # self._current_graphtype = RADIO_BUTTONS_GRAPHTYPE[labels[self.rbtoggle_graph.active]]

        self.rbtoggle_graph.on_clicked (self.toggle_graph_type )
        bnext_gen.on_clicked (self.next_generation )
        bprev_gen.on_clicked (self.previous_generation)
        bnext_group.on_clicked (self.next_group )
        bprev_group.on_clicked (self.previous_group )
        pyplot.subplots_adjust ( bottom=0.2 )


def main ():
    ## test the class

    alsb_plot = Plotter ( "results.out" )
    alsb_plot.init_buttons ()
    alsb_plot.display_graph ()
    pyplot.show ()

if __name__ == "__main__":
    main ()

#!/usr/bin/python3

# ALSB plotting
# author: poseidopn
# date: 15 June 2015
from math import sqrt
from numpy import ndarray, array, arange, resize, reshape
from matplotlib.backend_bases import NavigationToolbar2
import matplotlib.pyplot as pyplot
from sys import setrecursionlimit
setrecursionlimit (32000)
# from time import sleep

current_generation = 0
stats = {}

def read_file (filename):
    global stats
    try:
        out = open (filename)
        cur_gen = 0
        for line in out:
            values = [eval (i) for i in line.split ()]
            if len (values) == 1:
                cur_gen = values[0]
                continue
            else:
                try:
                    stats[cur_gen][values[0]] = ((values[1], values[2], values[3]), values[4], array(values[5:]))
                except:
                    stats[cur_gen] = {values[0]: ((values[1], values[2], values[3]), values[4], array(values[5:]))}
        out.close ()
    except:
        print ("try:9 exception found")
        return None

def plot_generation (table, gen=6, b=10):
    histogram = (gen > len (stats.keys()) -1 )
    gen = gen  % (len (stats.keys()) -1);
    if type (gen) != int:
        return None
    if table == None:
        return None
    try:
        table[gen]
    except KeyError:
        return None

    idx = arange (0, 30, 1)

##    pyplot.ylabel("Agent Energy")
##    pyplot.xlabel("Agents")
    rect = []
    width = 0.35

    ## sleep (500)
    if ( not histogram ):
        pyplot.suptitle("Generation {0}: agent energies per group".format (gen) )
    else:
        pyplot.suptitle("Generation {0}: histograms of agent energies per group".format (gen) )
    for k in table[gen].keys():
        group_colour = (table[gen][k][0][0]/255.0, table[gen][k][0][1]/255.0, table[gen][k][0][2]/255.0)

        if ( not histogram ):
            table[gen][k][2].resize ( (30) )
            pyplot.bar(idx + k * width,table[gen][k][2], width,   color=group_colour )
        else:
            if (len (table[gen][k][2])== 0 ):
                continue
            pyplot.hist(table[gen][k][2], bins=b, alpha=0.5,color=group_colour, label="Group {0}".format(k) , cumulative=True)

    pyplot.legend(loc='upper right')
    pyplot.show()

forward = NavigationToolbar2.forward

def new_forward(self, *args, **kwargs):
    global current_generation
    global stats
    if (current_generation < 2 * (len(stats.keys()) - 1)):
        current_generation += 1
        pyplot.clf()
        #pyplot.cla()

        pyplot.close ()
        stats = {}
        read_file ("results.out")
        plot_generation ( stats , current_generation )
    forward(self, *args, **kwargs)

NavigationToolbar2.forward = new_forward

back = NavigationToolbar2.back

def new_back(self, *args, **kwargs):
    global current_generation
    global stats
    if (current_generation > 0):
        current_generation -= 1
        pyplot.clf()
        #pyplot.cla()

        pyplot.close ()
        stats = {}
        read_file ("results.out")
        plot_generation ( stats , current_generation )
    back(self, *args, **kwargs)

NavigationToolbar2.back = new_back

def main ():
    read_file ("results.out")
    # print (stats)
    plot_generation (stats, current_generation)

if __name__ == "__main__":
    main ()

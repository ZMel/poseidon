### ALSB PLOT

#### Requirements

You need to install [Python 3.4][],  [matplotlib][] and [numpy][].

To install the two python packages, download them from the following sites

1. [Python 3.4 download][]
3. [numpy 1.9.2 download][]

Assuming you have used the default configuration when instaling [Python 3.4][],
you can install matplotlib run the following command on ``Command Prompt`` or
``PowerShell``

    pip install matplotlib

If you need to install ``pip``, download and run the [get_pip.py][] file.


[get_pip.py]: https://bootstrap.pypa.io/get-pip.py
[matplotlib]: http://matplotlib.org
[numpy]: http://numpy.org
[numpy 1.9.2 download]: http://sourceforge.net/projects/numpy/files/NumPy/1.9.2/numpy-1.9.2-win32-superpack-python3.4.exe/download
[matplotlib 1.4.3 download]: http://matplotlib.org/downloads.html
[Python 3.4 download]: https://www.python.org/ftp/python/3.4.3/python-3.4.3.msi
[Python 3.4]: https://www.python.org/download/releases/3.4.3/

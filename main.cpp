#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <fcntl.h>
#ifdef _WINDOWS
#include <direct.h>
#include <io.h>
#endif

#include "utils.h"
#include "CController.h"
#include "CTimer.h"
#include "Icon.h"
#include "CParams.h"

// Global variables
char * szApplicationName = "Agent Cooporation Simulation";
char * szWindowClassName = "AgentCooperation";

//The controller class for this simulation
CController * g_pController = NULL;

//create an instance of the parameter class.
CParams   g_Params;

// run counter
int run = 0;
int finalRun;

/**
* Cleans up any memory issues when the application exits
*/
void Cleanup()
{
	if (g_pController)

		delete g_pController;
}

#ifdef _WINDOWS

/**
* Opens the window for the environment
*/
LRESULT CALLBACK WindowProc(HWND hwnd,
	UINT msg,
	WPARAM wparam,
	LPARAM lparam)
{
	// Dimensions of the client window area
	static int cxClient, cyClient;

	// Stores the back buffer
	static HDC hdcBackBuffer;
	static HBITMAP	hBitmap;
	static HBITMAP	hOldBitmap;


	switch (msg)
	{
	case WM_CREATE:
	{
					  // Seed the random number generator
					  srand((unsigned)time(NULL));

					  // Get the size of the client window
					  RECT rect;
					  GetClientRect(hwnd, &rect);

					  cxClient = rect.right;
					  cyClient = rect.bottom;

					  // Setup the controller
					  g_pController = new CController(hwnd, true, 0, 0);

					  // Create a surface for us to render to(backbuffer)
					  hdcBackBuffer = CreateCompatibleDC(NULL);

					  HDC hdc = GetDC(hwnd);

					  hBitmap = CreateCompatibleBitmap(hdc, cxClient, cyClient);
					  ReleaseDC(hwnd, hdc);

					  hOldBitmap = (HBITMAP)SelectObject(hdcBackBuffer, hBitmap);
	}
		break;

		// Key press messages
	case WM_KEYUP:
	{
					 switch (wparam)
					 {
					 case VK_ESCAPE:
					 {
									   PostQuitMessage(0);
					 }
						 break;
					 case 'F':
					 {
								 g_pController->FastRenderToggle();
					 }
						 break;
						 // Reset the simulation
					 case 'R':
					 {
								 if (g_pController)
								 {
									 delete g_pController;
								 }

								 // Setup new controller
								 g_pController = new CController(hwnd, true, 1, 0);
					 }
						 break;
					 }//end WM_KEYUP switch
	}
		break;

		// Has the user resized the client area
	case WM_SIZE:
	{
					cxClient = LOWORD(lparam);
					cyClient = HIWORD(lparam);

					// Resize the backbuffer accordingly
					SelectObject(hdcBackBuffer, hOldBitmap);

					HDC hdc = GetDC(hwnd);

					hBitmap = CreateCompatibleBitmap(hdc, cxClient, cyClient);
					ReleaseDC(hwnd, hdc);

					hOldBitmap = (HBITMAP)SelectObject(hdcBackBuffer, hBitmap);
	}

		break;

	case WM_PAINT:
	{
					 PAINTSTRUCT ps;

					 BeginPaint(hwnd, &ps);

					 // Fill backbuffer with white
					 BitBlt(hdcBackBuffer,
						 0,
						 0,
						 cxClient,
						 cyClient,
						 NULL,
						 NULL,
						 NULL,
						 WHITENESS);

					 // Render the mines and sweepers
					 g_pController->Render(hdcBackBuffer);

					 // Now blit backbuffer to front
					 BitBlt(ps.hdc, 0, 0, cxClient, cyClient, hdcBackBuffer, 0, 0, SRCCOPY);

					 EndPaint(hwnd, &ps);
	}

		break;

	case WM_DESTROY:
	{
					   SelectObject(hdcBackBuffer, hOldBitmap);

					   // Clean up backbuffer objects
					   DeleteDC(hdcBackBuffer);
					   DeleteObject(hBitmap);

					   // Kill the application, this sends a WM_QUIT message 
					   PostQuitMessage(0);
	}
		break;
	default:break;
	}

	// Default msg handler 
	return (DefWindowProc(hwnd, msg, wparam, lparam));

}

#endif

#ifdef _WINDOWS
//-----------------------------------SetStdOutToNewConsole---------------------------
//	This opens up a console to display standard out
//-----------------------------------------------------------------------------------
void SetStdOutToNewConsole()
{
	int hConHandle;
	long lStdHandle;
	CONSOLE_SCREEN_BUFFER_INFO coninfo;
	FILE *fp;

	// allocate a console for this app
	AllocConsole();

	// set the screen buffer to be big enough to let us scroll text
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &coninfo);
	coninfo.dwSize.Y = 500;
	SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), coninfo.dwSize);

	// redirect unbuffered STDOUT to the console
	lStdHandle = (long)GetStdHandle(STD_OUTPUT_HANDLE);
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
	fp = _fdopen(hConHandle, "w");
	*stdout = *fp;
	setvbuf(stdout, NULL, _IONBF, 0);

	// redirect unbuffered STDIN to the console
	lStdHandle = (long)GetStdHandle(STD_INPUT_HANDLE);
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
	fp = _fdopen(hConHandle, "r");
	*stdin = *fp;
	setvbuf(stdin, NULL, _IONBF, 0);

	// redirect unbuffered STDERR to the console
	lStdHandle = (long)GetStdHandle(STD_ERROR_HANDLE);
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
	fp = _fdopen(hConHandle, "w");
	*stderr = *fp;
	setvbuf(stderr, NULL, _IONBF, 0);

	// make cout, wcout, cin, wcin, wcerr, cerr, wclog and clog
	// point to console as well
	std::cout.sync_with_stdio();
	std::cin.sync_with_stdio();
	std::cerr.sync_with_stdio();
	std::clog.sync_with_stdio();
}

/**
* Entry point for the main application
*/
int WINAPI WinMain(HINSTANCE hinstance,
	HINSTANCE hprevinstance,
	LPSTR lpcmdline,
	int ncmdshow)
{
	SetStdOutToNewConsole();
#endif
#ifndef _WINDOWS
	int main()
	{
std::cout << "-----------------------------------------------------------------------" << std::endl;

		// clear contents of the output file
		ofstream ofs(CParams::sOutFilename, ios::out | ios::trunc);
		ofs.close();

		std::cout << std::endl << "Agent Cooperation Simulation:" << std::endl;
		std::cout << "-----------------------------------------------------------------------" << std::endl << std::endl;

		std::cout << "Enter desired number of runs:" << std::endl;
		std::cin >> finalRun;

		g_pController = new CController(NULL, true, 0, 0);
#endif
#ifdef _WINDOWS
		WNDCLASSEX winclass;
		HWND	   hwnd;
		MSG		   msg;

		// Fill in the window class stucture
		winclass.cbSize = sizeof(WNDCLASSEX);
		winclass.style = CS_HREDRAW | CS_VREDRAW;
		winclass.lpfnWndProc = WindowProc;
		winclass.cbClsExtra = 0;
		winclass.cbWndExtra = 0;
		winclass.hInstance = hinstance;
		winclass.hIcon = LoadIcon(hinstance, MAKEINTRESOURCE(IDI_ICON1));
		winclass.hCursor = LoadCursor(NULL, IDC_ARROW);
		winclass.hbrBackground = NULL;
		winclass.lpszMenuName = NULL;
		winclass.lpszClassName = szWindowClassName;
		winclass.hIconSm = LoadIcon(hinstance, MAKEINTRESOURCE(IDI_ICON1));


	
std::cout << "-----------------------------------------------------------------------" << std::endl;

		// clear contents of the output file
		ofstream ofs(CParams::sOutFilename, ios::out | ios::trunc);
		ofs.close();

		std::cout << std::endl << "Agent Cooperation Simulation:" << std::endl;
		std::cout << "-----------------------------------------------------------------------" << std::endl << std::endl;

		std::cout << "Enter desired number of runs:" << std::endl;
		std::cin >> finalRun;
		

		// Register the window class
		if (!RegisterClassEx(&winclass))
		{
			MessageBox(NULL, "Error Registering Class!", "Error", 0);
			return 0;
		}

		// Create the window that cannot be resized
		if (!(hwnd = CreateWindowEx(NULL,
			szWindowClassName,
			szApplicationName,
			WS_OVERLAPPED | WS_VISIBLE | WS_CAPTION | WS_SYSMENU,
			GetSystemMetrics(SM_CXSCREEN) / 2 - CParams::WindowWidth / 2,
			GetSystemMetrics(SM_CYSCREEN) / 2 - CParams::WindowHeight / 2,
			CParams::WindowWidth,
			CParams::WindowHeight,
			NULL,
			NULL,
			hinstance,
			NULL)))
		{
			MessageBox(NULL, "Error Creating Window!", "Error", 0);
			return 0;
		}

		// Show the window
		ShowWindow(hwnd, SW_SHOWDEFAULT);
		UpdateWindow(hwnd);

#endif
		// Create a timer
		CTimer timer(CParams::iFramesPerSecond);

		// Start the timer
		timer.Start();

		// Enter the message loop
		bool bDone = false;

		while (!bDone)
		{
#ifdef _WINDOWS
			while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				if (msg.message == WM_QUIT)
				{
					// Stop loop if it's a quit message
					bDone = TRUE;
				}

				else
				{
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
			}

			if (timer.ReadyForNextFrame() || g_pController->FastRender())
			{
#endif			
				if (!g_pController->Update())
				{
					if (!g_pController->end)
					{
						int modeChoice = g_pController->modeChoice;
						int paramChoice = g_pController->paramChoice;
						
						#ifdef _WINDOWS
						g_pController = new CController(hwnd, false, modeChoice, paramChoice);
						#else
						g_pController = new CController(NULL, false, modeChoice, paramChoice);
						#endif

						g_pController->FastRenderToggle();
						run++;
						if (run >= finalRun-1)
							g_pController->end = true;
					}
					else
					{
						// End the application if problem found
						bDone = true;
					}	
				}

				// Call WM_PAINT to render our scene
#ifdef _WINDOWS
				InvalidateRect(hwnd, NULL, TRUE);
				UpdateWindow(hwnd);
			}
#endif


		}
		// Clean up everything and exit the app
		Cleanup();
#ifdef _WINDOWS
		UnregisterClass(szWindowClassName, winclass.hInstance);
#endif
		return 0;
	}

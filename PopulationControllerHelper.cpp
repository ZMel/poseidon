#include "CController.h"

void CController::populationExperimentSetup()
{
	for (int i = 0; i < (int)CParams::numPatches; ++i)
	{
		patchesVector.push_back(ResourcePatch(CParams::defaultResourcePositions[i], widthOfPatch));
	}
	generateResources();
	
	generateRedAndBlue( CParams::iNumAgents , 1);
}
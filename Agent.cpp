#include "Agent.h"


/**
 *  Creates a new agent with default values, random position and rotation
 */
Agent::Agent() :
    m_vPosition(),
	m_vLookAt(),
    m_dRotation(Utilities::getDouble()*CParams::dTwoPi),
	m_dSpeed(0),
	energy(0),
	startingEnergy(0),
	energyLeftForGroup(0),
	m_dScale(CParams::dSweeperScale),
	energyLost(0),
	vClosestObject(),
	closestResourceIndex(0),
	numberResourcesGathered(0),
	groupNumber(-1),
	parentGroupId(-1),
	parent2GroupId(-1)
{
    brain = CNeuralNet();
	m_vPosition = SVector2D((Utilities::getDouble() * CParams::WindowWidth),
		(Utilities::getDouble() * CParams::WindowHeight));
}

Agent::Agent(SVector2D position_, int groupNumber_, double startingFitness, CNeuralNet & brain_) :
    m_dRotation(Utilities::getDouble()*CParams::dTwoPi),
    m_vPosition(position_),
    energy(startingFitness),
    startingEnergy(startingFitness),
	energyLeftForGroup(0),
    m_dScale(CParams::dSweeperScale),
	energyLost(0),
	vClosestObject(),
	closestResourceIndex(0),
	numberResourcesGathered(0),
	groupNumber(-1),
	parentGroupId(-1),
	parent2GroupId(-1)
{
	brain = CNeuralNet( brain_ );
}

/**
*  Requires all information about an agent to perfectly reconstruct the simulation from a save point.
*/
Agent::Agent(double _m_dRotation, double _m_dSpeed, double _energy, double _startingEnergy, double _energyLeftForGroup, double _m_dScale, double _energyLost,
	int _closestResourceIndex, int _numberResourcesGathered, int _groupNumber, int _parentGroupId, int _parent2GroupId,
	SVector2D _m_vPosition, SVector2D _m_vLookAt, SVector2D _vClosestObject, CNeuralNet *& brain_) :
	m_vPosition(_m_vPosition),
	m_vLookAt(_m_vLookAt),
	m_dRotation(_m_dRotation),
	m_dSpeed(_m_dSpeed),
	energy(_energy),
	startingEnergy(_startingEnergy),
	energyLeftForGroup(_energyLeftForGroup),
	m_dScale(_m_dScale),
	energyLost(_energyLost),
	vClosestObject(_vClosestObject),
	closestResourceIndex(_closestResourceIndex),
	numberResourcesGathered(_numberResourcesGathered),
	groupNumber(_groupNumber),
	parentGroupId(_parentGroupId),
	parent2GroupId(_parent2GroupId)
{
	if (brain_)
	{
		this->brain = CNeuralNet(*brain_);
		delete brain_;
		brain_ = nullptr;
	}
	else
	{
		this->brain = CNeuralNet();
	}
}

Agent::Agent(const Agent & other) :
	m_vPosition(other.m_vPosition),
	m_vLookAt(other.m_vLookAt),
	m_dRotation(other.m_dRotation),
	m_dSpeed(other.m_dSpeed),
	energy(other.energy),
	startingEnergy(other.startingEnergy),
	energyLeftForGroup(other.energyLeftForGroup),
	m_dScale(other.m_dScale),
	energyLost(other.energyLost),
	vClosestObject(other.vClosestObject),
	closestResourceIndex(other.closestResourceIndex),
	numberResourcesGathered(other.numberResourcesGathered),
	groupNumber(other.groupNumber),
	parentGroupId(other.parentGroupId),
	parent2GroupId(other.parent2GroupId)
{
	this->brain = CNeuralNet(other.brain);
}

Agent::Agent(Agent && other) :
	m_vPosition(other.m_vPosition),
	m_vLookAt(other.m_vLookAt),
	m_dRotation(other.m_dRotation),
	m_dSpeed(other.m_dSpeed),
	energy(other.energy),
	startingEnergy(other.startingEnergy),
	energyLeftForGroup(other.energyLeftForGroup),
	m_dScale(other.m_dScale),
	energyLost(other.energyLost),
	vClosestObject(other.vClosestObject),
	closestResourceIndex(other.closestResourceIndex),
	numberResourcesGathered(other.numberResourcesGathered),
	groupNumber(other.groupNumber),
	parentGroupId(other.parentGroupId),
	parent2GroupId(other.parent2GroupId)
{
	this->brain = CNeuralNet ( other.brain ); 

	other.m_dRotation = 0.0;
	other.m_dSpeed = 0.0;
	other.energy = 0.0;
	other.energyLeftForGroup = 0.0;
	other.m_dScale = 0.0;
	other.energyLost = 0.0;
}

Agent & Agent::operator=(const Agent & other)
{
	if ( this != &other )
	{
		m_vPosition = other.m_vPosition;
		m_vLookAt = other.m_vLookAt;
		m_dRotation = other.m_dRotation;
		m_dSpeed = other.m_dSpeed;
		energy = other.energy;
		const_cast<double&>(startingEnergy) = other.startingEnergy;
		energyLeftForGroup = other.energyLeftForGroup;
		m_dScale = other.m_dScale;
		energyLost = other.energyLost;
		vClosestObject = other.vClosestObject;
		closestResourceIndex = other.closestResourceIndex;
		numberResourcesGathered = other.numberResourcesGathered;
		groupNumber = other.groupNumber;
		parentGroupId = other.parentGroupId;
		parent2GroupId = other.parent2GroupId;
		brain = other.brain;
	}
	return *this;
}
Agent & Agent::operator=(Agent && other)
{
	if ( this != &other )
	{
		m_vPosition = other.m_vPosition;
		m_vLookAt = other.m_vLookAt;
		m_dRotation = other.m_dRotation;
		m_dSpeed = other.m_dSpeed;
		energy = other.energy;
		const_cast<double&>(startingEnergy) = other.startingEnergy;
		energyLeftForGroup = other.energyLeftForGroup;
		m_dScale = other.m_dScale;
		energyLost = other.energyLost;
		vClosestObject = other.vClosestObject;
		closestResourceIndex = other.closestResourceIndex;
		numberResourcesGathered = other.numberResourcesGathered;
		groupNumber = other.groupNumber;
		parentGroupId = other.parentGroupId;
		parent2GroupId = other.parent2GroupId;

		brain = other.brain; 

		other.m_dRotation = 0.0;
		other.m_dSpeed = 0.0;
		other.energy = 0.0;
		other.energyLeftForGroup = 0.0;
		other.m_dScale = 0.0;
		other.energyLost = 0.0;
	}
	return *this;
}

Agent::~Agent () 
{

}

void Agent::Reset()
{
	m_vPosition = SVector2D((Utilities::getDouble() * CParams::WindowWidth), (Utilities::getDouble() * CParams::WindowHeight));
	m_dRotation = Utilities::getDouble()*CParams::dTwoPi;
	return;
}

void Agent::WorldTransform(vector<SPoint> &agent)
{
	C2DMatrix matTransform;
	matTransform.Scale(m_dScale, m_dScale);
	matTransform.Rotate(m_dRotation - CParams::dHalfPi);
	matTransform.Translate(m_vPosition.x, m_vPosition.y);
	matTransform.TransformSPoints(agent);
}

bool Agent::Update()
{
	m_vLookAt.x = cos(m_dRotation);
	m_vLookAt.y = sin(m_dRotation);

	m_vPosition += (m_vLookAt * m_dSpeed);

	// handles the toroidal nature of the world
	if (m_vPosition.x > CParams::WindowWidth) m_vPosition.x = 0;
	if (m_vPosition.x < 0) m_vPosition.x = CParams::WindowWidth;
	if (m_vPosition.y > CParams::WindowHeight) m_vPosition.y = 0;
	if (m_vPosition.y < 0) m_vPosition.y = CParams::WindowHeight;
	return true;
}


/**
*  Uses KD tree to return vector of coordinates of closets resource to this agent
*/
SVector2D Agent::GetClosestResource(vector<Resource> &resources, KdNode *& kdRoot)
{
	closestResourceIndex = -1;
	double best_dist = 999999;
	vClosestObject  = SVector2D(99999, 99999);

	KdNode * closestResource = 0;
	Resource dummyRes(m_vPosition.x, m_vPosition.y);
	KdNode * testNode = new KdNode( &dummyRes );

	// Attempt to find the nearest resource
	testNode->nearest(kdRoot, testNode, 0, 2, closestResource, best_dist, m_vPosition);

	// If an resource was found
	if (closestResource != 0)
	{
		closestResourceIndex = closestResource->resource->getIndex();
		vClosestObject = closestResource->resource->getPosition();
	}
	delete testNode;

	return vClosestObject;
}

SVector2D Agent::GetClosestAgent(vector<Agent> &agents)
{
	double closest_so_far = 99999;
	SVector2D vClosestObject(99999, 99999);
	for (uint i = 0; i < agents.size(); ++i)
	{
		double len_to_object = QuickVec2DLength(agents[i].getPosition() - m_vPosition);
		if ((len_to_object < closest_so_far) && (len_to_object <= CParams::fovRadius))
		{
			closest_so_far = len_to_object;
			vClosestObject = m_vPosition - agents[i].getPosition();
		}
	}
	return vClosestObject;
}

int Agent::CheckForResource(vector<Resource> &resources, KdNode *& tree, double size)
{
    GetClosestResource ( resources, tree ) ;
	SVector2D DistToObject = vClosestObject - m_vPosition;
	if (Vec2DLength(DistToObject) <= (size + 5))
	{
		return closestResourceIndex;
	}
	return -1;
}

void Agent::turn(SPoint pt, double rate_factor, bool towards)
{
	double aclockRotRads = m_dRotation + (rate_factor*MAX_TURNING_RATE_IN_DEGREES)*CParams::dPi / 180;
	double clockRotRads = m_dRotation - (rate_factor*MAX_TURNING_RATE_IN_DEGREES)*CParams::dPi / 180;
	SVector2D vLookAC(cos(aclockRotRads), sin(aclockRotRads));
	SVector2D vLookC(cos(clockRotRads), sin(clockRotRads));
	
	// Get the vector to the point from the agents current position:
	SVector2D vObj(SVector2D(pt.x, pt.y) - m_vPosition);
	Vec2DNormalize(vObj);

	// 1 if the two vectors point in the same direction
	// 0 if the two vectors are perpendicular
	// -1 if the two vectors are pointing in opposite directions
	// Therefore let's work out which if ACW rotation or CW rotation brings us closer to 1:
	double dot_aclockW = Vec2DDot(vLookAC, vObj);
	double dot_clockW = Vec2DDot(vLookC, vObj);

	if (towards)
		m_dRotation = (abs(1 - dot_aclockW) < abs(1 - dot_clockW)) ? aclockRotRads : clockRotRads;
	else
		m_dRotation = (abs(1 - dot_aclockW) < abs(1 - dot_clockW)) ? clockRotRads : aclockRotRads;
}

/**
*  ostream overload for data collection, write all params to file
*/
ostream& operator<<(ostream& out, const Agent & agent)
{
	out << "Agent Variables_"
		<< agent.m_dRotation
		<< "," << agent.m_dSpeed
		<< "," << agent.energy
		<< "," << agent.startingEnergy
		<< "," << agent.energyLeftForGroup
		<< "," << agent.m_dScale
		<< "," << agent.energyLost
		<< "," << agent.closestResourceIndex
		<< "," << agent.numberResourcesGathered
		<< "," << agent.groupNumber
		<< "," << agent.parentGroupId
		<< "," << agent.parent2GroupId;

	out << std::endl
		<< "Agent Position_"
		<< agent.m_vPosition.x
		<< "," << agent.m_vPosition.y;

	out << std::endl 
		<< "Agent Look At_"
		<< agent.m_vLookAt.x
		<< "," << agent.m_vLookAt.y;

	out << std::endl 
		<< "Agent Closest Object_"
		<< agent.vClosestObject.x
		<< "," << agent.vClosestObject.y;

	
    {
	    out << std::endl 
		    << agent.brain 
		    << std::endl;
    }   
	return out;
}
void Agent::decreaseEnergy()
{
	// Only decrease energy if moving
	if ( std::abs( m_dSpeed ) > DBL_EPSILON)
	{
		double energyDecrease = CParams::dEnergyCostPerTick;
		energyLost += energyDecrease;
		energy -= energyDecrease;

		if (energy < 0)
		{
			energy = 0;
		}

		// If the starting energy is finished store what will be giving to group
		if (energyLost > startingEnergy)
		{
			energyLeftForGroup -= energyDecrease;
			if (energyLeftForGroup < 0)
			{
				energyLeftForGroup = 0;
			}
		}
	}
}

void Agent::setBrain(CNeuralNet & brain_)
{ 
	brain = CNeuralNet(brain_);
}

void Agent::setStartingEnergy( double energy )
{
    if ( energy > DBL_EPSILON )
    {
        const_cast<double&>(startingEnergy) = energy ;
    }
}

#include "CController.h"

Group::Group(int _groupId, double _energy, int _r, int _g, int _b) : groupId(_groupId), energy(_energy), r(_r), g(_g), b(_b)
{
	this->numberOfStartingAgents = 0;
    this->numberResourcesGathered = 0;
	this->numberOfCooperativeAgents = 0;
	this->degreeOfCooperation = 0;
	this->intraGroupCooperation = 0;
	this->groupDiversity = 0;
    this->representativeANN = nullptr;
	this->sqauredRepresentativeANN = nullptr;
	#ifdef _WINDOWS
		pen = CreatePen(PS_SOLID, 1, RGB(r, g, b));
	#endif
	maxAgentEnergy = 0;
}

Group::Group(int _groupId, double _energy, int _numberResourcesGathered,
	double _averageRelatedness, double _maxAgentEnergy,
	int _r, int _g, int _b, int _numberOfStartingAgents, int _numberofCooperativeAgents, double _intraGroupCooperation, double _degreeOfCooperation, double _groupDiversity, std::vector<Agent> _agentVector) :
	groupId(_groupId),
	energy(_energy),
	numberResourcesGathered(_numberResourcesGathered),
	averageRelatedness(_averageRelatedness),
	maxAgentEnergy(_maxAgentEnergy),
	numberOfStartingAgents(_numberOfStartingAgents),
	numberOfCooperativeAgents(_numberofCooperativeAgents),
	intraGroupCooperation(_intraGroupCooperation),
	degreeOfCooperation(_degreeOfCooperation),
	groupDiversity(_groupDiversity),
	r(_r), g(_g), b(_b)
{
    this->representativeANN = nullptr;
	this->sqauredRepresentativeANN = nullptr;
#ifdef _WINDOWS
	pen = CreatePen(PS_SOLID, 1, RGB(r, g, b));
#endif
	maxAgentEnergy = 0;

	for (uint i = 0; i < _agentVector.size(); ++i)
	{
		agentVector.push_back(_agentVector[i]);
	}
}

Group::Group( const Group & other ):
    groupId(other.groupId),
    energy(other.energy),
	numberResourcesGathered (other.numberResourcesGathered),
    collectedResources(other.collectedResources),
	averageRelatedness(other.averageRelatedness),
	numberOfStartingAgents(other.numberOfStartingAgents),
	maxAgentEnergy(other.maxAgentEnergy),
	r(other.r), g(other.g), b(other.b), 
	numberOfCooperativeAgents(other.numberOfCooperativeAgents), 
	degreeOfCooperation(other.degreeOfCooperation), 
	intraGroupCooperation(other.intraGroupCooperation),
	groupDiversity(other.groupDiversity)
{
#ifdef _WINDOWS
	pen = CreatePen(PS_SOLID, 1, RGB(r, g, b));
#endif
    agentVector = std::vector<Agent>(other.agentVector);
    if ( other.representativeANN )
    {
        representativeANN = new CNeuralNet( *(other.representativeANN) );
    }
    else 
    {
        representativeANN = nullptr;
    }

	if (other.sqauredRepresentativeANN)
	{
		sqauredRepresentativeANN = new CNeuralNet(*(other.sqauredRepresentativeANN));
	}
	else
	{
		sqauredRepresentativeANN = nullptr;
	}
}

Group::Group(Group && other) :
	groupId(other.groupId),
	energy(other.energy),
	collectedResources(other.collectedResources),
	numberResourcesGathered(other.numberResourcesGathered),
	numberOfStartingAgents(other.numberOfStartingAgents),
	averageRelatedness(other.averageRelatedness),
	maxAgentEnergy(other.maxAgentEnergy),
	r(other.r), g(other.g), b(other.b), numberOfCooperativeAgents(other.numberOfCooperativeAgents), degreeOfCooperation(other.degreeOfCooperation),
	intraGroupCooperation(other.intraGroupCooperation), groupDiversity(other.groupDiversity)
{
#ifdef _WINDOWS
	pen = CreatePen(PS_SOLID, 1, RGB(r, g, b));
#endif
    agentVector = other.agentVector;
    representativeANN = (other.representativeANN);
	sqauredRepresentativeANN = (other.sqauredRepresentativeANN);

    other.agentVector.clear();
    other.collectedResources.clear();
    other.representativeANN = nullptr;
	other.sqauredRepresentativeANN = nullptr;
    other.groupId = 0; 
    other.energy = 0;
	other.numberOfStartingAgents = 0;
    other.numberResourcesGathered = 0 ;
    other.averageRelatedness = 0;
    other.maxAgentEnergy = 0;
	other.degreeOfCooperation = 0;
	other.numberOfCooperativeAgents = 0;
	other.intraGroupCooperation = 0;
	other.groupDiversity = 0;
}

Group::Group (): 
    groupId(-1),
    energy(0),
	numberResourcesGathered(0), 
	averageRelatedness(0),
	maxAgentEnergy(0),
	r(0), g(0), b(0), numberOfStartingAgents(0), numberOfCooperativeAgents(0), degreeOfCooperation(0), intraGroupCooperation(0), groupDiversity(0)
{
    representativeANN = nullptr; 
	sqauredRepresentativeANN = nullptr;
#ifdef _WINDOWS
	pen = CreatePen(PS_SOLID, 1, RGB(r, g, b));
#endif
}

Group::~Group()
{
    if ( representativeANN ) 
    {
        delete representativeANN;
    }

    representativeANN = nullptr;
	sqauredRepresentativeANN = nullptr;
}

Group& Group::operator=(const Group & other) 
{
    if ( this != &other )
    {
        groupId = other.groupId ; 
        energy = other.energy ; 
    	numberResourcesGathered  = other.numberResourcesGathered ; 
        collectedResources = other.collectedResources;
    	averageRelatedness = other.averageRelatedness ; 
		degreeOfCooperation = other.degreeOfCooperation;
		intraGroupCooperation = other.intraGroupCooperation;
    	maxAgentEnergy = other.maxAgentEnergy ;  
        r = other.r ;
        g = other.g ;
        b = other.b ;
		numberOfCooperativeAgents = other.numberOfCooperativeAgents;
		groupDiversity = other.groupDiversity;
#ifdef _WINDOWS
		pen = CreatePen(PS_SOLID, 1, RGB(r, g, b));
#endif
        if ( this->representativeANN ) 
        {
            delete representativeANN;
        }
        if ( other.representativeANN )
        {
            this->representativeANN = new CNeuralNet ( *(other.representativeANN) );
        }
        else
        {
            this->representativeANN = nullptr;
        }

		if (this->sqauredRepresentativeANN)
		{
			delete sqauredRepresentativeANN;
		}
		if (other.sqauredRepresentativeANN)
		{
			this->sqauredRepresentativeANN = new CNeuralNet(*(other.sqauredRepresentativeANN));
		}
		else
		{
			this->sqauredRepresentativeANN = nullptr;
		}
        this->agentVector = std::vector<Agent> ( other.agentVector );
    }
    return *this; 
}

Group& Group::operator=(Group && other) 
{
    if ( this != &other )
    {
        groupId = other.groupId ; 
        energy = other.energy ; 
        collectedResources = other.collectedResources;
    	numberResourcesGathered  = other.numberResourcesGathered; 
		numberOfCooperativeAgents = other.numberOfCooperativeAgents;
		degreeOfCooperation = other.degreeOfCooperation;
    	averageRelatedness = other.averageRelatedness ; 
    	maxAgentEnergy = other.maxAgentEnergy ;  
        r = other.r ;
        g = other.g ;
        b = other.b ;
		groupDiversity = other.groupDiversity;
#ifdef _WINDOWS
		pen = CreatePen(PS_SOLID, 1, RGB(r, g, b));
#endif
        if ( this->representativeANN ) 
        {
            delete representativeANN;
        }

        this->representativeANN = (other.representativeANN);
        other.representativeANN = nullptr; 

		if (this->sqauredRepresentativeANN)
		{
			delete sqauredRepresentativeANN;
		}

		this->sqauredRepresentativeANN = (other.sqauredRepresentativeANN);
		other.sqauredRepresentativeANN = nullptr;

        this->agentVector = other.agentVector ;
    }
    return *this; 
}

void CController::groupUpdate()
{
	// Only perform the update if we are not on the first tick
	if (ticksPerGeneration <= CParams::iNumTicks) 
	{
		/*
		* Loop through the groups agentVectors,
		* pool together agent energy and remove agents with 0 energy
		*/
		for (Group & group : groupVector)
		{
			double totalEnergy = 0.0;
            // Loop through agent vector
			std::vector<Agent>::iterator itend = std::remove_if(group.agentVector.begin(), group.agentVector.end(),
				[&totalEnergy](Agent & agent)->bool
			{
				// If agent has > 0 energy, pool energy
				if (agent.getEnergy() > DBL_EPSILON)
				{
					double diff = agent.getEnergy() - agent.startingEnergy;
					if (diff > DBL_EPSILON)
					{
						totalEnergy += diff;
					}
					return false; 
				}

				// Remove agent
				return true ;
			});

			group.energy = totalEnergy;
			uint new_size = std::distance(group.agentVector.begin(), itend);
			group.agentVector.resize(new_size);
			group.degreeOfCooperation = ((double)group.collectedResources.size() / (double)numberOfActiveResources);
			if (group.degreeOfCooperation > 1)
			{
				cout << numberOfActiveResources << endl;
			}
		}
	}else
	{
		// Reset the groups resource and cooperative agent counts
		for (uint i = 0; i < groupVector.size(); i++)
		{
			groupVector[i].collectedResources.clear();
            groupVector[i].numberResourcesGathered = 0;
			groupVector[i].numberOfCooperativeAgents = 0;
			groupVector[i].degreeOfCooperation = 0;
		}
	}

	if(ticksPerGeneration >= CParams::iNumTicks)
	{
		/*
		* Iterate through groupVector and remove groups with 0 energy or agents
		* Only remove a group after the first generaiton
		*/

		// Iterate through groups and remove valid ones
		uint size = groupVector.size();
		std::vector<Group>::iterator gpend = std::remove_if(groupVector.begin(), groupVector.end(),
			[](Group & group)->bool
		{

			// If the group has 0 agents or energy, remove it
			return (group.agentVector.size() == 0 || group.energy <= DBL_EPSILON);
		});

		uint new_size = std::distance(groupVector.begin(), gpend);
		numberOfGroupDeaths += size - new_size;
		groupVector.resize(new_size);
	}

	// Only update the groupId for Relatedness experiment
	if (modeSwitch == 1)
	{
		// Update the groupdId variable
		for (uint i = 0; i < groupVector.size(); i++)
		{
			groupVector[i].groupId = i;
			for (Agent & agent : groupVector[i].agentVector)
			{
				agent.groupNumber = i;
			}
		}
	}
}


void CController::distributeGroupEnergy()
{
	// Update the groups energy
	for (size_t i = 0; i < groupVector.size(); ++i)
	{
		for (size_t j = 0; j < groupVector[i].agentVector.size(); ++j)
		{
			groupVector[i].agentVector[j].setEnergy(groupVector[i].energy / (double)groupVector[i].agentVector.size());
			groupVector[i].agentVector[j].setStartingEnergy(groupVector[i].agentVector[j].getEnergy());
		}
	}
}

void CController::generateAgents(std::vector<Agent> & children)
{
	double energyPerAgent = CParams::groupStartingEnergy / numberAgents;

	int r = Utilities::getInteger(200);
	int g = Utilities::getInteger(200);
	int b = Utilities::getInteger(200);
    Group group (0, CParams::groupStartingEnergy, r, g, b);

	// Generate the agents
	for (int j = 0; j < numberAgents; ++j)
	{
		SVector2D position;

		// Sets a random direction to move to
		position.x = Utilities::getInteger(800); 
		position.y = Utilities::getInteger(800);
		CNeuralNet brain = CNeuralNet(
                CParams::iNumInputs, 
                CParams::iNeuronsPerHiddenLayer, 
                CParams::iNumHidden, 
                CParams::iNumOutputs);
	
        Agent agent (position, 0, energyPerAgent, brain);
	    group.agentVector.push_back(agent);
    }

	group.numberOfStartingAgents = group.agentVector.size();
	group.calculateRepresentativeANN();
    groupVector.push_back ( group );
	children = group.agentVector;
}

void CController::generateRandomTeams()
{
	int numberOfTimes = 6;
	double energyPerAgent = CParams::groupStartingEnergy / numberAgents;

	int r = Utilities::getInteger(200);
	int g = Utilities::getInteger(200);
	int b = Utilities::getInteger(200);

	Group group (0, CParams::groupStartingEnergy,r, g, b);

	// Generate the groups
	for (int i = 0; i < numberOfTimes; ++i)
	{
		// Generate the agents
		for (int j = 0; j < numberAgents / numberOfTimes; ++j)
		{
			SVector2D position;

			// Sets a random direction to move to
		    position.x = Utilities::getInteger(800); 
		    position.y = Utilities::getInteger(800);
			CNeuralNet brain;

			// Creating the queen
			if (j == 0)
			{
				brain = CNeuralNet(
                CParams::iNumInputs, 
                CParams::iNeuronsPerHiddenLayer, 
                CParams::iNumHidden, 
                CParams::iNumOutputs);
			}
			else
			{
				// Create the agents temporary brain
				brain = CNeuralNet ( *(group.agentVector[0].getBrain()) );

				// Mutate the brain slightly to maintain some relatedness
				EA::mutate(brain, CParams::dMutationRate, CParams::dMaxPerturbation);
			}

            Agent agent (position, 0, energyPerAgent, brain);
			group.agentVector.push_back( agent );
		}
	}

	group.numberOfStartingAgents = group.agentVector.size();
	group.calculateRepresentativeANN();
    groupVector.push_back ( group );
}

void CController::generateRelatedTeams()
{
	double energyPerAgent = CParams::groupStartingEnergy / (numberAgents / 2);
	int r = 0;
	int g = 0;
	int b = 0;

	// Iterate through the groups
	for (size_t i = 0; i < 2; i++)
	{
        Group group (i, CParams::groupStartingEnergy,r, g, b);
		// Iterate through the agents
		for (int j = 0; j < numberAgents / 2; j++)
		{
			SVector2D position;

			if (i == 0)
			{
				position =SVector2D(750, 20);
				r = 255;
				b = 0;
			}
			else
			{
				position = SVector2D(20, 730);
				r = 0;
				b = 255;
			}
            Agent agent;
			// Creating the queen
			if (j == 0)
			{
				CNeuralNet brain = CNeuralNet(
                CParams::iNumInputs, 
                CParams::iNeuronsPerHiddenLayer, 
                CParams::iNumHidden, 
                CParams::iNumOutputs);
                agent = Agent (position, i, energyPerAgent, brain);
				group.agentVector.push_back( agent ) ;
			}
			else
			{
				// Create the agents temporary brain
				CNeuralNet brain = group.agentVector[0].getRBrain();

				// Mutate the brain but keep relatedness
				EA::mutate(brain, CParams::dMutationRate, CParams::dMaxPerturbation);


				// TODO: Check if temp related to queen, otherwise redo

				// Add agent to group
                agent = Agent (position, i, energyPerAgent, brain);
				group.agentVector.push_back( agent );
			}
		}
		group.numberOfStartingAgents = group.agentVector.size();
		group.calculateRepresentativeANN();
		groupVector.push_back( group );
	}

	std::cout << "" << std::endl;
	std::cout << "Relatedness of agents 0 and 20:\t" << groupVector[0].agentVector[0].getBrain()->calculateRelatedness(groupVector[0].agentVector[1].getBrain()) << std::endl;
	std::cout << "There for the agents are related:\t" << groupVector[0].agentVector[0].getBrain()->areRelated(groupVector[0].agentVector[1].getBrain()) << std::endl;
	std::cout << "" << std::endl;
	std::cout << "" << std::endl;

	// Test relatedness of two agents
	std::cout << "" << std::endl;
	std::cout << "The individual weight threshold is: " << CParams::geneRelatedThreshold << std::endl;
	std::cout << "The agent threshold is: " << CParams::relatedThreshold << std::endl;

	for (size_t i = 0; i < groupVector.size(); i++)
	{
		for (size_t j = 0; j < groupVector[i].agentVector.size(); j++)
		{
			std::cout << "Relatedness of agents 0 and 20:\t" << groupVector[i].agentVector[0].getBrain()->calculateRelatedness(groupVector[i].agentVector[j].getBrain()) << std::endl;
			std::cout << "There for the agents are related:\t" << groupVector[i].agentVector[0].getBrain()->areRelated(groupVector[i].agentVector[j].getBrain()) << std::endl;
			std::cout << "" << std::endl;
		}
		std::cout << "" << std::endl;
	}
}

void Group::calculateRepresentativeANN()
{
	// Creates blank ANN
	if (representativeANN)
	{
		delete representativeANN;
	}

	if (sqauredRepresentativeANN)
	{
		delete sqauredRepresentativeANN;
	}

	representativeANN = new CNeuralNet(
                CParams::iNumInputs, 
                CParams::iNeuronsPerHiddenLayer, 
                CParams::iNumHidden, 
                CParams::iNumOutputs);

	sqauredRepresentativeANN = new CNeuralNet(
		CParams::iNumInputs,
		CParams::iNeuronsPerHiddenLayer,
		CParams::iNumHidden,
		CParams::iNumOutputs);

    for ( uint i = 0;  i < representativeANN->size() ;  i++ ) 
    {
        representativeANN->_network[i] = 0.0;
		sqauredRepresentativeANN->_network[i] = 0.0;
    }

	// iterate through all agents
	for (size_t agentNo = 0; agentNo < agentVector.size(); agentNo++)
	{
		for (uint i = 0; i < representativeANN->size(); ++i)
		{
			representativeANN->_network[i] += agentVector[agentNo].getBrain()->_network[i];
			sqauredRepresentativeANN->_network[i] += std::pow(agentVector[agentNo].getBrain()->_network[i], 2);
		}
	}

	// Only need to divide if we have less than 1 agent
	if (agentVector.size() != 1)
	{
		// iterate through representative and calculate averages
		for (uint i = 0; i < representativeANN->size(); ++i)
		{
			// Get average
			representativeANN->_network[i] /= (double)agentVector.size();
		}
	}
}

void Group::calculateAverageRelatedness()
{
	averageRelatedness = 0.0;

	// iterate through all agents
	for (size_t agentNo = 0; agentNo < agentVector.size(); agentNo++)
	{
		// Relatedness of agent to representative ann
		double r = representativeANN->calculateRelatedness(agentVector[agentNo].getBrain());
		averageRelatedness += r;
	}
	averageRelatedness /= agentVector.size();
}

void Group::calculateGroupDiversity()
{
	groupDiversity = 0.0;

	// iterate through all agents
	for (size_t i = 0; i < agentVector.size(); i++)
	{
		Agent & agent1 = agentVector[i];

		// iterate through all agents
		for (size_t j = 0; j < agentVector.size(); j++)
		{
			Agent & agent2 = agentVector[j];

			if (agent1.getBrain() != agent2.getBrain())
			{
				groupDiversity += (1 - agent1.getBrain()->calculateRelatedness(agent2.getBrain()));
			}
		}	
	}
	groupDiversity /= std::pow(agentVector.size(), 2);
}
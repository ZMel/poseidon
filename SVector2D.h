#ifndef S2DVECTOR_H
#define S2DVECTOR_H

//!  2D Vector Structure and Methods
/*!
     Defines the widely used 2D vector and all its operations.
*/

#include <math.h>

struct SVector2D
{
	double x, y;
	
	/**
	*  SVector2D Constructor
	*
	*  @param double:   first value
	*  @param double:   second value
	*/
	SVector2D(double a = 0.0f, double b = 0.0f):x(a),y(b){}


	/**
	*  Operator overload for Svector2D: +=
	*/
	SVector2D &operator+=(const SVector2D &rhs)
	{
		x += rhs.x;
		y += rhs.y;

		return *this;
	}

	/**
	*  Operator overload for Svector2D: -=
	*/
	SVector2D &operator-=(const SVector2D &rhs)
	{
		x -= rhs.x;
		y -= rhs.y;

		return *this;
	}

	/**
	*  Operator overload for Svector2D: *=
	*/
	SVector2D &operator*=(const double &rhs)
	{
		x *= rhs;
		y *= rhs;

		return *this;
	}

	/**
	*  Operator overload for Svector2D: /=
	*/
  	SVector2D &operator/=(const double &rhs)
	{
		x /= rhs;
		y /= rhs;

		return *this;
	}
};

/**
*  Operator overload for Svector2D: Vector * Scalar
*/
inline SVector2D operator*(const SVector2D &lhs, double rhs)
{
  SVector2D result(lhs);
  result *= rhs;
  return result;
}

/**
*  Operator overload for Svector2D: Scalar * Vector
*/
inline SVector2D operator*(double lhs, const SVector2D &rhs)
{
  SVector2D result(rhs);
  result *= lhs;
  return result;
}

/**
*  Operator overload for Svector2D: -
*/
inline SVector2D operator-(const SVector2D &lhs, const SVector2D &rhs)
{
  SVector2D result(lhs);
  result.x -= rhs.x;
  result.y -= rhs.y;
  
  return result;
}

/**
*  Returns the true length of a 2D vector.
*
*  @param const SVector2D&:   some vector
*  @retval double:            length of given vector
*/
inline double Vec2DLength(const SVector2D &v)
{
	return sqrt(v.x * v.x + v.y * v.y);
}

/**
*  Returns the squared length of a 2D vector (avoids costly sqrt operation).
*
*  @param const SVector2D&:   some vector
*  @retval double:            squared length of given vector
*/
inline double QuickVec2DLength(const SVector2D &v)
{
	return (v.x * v.x + v.y * v.y);
}

/**
*  Normalizes a 2D vector.
*
*  @param SVector2D&:   some vector
*/
inline void Vec2DNormalize(SVector2D &v)
{
	double vector_length = Vec2DLength(v);

	v.x = v.x / vector_length;
	v.y = v.y / vector_length;
}

/**
*  Calculates the dot product between two 2D vectors
*
*  @param SVector2D&:    first vector
*  @param SVector2D&:    second vector
*  @retval double:       dot product between the two given vectors
*/
inline double Vec2DDot(SVector2D &v1, SVector2D &v2)
{
	return v1.x*v2.x + v1.y*v2.y;
}

/**
*  Returns positive if v2 is clockwise of v1, negative if anticlockwise.
*
*  @param SVector2D&:   first vector
*  @param SVector2D&:   second vector
*  @retval int:         1 or -1
*/
inline int Vec2DSign(SVector2D &v1, SVector2D &v2)
{
  if (v1.y*v2.x > v1.x*v2.y)
  { 
    return 1;
  }
  else 
  {
    return -1;
  }
}
#endif
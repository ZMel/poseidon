#include "KdNode.h"

KdNode::KdNode()
{
    resource = nullptr;
    left = nullptr;
    right = nullptr;
}

KdNode::KdNode( const KdNode & other )
{
    resource = other.resource; 
    if ( other.left ) 
    {
        left = new KdNode ( *(other.left) );
    }
    else 
    {
        left = nullptr;
    }
    if ( other.right )
    {
        right = new KdNode ( *(other.right) );
    }
    else
    {
        right = nullptr;
    }
}

KdNode::KdNode ( KdNode && other )
{
    resource = other.resource;
    left = other.left;
    right = other.right;
    
    other.resource = nullptr;
    other.left = nullptr;
    other.right = nullptr;
}

KdNode& KdNode::operator=( const KdNode & other )
{
    if ( this != &other )
    {
        if ( left )
            delete left;
        if ( right )
            delete right ;

        resource = other.resource; 
        if ( other.left ) 
        {
            left = new KdNode ( *(other.left) );
        }
        else 
        {
            left = nullptr;
        }
        if ( other.right )
        {
            right = new KdNode ( *(other.right) );
        }
        else
        {
            right = nullptr;
        }   
    }
    return *this;
}

KdNode& KdNode::operator=( KdNode && other )
{
    if ( this != &other )
    {
        if ( left )
            delete left;
        if ( right )
            delete right ;

        resource = other.resource; 
        left = other.left;
        right = other.right ;

        other.right = nullptr;
        other.left = nullptr;
    }
    return *this;
}

KdNode::~KdNode () 
{
    if ( this->left )
    {
        delete this->left;
    }
    this->left = nullptr;
    if ( this->right )
    {
        delete this->right ;
    }
    this->right = nullptr;
    this->resource = nullptr;
}

KdNode::KdNode(Resource * resource_)
{
	resource = resource_;
    left = nullptr;
    right = nullptr;
}

double KdNode::dist(KdNode *a, KdNode *b, int dim)
{
    return QuickVec2DLength( a->resource->getPosition() - b->resource->getPosition () ) ;
}

void KdNode::swap(KdNode *x, KdNode *y)
{
    Resource * tmp = x->resource; 
    x->resource = y->resource ;
    y->resource = tmp;
}

KdNode * KdNode::find_median(KdNode * start, KdNode *end, int idx)
{
	if (end <= start) return NULL;
	if (end == start + 1)
		return start;

	KdNode *p, *store, *md = start + (end - start) / 2;
	double pivot;

	while (1) {
		if (idx == 0)
		{
			pivot = md->resource->getPosition().x;
		}else
		{
			pivot = md->resource->getPosition().y;
		}

		swap(md, end - 1);

		for (store = p = start; p < end; p++) {
			
			if (idx == 0)
			{
				if (p->resource->getPosition().x < pivot) {
					if (p != store)
						swap(p, store);
					store++;
				}
			}
			else
			{
				if (p->resource->getPosition().y < pivot) {
					if (p != store)
						swap(p, store);
					store++;
				}
			}
		}

		swap(store, end - 1);

		if (idx == 0)
		{
			/* median has duplicate values */
			if (store->resource->getPosition().x == md->resource->getPosition().x)
				return md;
		}
		else
		{
			/* median has duplicate values */
			if (store->resource->getPosition().y == md->resource->getPosition().y)
				return md;
		}

		if (store > md) end = store;
		else        start = store;
	}
}

KdNode * KdNode::make_tree(KdNode * t, int len, int i, int dim)
{
	KdNode * n;

	if (!len) return 0;

	if ((n = find_median(t, t + len, i))) {
		i = (i + 1) % dim;
		n->left = make_tree(t, n - t, i, dim);
		n->right = make_tree(n + 1, t + len - (n + 1), i, dim);
	}

	return n;
}

void KdNode::nearest(KdNode *& root, KdNode *& agentPosition, int i, int dim,
	KdNode *& best, double & best_dist, SVector2D agentPos)
{
	double d, dx, dx2;

	// If we are at the bottom of the tree
	if (!root)
	{
		return;
	}

	// Current distance
	d = dist(root, agentPosition, dim);

	// If we are in the x dimension
	if (i == 0)
	{
		// Get the x side of the distance formula
		dx = root->resource->getPosition().x - agentPosition->resource->getPosition().x;
	}
	else
	{
		// Get the y side of the distance formula
		dx = root->resource->getPosition().y - agentPosition->resource->getPosition().y;
	}

	dx2 = dx * dx;

	if ((d < best_dist) && root->resource->isActive() && (d <= CParams::fovRadius))
	{
		best_dist = d;
		best = root;
	}

	nearest(dx > 0 ? root->left : root->right, agentPosition, (++i) % dim, dim, best, best_dist, agentPos);

	if (dx2 >= best_dist)
	{
		return;
	}

	nearest(dx > 0 ? root->right : root->left, agentPosition, (++i) % dim, dim, best, best_dist, agentPos);
}

int KdNode::preOrder(KdNode * root, bool print, SVector2D agentPost, std::string tabAmount)
{
	// If we are at the end
	if (root == NULL) {
		return numActive;
	}

	// If this resource is active and print then display
	if (root->resource->isActive() && print)
	{
		numActive += 1;
		SVector2D pos = QuickVec2DLength(root->resource->getPosition() - agentPost);
		std::cout << tabAmount << root->resource->getIndex() << ":  (" << root->resource->getPosition().x << " ," << root->resource->getPosition().y << "), Active:  " << root->resource->isActive() << "    (" << pos.x << " ," << pos.y << ")" << std::endl;
	}else if  (root->resource->isActive())
	{
		numActive += 1;
	}

	// Traverse the left and right subtrees
	numActive = preOrder(root->left, print, agentPost, (tabAmount + " "));
	numActive = preOrder(root->right, print, agentPost, (tabAmount + " "));
	return numActive;
}

#include "CController.h"

void CController::relatednessExperimentSetup(int previousChoice)
{
	int choice = 0;
	std::vector<Agent> children;

	if (previousChoice == 0)
	{
		std::cout << std::endl << std::endl << "Standard (1), Patchy Resources (2), Restart (3)";
		std::cout << std::endl << "Standard: Current chosen params from params.ini file";
		std::cout << std::endl << "Patchy Resources: Rich resource patches are distributed in the environment";
		std::cin >> choice;
	}
	else
	{
		choice = previousChoice;
	}
	
	if (choice == 1)
	{
		numberOfPatches = 1;
		widthOfPatch = 800;
		resourceDensity = 0.04;
		CParams::setResourcePositions.push_back(CParams::defaultResourcePositions[0]);

		// Otherwise check for other options chosen
		numberAgents = CParams::iNumSweepers;
		movement = true;
		numberOfPatches = CParams::numPatches;
		resourceDensity = CParams::density;
		CParams::setResourcePositions.push_back(CParams::defaultResourcePositions[0]);

		// Initialize the patches
		for (int i = 0; i < numberOfPatches; ++i)
		{
			ResourcePatch patch(CParams::setResourcePositions[i], widthOfPatch);
			patchesVector.push_back(patch);
		}

		// Add new resources to the simulation
		//generateResources();

		generatePresetResourceConfig(500);

		// Add the agents to the simulation
		generateAgents(children);
		paramChoice = 1;
		writeSnapshot();
	}
	else if (choice == 2)
	{
		int numberOfResourceNodes = 2000;
		numberOfPatches = 9;
		widthOfPatch = 150;
		SVector2D pos;

		for (int i = 0; i < 9; i++)
		{
			//pos.x = Utilities::getInteger(600, 0); pos.y = Utilities::getInteger(600, 0);
			//CParams::setResourcePositions.push_back(pos);
			patchesVector.push_back(ResourcePatch(CParams::defaultResourcePositions[i], widthOfPatch));
		}

		//// Initialize the patches
		//for (int i = 0; i < numberOfPatches; ++i)
		//{
		//	patchesVector.push_back(ResourcePatch(CParams::setResourcePositions[i], widthOfPatch));
		//}

		generatePresetResourceConfig(numberOfResourceNodes);

		// Add the agents to the simulation
		generateAgents(children);
		writeSnapshot();
		paramChoice = 2;
	}
	else
	{
		int temp_generationCounter = generationCounter;
		int temp_tickerPerGeneration = ticksPerGeneration;
		double temp_relatednessTreshold = CParams::relatedThreshold;
		double temp_geneThreshold = CParams::geneRelatedThreshold;
		int endGeneration = CParams::endGenerationNo;
		int temp_simluationRunId = CParams::simulationRunId;
		int localSelection = CParams::localSelection;
		double resourceGrowRate = CParams::resourceGrowthRate;


		#ifdef _WINDOWS
		snapshotFileName = "./SnapShots/Uniform/0.txt";
		#else
		snapshotFileName = "../SnapShots/Uniform/0.txt";
		#endif

		// Check if the file exists
		ifstream myfile(snapshotFileName);

		// Call the reset method
		snapshotRestart();

		generationCounter = temp_generationCounter;
		ticksPerGeneration = temp_tickerPerGeneration;
		CParams::relatedThreshold = temp_relatednessTreshold;
		CParams::geneRelatedThreshold = temp_geneThreshold;
		CParams::endGenerationNo = endGeneration;
		CParams::simulationRunId = temp_simluationRunId;
		CParams::localSelection = localSelection;
		CParams::resourceGrowthRate = resourceGrowRate;

		modeChoice = 1;
		modeSwitch = 1;
		paramChoice = 3;

		for (Group & group : groupVector)
		{
			for (Agent & agent : group.agentVector)
			{
				children.push_back(agent);
			}
		}
	}

	calculatePopulationDiversity(children);

	// Calculate the average ANN per group
	for (size_t i = 0; i < groupVector.size(); i++)
	{
		groupVector[i].calculateRepresentativeANN();
		groupVector[i].calculateGroupDiversity();
	}
}

void CController::calculatePopulationRepresentivieANN()
{
	// Creates blank ANN
	if (populationRepresentativeANN)
	{
		delete populationRepresentativeANN;
	}

	if (squaredPopulationRepresentativeANN)
	{
		delete squaredPopulationRepresentativeANN;
	}

	populationRepresentativeANN = new CNeuralNet(
		CParams::iNumInputs,
		CParams::iNeuronsPerHiddenLayer,
		CParams::iNumHidden,
		CParams::iNumOutputs);

	squaredPopulationRepresentativeANN = new CNeuralNet(
		CParams::iNumInputs,
		CParams::iNeuronsPerHiddenLayer,
		CParams::iNumHidden,
		CParams::iNumOutputs);

	for (uint i = 0; i < populationRepresentativeANN->size(); i++)
	{
		populationRepresentativeANN->_network[i] = 0.0;
		squaredPopulationRepresentativeANN->_network[i] = 0.0;
	}

	// iterate through all groups representative ann's
	for (size_t groupNo = 0; groupNo < groupVector.size(); groupNo++)
	{
		for (uint i = 0; i < populationRepresentativeANN->size(); ++i)
		{
			populationRepresentativeANN->_network[i] += groupVector[groupNo].representativeANN->_network[i];
			squaredPopulationRepresentativeANN->_network[i] += groupVector[groupNo].sqauredRepresentativeANN->_network[i];
		}
	}

	// iterate through representative and calculate averages
	for (uint i = 0; i < populationRepresentativeANN->size(); ++i)
	{
		// Get average
		populationRepresentativeANN->_network[i] /= (double)groupVector.size();
		squaredPopulationRepresentativeANN->_network[i] /= (double)groupVector.size();
	}
}


void CController::calculatePopulationDiversity(std::vector<Agent> & children)
{
	//calculatePopulationRepresentivieANN();

	//int annSize = populationRepresentativeANN->size();
	//populationDiversity = 0.0;
 //
	//for (size_t i = 0; i < annSize; i++)
	//{
	//	populationDiversity += (squaredPopulationRepresentativeANN->_network[i] - std::pow(populationRepresentativeANN->_network[i], 2));
	//}

	//populationDiversity = std::sqrt(populationDiversity) / annSize;

	populationDiversity = 0.0;

	for (size_t i = 0; i < children.size(); i++)
	{
		for (size_t j = 0; j < children.size(); j++)
		{
			if (children[i].getBrain() != children[j].getBrain())
			{
				populationDiversity += 1 - children[i].getBrain()->calculateRelatedness(children[j].getBrain());
			}
		}
	}

	populationDiversity /= std::pow(children.size(), 2);
	//cout << "Population Diversity: " << populationDiversity << endl << endl;
}
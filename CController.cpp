#include "CController.h"

//----------------------------------------------------------
//
//  Define geometry of agents and resources
// 
//-----------------------------------------------------------------------
const int	 numberAgentVerticies = 4;
const SPoint agent[numberAgentVerticies] = {
	SPoint(-1, -1),
	SPoint(-1, 1),
	SPoint(1, 1),
	SPoint(1, -1) };

const int numberResourceVertices = 4;
const SPoint resource[numberResourceVertices] = {
	SPoint(-1, -1),
	SPoint(-1, 1),
	SPoint(1, 1),
	SPoint(1, -1) };

ResourcePatch::ResourcePatch(SVector2D topLeft_, double width_) : topLeft(topLeft_), width(width_)
{}

CController::CController(H_WINDOW hwndMain, bool first, int choice_, int pChoice) :
numberAgents(CParams::iNumSweepers),
simulationSpeed(false),
ticksPerGeneration(0),
numberResources(CParams::iNumMines),
resourceDensity(CParams::density),
numberOfPatches(CParams::numPatches),
widthOfPatch(CParams::patchWidth),
m_hwndMain(hwndMain),
generationCounter(0),
cxClient(CParams::WindowWidth),
cyClient(CParams::WindowHeight),
modeChoice(choice_),
populationRepresentativeANN(nullptr),
squaredPopulationRepresentativeANN(nullptr),
firstRun(first),
paramChoice(pChoice)
{
	// Create a pen for the graph drawing
#ifdef _WINDOWS
	m_BluePen = CreatePen(PS_SOLID, 1, RGB(0, 0, 255));
	m_RedPen = CreatePen(PS_SOLID, 1, RGB(255, 0, 0));
	m_GreenPen = CreatePen(PS_SOLID, 1, RGB(0, 150, 0));
	m_currentPen = NULL;
	m_OldPen = NULL;
#endif

	// Fill the vertex buffers
	for (int i = 0; i < numberAgentVerticies; ++i)
	{
		agentVerticies.push_back(agent[i]);
	}
	for (int i = 0; i < numberResourceVertices; ++i)
	{
		resourceVerticies.push_back(resource[i]);
	}

	// Indicate that we are on the first generation of the system
	firstGeneration = true;

	if (firstRun == true)
	{
		if (modeChoice == 0)
		{
			std::cout << std::endl << "Relatedness Experiment (1)." << endl << "Group Size Experiment(2)." << endl << "Competition Experiment(3)." << endl << "Reload from snapshot(4)." << endl;
			std::cin >> choice;
			modeChoice = choice;
		}
	}
	else
	{
		choice = modeChoice;
	}

	if (choice == 1)
	{
		relatednessExperimentSetup(pChoice);
		modeSwitch = 1;
	}
	if (choice == 2)
	{
		if (firstRun == true)
			populationExperimentSetup();
		else
			populationExperimentSetup();
		modeSwitch = 2;
	}
	if (choice == 3)
	{
		if (firstRun == true)
			competitionExperimentSetup(0);
		else
			competitionExperimentSetup(pChoice);
		modeSwitch = 3;
	}
	if (choice == 4)
	{
		// If reset option chosen the load values from reset file
		std::cout << std::endl << "Please enter the SnapShot run id and the snapshot generationNumber seperated by a ',' e.g. 200,20:";
		std::cin >> snapshotFileName;
		std::vector<string> tokens = splitByDelim(snapshotFileName, ',');

		snapshotFileName = "./SnapShots/" + tokens[0] + "/" + tokens[1] + ".txt";

		// Check if the file exists
		ifstream myfile(snapshotFileName);

		while (!myfile.is_open())
		{
			std::cout << std::endl << "That snapshot does not exist, please try again e.g. 1,02-09-2015_22-15-23:";
			snapshotFileName = "";
			std::cin >> snapshotFileName;
			std::vector<string> tokens = splitByDelim(snapshotFileName, ',');

			snapshotFileName = "./SnapShots/" + tokens[0] + "/" + tokens[1] + ".txt";
			myfile.open(snapshotFileName);
		}

		// Call the reset method
		snapshotRestart();
	}

	numberOfGroups = groupVector.size();

	// Increment the simulationRunId in the params file
	CParams::simulationRunId++;
	CParams::writeResetFileId(std::string(
#ifndef _WINDOWS
		"../"
#endif
		"params.ini"));

#ifndef _WINDOWS
	cout << endl << "Generation Information:" << endl;
	cout << "---------------------------------";
#endif

	// Feed resource positions into KD tree for fast lookup
	initializeKdTree();
}

CController::~CController()
{
#ifdef _WINDOWS
	DeleteObject(m_BluePen);
	DeleteObject(m_RedPen);
	DeleteObject(m_GreenPen);
	DeleteObject(m_currentPen);
	DeleteObject(m_OldPen);
#endif
	
	CParams::setResourcePositions.clear();
}

/**
*  Builds a KD tree based on the resource configuration
*  Used for quick lookup of closest resource to a given agent
*/
void CController::initializeKdTree()
{
	uint size = resourcesVector.size();
	KdNode * wp = new KdNode[size];
	for (size_t i = 0; i < size; i++)
	{
		resourcesVector[i].setActiveStatus(true);
		wp[i] = KdNode(&resourcesVector.at(i));
	}
	root = root->make_tree(wp, size, 0, 2);
}

void CController::WorldTransform(vector<SPoint> &VBuffer, SVector2D vPos)
{
	// create the world transformation matrix
	C2DMatrix matTransform;
	// scale
	matTransform.Scale(CParams::dMineScale, CParams::dMineScale);
	// translate
	matTransform.Translate(vPos.x, vPos.y);
	// transform the ships vertices
	matTransform.TransformSPoints(VBuffer);
}

/**
*  Generates resources in the world based on number of patches,
*  patch position, and resource density of patches
*/
void CController::generateResources()
{
	std::cout << "" << std::endl;
	std::cout << "Patch Density: " << resourceDensity << std::endl;
	std::cout << "Number of Patches : " << numberOfPatches << std::endl;
	std::cout << "Patch Width : " << widthOfPatch << std::endl;
    int idx = 0;

	// Initialize resources in random positions within the resource patches
	for (int i = 0; i < numberOfPatches; ++i)
	{
		// Go through all cells within the patch and possibly define a reource
		for (float x = patchesVector[i].topLeft.x; x < (patchesVector[i].topLeft.x + patchesVector[i].width); x += 5)
		{
			for (float y = patchesVector[i].topLeft.y; y < (patchesVector[i].topLeft.y + patchesVector[i].width); y += 5)
			{
				double percentageToPlace = Utilities::getDouble();

				// If we can place
				if (percentageToPlace != 0 && percentageToPlace <= (resourceDensity / 2))
				{
                    SVector2D position (x, y);
                    Resource res (position, idx++);
					resourcesVector.push_back(res);
				}
			}
		}
	}
	numberResources = resourcesVector.size();
	numberOfActiveResources = numberResources;
	printf("Number of Resources:  %d \n", numberResources);
}

void CController::generatePresetResourceConfig(int numNodes)
{
	int nodesPerPatch = numNodes / numberOfPatches;
	int idx = 0;

	// Initialize resources in random positions within the resource patches
	for (int i = 0; i < numberOfPatches; ++i)
	{
		for (int j = 0; j < nodesPerPatch; ++j)
		{
			SVector2D position(Utilities::getDouble(patchesVector[i].topLeft.x + patchesVector[i].width, patchesVector[i].topLeft.x),
				Utilities::getDouble(patchesVector[i].topLeft.y + patchesVector[i].width, patchesVector[i].topLeft.y));
			Resource res(position, idx++);
			resourcesVector.push_back(res);
		}
	}
	numberResources = resourcesVector.size();
	numberOfActiveResources = numberResources;
	printf("Number of Resources:  %d \n", numberResources);
}

bool CController::Update()
{
	std::ofstream to_file(CParams::sOutFilename, ios::app | ios::out);
   
	if ( firstGeneration )
    {
        for (uint j = 0; j < groupVector.size(); ++j)
	    {
	        for (uint i = 0; i < groupVector[j].agentVector.size(); ++i)
		    {
                agentUpdate ( i, j );
            }
        }
        firstGeneration = false;
    }	
	
	if (groupVector.size() != 0)
	{
		/*
		* Run the simulation iNumTicks times.
		* Each agents ANN is updated with surrounding information
		* Output from ANN obtained and agent moved
		*/
		if (ticksPerGeneration++ < CParams::iNumTicks)
		{
			if (modeSwitch == 2 && ticksPerGeneration == 1)
			{
				using namespace statsOutstream;
				to_file << generationCounter << " " 
						<< resourcesVector.size() - inactiveResources.size() << std::endl;
				for (Group g : groupVector)
					to_file << g;
			}

			// Iterate through the inactive resources and check if any must be reactivated
			for (size_t i = 0; i < inactiveResources.size(); i++)
			{
				// If its time for the resource to regrow
				if (resourcesVector[inactiveResources[i]].getInactiveTickCount() >= CParams::resourceGrowthRate)
				{
					// Set the resource as active and remove it from the inactive vector
					resourcesVector[inactiveResources[i]].setActiveStatus(true);
					resourcesVector[inactiveResources[i]].resetInactiveTickCount();
                    inactiveResources.erase ( inactiveResources.begin () + i ) ;
					i--;
				}
				else
				{
					resourcesVector[inactiveResources[i]].incrementInactiveTickCount();
				}
			}


   			// Iterate through the agents and update the simulation
			for (uint j = 0; j < groupVector.size(); ++j)
			{
				for (uint i = 0; i < groupVector[j].agentVector.size(); ++i)
				{
					// If the agent has 0 energy skip it (Note: GroupUpdate will remove agent)
					if (groupVector[j].agentVector[i].getEnergy() == 0)
					{
						continue;
					}

					// Update agents ANN output and decrease its energy for moving
					agentUpdate(i, j);

					// Update the ANN and position
					if (!groupVector[j].agentVector[i].Update() && m_hwndMain)
					{
						// error in processing the neural net
#ifdef _WINDOWS
						MessageBox(m_hwndMain, "Wrong amount of NN inputs!", "Error", MB_OK);
#endif
						return false;
					}

					// See if it's found a resource
					int GrabHit = groupVector[j].agentVector[i].CheckForResource(resourcesVector, this->root, CParams::dMineScale);

					// if the agent has collided with an active resource AND is moving, pick up the resource
					if (GrabHit >= 0 && resourcesVector[GrabHit].isActive() && groupVector[j].agentVector[i].getSpeed() > DBL_EPSILON)
					{
						// Agent has discovered a resource so increase energy
						groupVector[j].agentVector[i].IncrementEnergy();
						groupVector[j].agentVector[i].incrementResourceCount();

						// Update the groups cooperative agent counter if this is the first resource collected by the agent
						if (groupVector[j].agentVector[i].numberResourcesGathered == 1)
						{
							groupVector[j].numberOfCooperativeAgents++;

							// Update the intra-group cooperation value
							groupVector[j].intraGroupCooperation = ((double)groupVector[j].numberOfCooperativeAgents / (double)groupVector[j].numberOfStartingAgents);
						}

						// Set the resource to inactive
						resourcesVector[GrabHit].setActiveStatus(false);
						inactiveResources.push_back(GrabHit);

						// Increase the number of resources gathered within this generation
						numberOfResourcesGathered++;
                        groupVector[j].numberResourcesGathered++;

						// Add the resource to the resourcesCollected vector if unqiue
						groupVector[j].collectedResources.insert(GrabHit);
					}

				}
			}
			groupUpdate();
        }
		else
		{
#ifndef _WINDOWS
			PlotStats();
#endif

			// Print out the stats of the system for all experiment versions
			{
				writeStats(modeSwitch, to_file);
			}

			// Check if we finished the last generation
			if (generationCounter == CParams::endGenerationNo)
			{
				cout << "-----------------------------------------------------------------------" << endl << endl;
				return false;
			}

			std::vector<Agent> children;

			// Calculate the average ANN per group
			for (size_t i = 0; i < groupVector.size(); i++)
			{
				groupVector[i].calculateRepresentativeANN();
			}
		
			if (modeSwitch == 1)
			{
				EA::rankBased(groupVector, children, 4);
			}
			else if (modeSwitch == 2)
			{
				for (int i = 0; i < groupVector.size(); ++i)
				{
					EA::withinGroupSelection(groupVector[i], 4, 2);
				}
			}
			else
			{
				for (int i = 0; i < groupVector.size(); ++i)
				{
					EA::withinGroupSelection(groupVector[i], 1, 3);
				}
			}

			// Set to true to reupdate all new agents positions
			firstGeneration = true;

			// After we crossover we need to distribute the energy to the new generation
            distributeGroupEnergy();
		
			// Update the group and agent energies
			groupUpdate();

			// Increment the generation counter
			++generationCounter;

			// Reset cycles
			ticksPerGeneration = 0;

			// Calculate the diversity of the population
			calculatePopulationDiversity(children);

			// Store the number of starting agents and calculate the group diversities
			for (size_t i = 0; i < groupVector.size(); i++)
			{
				groupVector[i].numberOfStartingAgents = groupVector[i].agentVector.size();
				groupVector[i].calculateGroupDiversity();
			}
			
			for (size_t i = 0; i < inactiveResources.size(); i++)
			{
				// Set the resource as active and remove it from the inactive vector
				resourcesVector[inactiveResources[i]].setActiveStatus(true);
				resourcesVector[inactiveResources[i]].resetInactiveTickCount();
				inactiveResources.erase(inactiveResources.begin() + i);
				i--;
			}

			// Reset the collection and active resource values
			numberOfResourcesGathered = 0;
			numberOfActiveResources = numberResources - inactiveResources.size();
			numberOfGroups = groupVector.size();
		}
		return true;
	}
	else
	{
		std::cerr << " ALL DEAD [ 2 ] ";
		return false;
	}
}

#ifdef _WINDOWS
//-----------------------Render-----------------------------------
//
//  Renders the world
// 
//-----------------------------------------------------------------------
void CController::Render(HDC surface)
{
	// Render the stats
	string s = "Generation:          " + itos(generationCounter);
	TextOut(surface, 5, 0, s.c_str(), s.size());

	if (!simulationSpeed)
	{
		// Keep a record of the old pen
		m_OldPen = (HPEN)SelectObject(surface, m_GreenPen);

		// Render the resources
		for (uint i = 0; i < resourcesVector.size(); ++i)
		{
			// If the resource is still active
			if (resourcesVector[i].isActive())
			{
				// Grab the vertices for the resource shape
				vector<SPoint> resourceVB = resourceVerticies;

				WorldTransform(resourceVB, resourcesVector[i].getPosition());

				// Draw the resources
				MoveToEx(surface, (int)resourceVB[0].x, (int)resourceVB[0].y, NULL);

				for (uint vert = 1; vert < resourceVB.size(); ++vert)
				{
					LineTo(surface, (int)resourceVB[vert].x, (int)resourceVB[vert].y);
				}

				LineTo(surface, (int)resourceVB[0].x, (int)resourceVB[0].y);
			}
		}

		// Render the agents
		for (size_t j = 0; j < groupVector.size(); j++)
		{
			m_currentPen = groupVector[j].pen;
			SelectObject(surface, m_currentPen);

			for (uint i = 0; i < groupVector[j].agentVector.size(); i++)
			{
				// Grab the agent vertices
				vector<SPoint> agentVB = agentVerticies;

				// Transform the vertex buffer
				groupVector[j].agentVector[i].WorldTransform(agentVB);
				
				// Draw the agents
				MoveToEx(surface, (int)agentVB[0].x, (int)agentVB[0].y, NULL);
                for (uint vert = 1; vert < agentVB.size(); ++vert)
				{
					LineTo(surface, (int)agentVB[vert].x, (int)agentVB[vert].y);
				}
				LineTo(surface, (int)agentVB[0].x, (int)agentVB[0].y);
			}
		}

		// Put old pen back
		SelectObject(surface, m_OldPen);
	}
	else
	{
		PlotStats(surface);
	}

}

void CController::PlotStats(HDC surface)
{
	int newPosition = 0;
	int numAgentsOverall = 0;

	string title = "Group Informaton:";
	string rule = "------------------------------";
	string numberOfGroups = "Number of Groups:    " + itos(groupVector.size());
	string numberOfAgents = "Number of Agents:    " + itos(numAgentsOverall);
	string groupDeaths = "Number of Extinct Groups:    " + itos(numberOfGroupDeaths);
	string numActiveResources = "Number of Active Resources:    " + itos(numberOfActiveResources);

	TextOut(surface, 5, 40, title.c_str(), title.size());
	TextOut(surface, 5, 60, rule.c_str(), rule.size());
	TextOut(surface, 5, 100, numberOfGroups.c_str(), numberOfGroups.size());
	TextOut(surface, 5, 140, groupDeaths.c_str(), groupDeaths.size());
	TextOut(surface, 5, 160, numActiveResources.c_str(), numActiveResources.size());
	
	for (int i = 0; i < groupVector.size(); ++i)
	{
		m_currentPen = groupVector[i].pen;
		SelectObject(surface, m_currentPen);
		newPosition = 20 * i;

		string groupId = "Group: " + ftos(groupVector[i].groupId);
		string colour = "";
		string numAgents = ";    Agents:    " + ftos(groupVector[i].agentVector.size());
		string groupEnergy = ";    Energy:    " + ftos((int)groupVector[i].energy);
		string resources = ";    Resources:    " + ftos(groupVector[i].collectedResources.size());
		string numCoop = ";    Num Coop:    " + ftos(groupVector[i].numberOfCooperativeAgents);
		string intraCoop = ";    Intra Coop:    " + ftos(std::roundf(groupVector[i].intraGroupCooperation * 100) / 100);

		numAgentsOverall += groupVector[i].agentVector.size();
		numberOfAgents = "Number of Agents:    " + itos(numAgentsOverall);
		groupId += colour + numAgents + numCoop + intraCoop + groupEnergy + resources;
		TextOut(surface, 5, 120, numberOfAgents.c_str(), numberOfAgents.size());
		TextOut(surface, 5, 200 + newPosition, groupId.c_str(), groupId.size());
		newPosition = 20 + newPosition;
	}
}

#else
void CController::PlotStats()
{
	cout << endl << endl << "Generation: " << generationCounter << endl << endl;

	cout << "Group Informaton:" << endl;
	cout << "------------------------------" << endl;

	cout << endl << "	Number of Groups:    " << groupVector.size() << endl;
	cout << "	Number of Extinct Groups:    " << numberOfGroupDeaths << endl << endl;
	cout << "	Number of Active Resources:    " + numberOfActiveResources;

	for (Group group : groupVector)
	{
		cout << "	Group: " << group.groupId;
		cout << ";	Colour (r,g,b):	(" << group.r << ", " << group.g << ", " << group.b << ")";
		cout << ";	Number Of Agents: " << group.agentVector.size();
		cout << ";	Energy: " << group.energy << "[" << (group.energy / (double)group.agentVector.size()) << "]";
		cout << ";  Unique Resources: " << group.collectedResources.size() << "[" << group.collectedResources.size() / (double)group.agentVector.size() << "]";

		cout << endl;
	}
}
#endif

void CController::createDir(string dir) {

#ifdef _WINDOWS
	dir = "./" + dir;
	_mkdir(dir.data());
#else
	dir = "../" + dir;
	mkdir(dir.data(), 0777);
#endif
}

double CController::scale(double input, double max)
{
	double mean = max / 2;
	double output = 0;
	output = (input - mean) / mean;
	if (output > 1)
		return 1;
	if (output < -1)
		return -1;

	return output;
}


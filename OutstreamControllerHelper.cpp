#include "CController.h"

// Resource ostream operator overload
ostream& operator<<(ostream& out, const ResourcePatch & resourcePatch)
{
	out << resourcePatch.topLeft.x
		<< "," << resourcePatch.topLeft.x
		<< std::endl << resourcePatch.width;

	return out;
}

// statsOutstream namespace Group ostream operator overload
ostream& statsOutstream::operator << (ostream& out, const Group & group)
{
	int sum = 0;
	for (Agent agent : group.agentVector)
	{
		if (agent.numberResourcesGathered) sum++;
	}
	out << group.groupId << " " 
		<< group.r << " " 
		<< group.g << " " 
		<< group.b << " " 
		<< group.energy << " "
		<< group.collectedResources.size() << " " 
		<< sum ;

	if (group.agentVector.size() == 0) {
		out << std::endl;
		return out;
	}
	else
		out << " ";

	for (size_t i = 0; i < group.agentVector.size() - 1; i++)
		out << group.agentVector[i].getEnergy() << " ";
	out << group.agentVector[group.agentVector.size() - 1].getEnergy() << std::endl;

	return out;
}

// restartOutstream namespace Group ostream operator overload
ostream& restartOutstream::operator << (ostream& out, const Group & group)
{
	out << "Group Variables_"
		<< group.groupId
		<< "," << group.energy
		<< "," << group.agentVector.size()
		<< "," << group.numberResourcesGathered
		<< "," << group.averageRelatedness
		<< "," << group.maxAgentEnergy
		<< "," << group.r
		<< "," << group.g
		<< "," << group.b
		<< "," << group.numberOfCooperativeAgents
		<< "," << group.intraGroupCooperation
		<< "," << group.degreeOfCooperation
		<< "," << group.groupDiversity
		<< "," << group.numberOfStartingAgents
		<< std::endl << *group.representativeANN << std::endl;

	out << std::endl
		<< "Agents: "
		<< std::endl;

	// Print out each agent on a new line
	for (size_t i = 0; i < group.agentVector.size(); i++)
	{
		out << std::endl 
			<< "Agent: " 
			<< std::endl 
			<< group.agentVector[i];
	}

	return out;
}

// CController ostream operator overload
ostream& operator << (ostream& out, const CController & controller)
{
	using namespace restartOutstream;

	// Print out the params from the params file
	out << CParams::returnParamsString() << std::endl;

	// Print the general CController parameters
	out << "Controller: "
		<< std::endl
		<< controller.numberAgents
		<< "," << controller.movement
		<< "," << controller.numberOfPatches
		<< "," << controller.resourceDensity
		<< "," << controller.widthOfPatch
		<< "," << controller.numberResources
		<< "," << controller.numberOfActiveResources
		<< "," << controller.numberOfGroupDeaths
		<< "," << controller.numberOfResourcesGathered
		<< "," << controller.m_NumWeightsInNN
		<< "," << controller.generationCounter
		<< "," << controller.cxClient
		<< "," << controller.cyClient
		<< "," << controller.removeAgentGroup
		<< "," << controller.maxDist
		<< "," << controller.firstGeneration
		<< "," << controller.simulationSpeed
		<< "," << controller.populationDiversity
		<< "," << controller.modeSwitch
		<< "," << controller.numberOfGroups
		<< "," << controller.groupVector.size()
		<< std::endl;

	// Print the patches, 2 lines each
	out << std::endl
		<< "Patches: "
		<< std::endl;

	for (size_t i = 0; i < controller.patchesVector.size(); i++)
	{
		out << controller.patchesVector[i] << std::endl;
	}

	// Print the resources, 2 lines each
	out << std::endl
		<< "Resources:"
		<< std::endl;

	for (size_t i = 0; i < controller.resourcesVector.size(); i++)
	{
		out << controller.resourcesVector[i] << std::endl;
	}

	// Print the groups
	out << std::endl
		<< "Groups: "
		<< std::endl
		<< std::endl;

	for (size_t i = 0; i < controller.groupVector.size(); i++)
	{
		// Seperate each group (and agents within) by a blank line
		out << "Group:" << std::endl << controller.groupVector[i] << std::endl;
	}

	return out;
}
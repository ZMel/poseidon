#ifndef AGENT_H
#define AGENT_H

//!  Brief Description
/*!
	 This is the agent class with agent shit in it.
*/

#include <vector>
#include <math.h>

#include "CNeuralNet.h"
#include "utils.h"
#include "C2DMatrix.h"
#include "SVector2D.h"
#include "CParams.h"
#include "Resource.h"
#include "KdNode.h"

using namespace std;
#define MAX_TURNING_RATE_IN_DEGREES 2.0

class Agent
{
private:
	SVector2D m_vPosition;		// Position in world
	SVector2D m_vLookAt;		// Direction agent is facing
	double m_dRotation;         // Rate at which agents are able to rotate
	double m_dSpeed;			// Movement speed
	double energy;				// Agents fitness score
	double energyLeftForGroup;	// Amount of gained energy to be given to group
	double m_dScale;			// Scale of visible agent
	double energyLost;			// Used to track amount of energy lost
	CNeuralNet brain;           // Neural network resposible for agent control

public:
	double rank;                        // Value used in rank based selection
	const double startingEnergy;		// Initial energy per round
	SVector2D vClosestObject;           // Object storing the position of the closest object to the agent
	int closestResourceIndex;           // Index position of closest resource object
	int numberResourcesGathered;        // How many resources this agent has gathered
	int groupNumber;                    // Which group this agent belongs to
	int parentGroupId;                  // Which group parent 1 came from
	int parent2GroupId;                 // Which group parent 2 came from

	// ----------------------------------------------------Constructors----------------------------------------------------------------------------------

	/**
	*  Default Constructor for an agent object.
	*
	*  @param SVector2D:       xy position of agent
	*  @param int:             group number to which this agent will be assigned
	*  @param double:          amount of energy (fitness) that this agent will start with
	*  @param CNeuralNet *:    neural network controller for this agent
	*/                                                                               
	Agent(SVector2D position_, int groupNumber_, double startingFitness, CNeuralNet & brain_);
	
	/**
	*  Snapshot Reset Constructor for an agent object.
	*
	*  @param double:             rotation speed of the agent
	*  @param double:             speed of the agent
	*  @param double:             current energy level of the agent
	*  @param double:             starting energy of the agent
	*  @param double:             energy remaining to be given back to group
	*  @param double:             scale of agent
	*  @param double:             amount of energy lost by this agent
	*  @param int:                index of the closest resource
	*  @param int:                number of individual resource nodes gathered
	*  @param int:                number of group to which this agent belongs
	*  @param int:                group number of first parent
	*  @param int:                group number of second parent
	*  @param SVector2D:          current xy position
	*  @param SVector2D:          current look direction
	*  @param SVector2D:          xy position of closest resource
	*  @param CNeuralNet *:       current neural network controller
	*/
	Agent(double _m_dRotation, double _m_dSpeed, double _energy, double _startingEnergy, double _energyLeftForGroup, double _m_dScale, double _energyLost,
		int _closestResourceIndex, int _numberResourcesGathered, int _groupNumber, int _parentGroupId, int _parent2GroupId,
		SVector2D _m_vPosition, SVector2D _m_vLookAt, SVector2D _vClosestObject, CNeuralNet *& brain_);

    ALSB_SIX_MEMBER_FUNCTIONS(Agent)

	// ----------------------------------------------------General Methods----------------------------------------------------------------------------------

	/**
	*  Updates the look at vector and position of an agent
	*
	*  @retval bool:                        TRUE if successful
	*/
	bool Update();

	/**
	*  Used to transform the sweepers vertices prior to rendering.
	*
	*  @param vector<SPoint> &:              agent object
	*/
	void WorldTransform(vector<SPoint> &agent);

	/**
	*  Checks to see if the agent has 'collected' a resource.
	*
	*  @param vector<Resource> &:     vector of all resources
	*  @param double:                 size of the agent
	*  @retval int:                   index into the resource vector of closest resource
	*/
	int CheckForResource(vector<Resource> &resources, KdNode *& tree, double size);

	/**
	*  Resets the agent's position, energy level and rotation to starting conditions.
	*/
	void Reset();

	/**
	*  Increments the energy collected by an agent when it collides with a resource.
	*/
	void IncrementEnergy()
	{ 
		energy += CParams::energyGainedPerResource; 
	}

	/**
	*  Increment the number of resources this agent has collected.
	*/
	void incrementResourceCount(){ numberResourcesGathered++; }

	/**
	*  Decrease agent based on distance moved.
	*/
	void decreaseEnergy ();	

	/**
	*  Turns towards/away from the specified point at a specified rate.
    *
	*  @param SPoint:     point to turn to
	*  @param double:     turning rate
	*  @param bool:       TRUE to turn towards, FALSE to turn away
	*/
	void turn(SPoint pt, double rate_factor, bool towards = true);



	// ----------------------------------------------------Setters----------------------------------------------------------------------------------

	/**
	*  Set the energy of an agent.
	*
	*  @param double:     value to set energy to
	*/
	void setEnergy(double energy_){ energy = energy_; }

	/**
	*  Set the starting energy of an agent
	*
	*  @param double:    value to set starting energy to
	*/
    void setStartingEnergy(double energy) ;

	/**
	*  Set the energy for a group of agents.
	*
	*  @param double:     value to set group energy to
	*/
	void setEnergyForGroup(double energy_){ energyLeftForGroup = energy_; }

	/**
	*  Set the position of an agent.
	*
	* @param SVector2D:     xy coordinates of new agent position
	*/
	void setPosition(SVector2D position_){ m_vPosition = position_; }

	/**
	*  Set the rotation of an agent.
	*
	*  @param double:     value by which rotation should be incremented
	*/
	void setRotation(double rotation) { m_dRotation += rotation; }

	/**
	*  Set the speed of an agent.
	*
	*  @param double:      value of desired agent speed
	*/
	void setSpeed(double speed){ m_dSpeed = speed; }

	/**
	*  Set the neural network of an agent.
	*
	*  @param CNeuralNet *:       desired neural network
	*/
	void setBrain(CNeuralNet & brain_);


	// ----------------------------------------------------Getters----------------------------------------------------------------------------------

	/**
	*  Get the movement speed of an agent.
	*
	*  @retval double:      agent move speed
	*/
	double getSpeed() const { return m_dSpeed; }

	/**
	*  Get starting energy of this agent.
	* 
	*  @retval double:     starting energy of this agent
	*/
	double getStartingEnergy() const { return startingEnergy; }

	/**
	*  Get current energy of this agent.
	*
	*  @retval double:     energy of this agent
	*/
	double getEnergy() const { return energy; }

	/**
	*  Get number of individual resources collected by this agent.
	*
	*  @retval int:        number of resources collected
	*/
	int getCollected() const { return numberResourcesGathered; }

	/**
	*  Get energy left over to share with group.
	*
	*  @retval double:     energy left over to be given back to group
	*/
	double getEnergyForGroup()const{ return energyLeftForGroup; }

	/**
	*  Get direction this agent is facing.
	*
	*  @retval SVector2D:  vector showing which direction agent is facing
	*/
	SVector2D getLookAt()const{ return m_vLookAt; }

	/**
	*  Get neural network for this agent.
	*
	*  @retval CNeuralNet *:   this agent's neural net
	*/
	CNeuralNet * getBrain(){ return &brain; }
	CNeuralNet & getRBrain(){ return brain; }

	/**
	*  Returns a vector to the closest resource.
	*
	*  @param vector<Resource> &:        vector of resource objects
	*  @param KdNode *:                  pointer to the root node of the KD tree in which resource nodes are stored
	*  @retval SVector2D:                vector to closest resource to this agent
	*/
	SVector2D GetClosestResource(vector<Resource> &objects, KdNode *& kdRoot);

	/**
	*  Returns a vector to the closest agent.
	*
	*  @param vector<Agent> &:           vector of all agents
	*  @retval SVector2D:                vector to closest agent
	*/
	SVector2D GetClosestAgent(vector<Agent> &agents);

	/**
	*  Get position of agent.
	*  
	*  @retval SVector2D:     xy coordinates of this agent
	*/
	SVector2D getPosition()const{ return m_vPosition; }

	/**
	*  Operator overload to allow saving data to file.
	*/
	friend ostream& operator<<(ostream&, const Agent&);

	/**
	*  Operator overload to allow sorting of a vector of agents based on their fitness valuse (lowest to highest)
	*
	*  @param const Agent &:     agent to compare to
	*  @retval bool:             TRUE if this agent has higher energy than the other, FALSE otherwise
	*/
	bool operator< (const Agent &other) const { return energy < other.energy; }
};
#endif



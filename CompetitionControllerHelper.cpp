#include "CController.h"
void CController::competitionExperimentSetup(int previousChoice)
{
	numberAgents = CParams::iNumSweepers;
	movement = true;
	int numberOfResourceNodes = 600;
	std::cout << "select a resource configuration on a scale from 1 to 5 (1 = uniform, 5 = patchy)" << std::endl;
	int opt;
	if (previousChoice == 0)
	{
		std::cin >> opt;
		paramChoice = opt;
	}
	else
		opt = previousChoice;
	if (opt == 1)
	{
		numberOfPatches = 1;
		CParams::setResourcePositions.push_back(CParams::defaultResourcePositions[0]);
	}
	else if (opt == 2)
	{
		numberOfPatches = 5;
		widthOfPatch = 200;
		SVector2D pos;
		for (int i = 0; i < numberOfPatches; i++)
		{
			pos.x = Utilities::getInteger(600, 0); pos.y = Utilities::getInteger(600, 0);
			CParams::setResourcePositions.push_back(pos);
		}
	}
	else if (opt == 3)
	{
		numberOfPatches = 10;
		widthOfPatch = 150;
		SVector2D pos;
		for (int i = 0; i < numberOfPatches; i++)
		{
			pos.x = Utilities::getInteger(600, 0); pos.y = Utilities::getInteger(600, 0);
			CParams::setResourcePositions.push_back(pos);
		}
	}
	else if (opt == 4)
	{
		numberOfPatches = 15;
		widthOfPatch = 100;
		SVector2D pos;
		for (int i = 0; i < numberOfPatches; i++)
		{
			pos.x = Utilities::getInteger(600, 0); pos.y = Utilities::getInteger(600, 0);
			CParams::setResourcePositions.push_back(pos);
		}
	}
	else
	{
		numberOfPatches = 20;
		widthOfPatch = 75;
		SVector2D pos;
		for (int i = 0; i < numberOfPatches; i++)
		{
			pos.x = Utilities::getInteger(750, 0); pos.y = Utilities::getInteger(750, 0);
			CParams::setResourcePositions.push_back(pos);
		}
	}

	// Initialize the patches
	for (int i = 0; i < numberOfPatches; ++i)
	{
		patchesVector.push_back(ResourcePatch(CParams::setResourcePositions[i], widthOfPatch));
	}
	generatePresetResourceConfig(numberOfResourceNodes);
	generateRedAndBlue();
}

void CController::generateRedAndBlue( const int * numAgents , int mode)
{
	double energyPerAgent = CParams::groupStartingEnergy / (CParams::iNumSweepers / 2);
	std::vector<Agent> queens;
	
	// Iterate through the groups
	for (size_t i = 0; i < 2; i++)
	{
		int r = 0;
		int g = 0;
		int b = 0;
		if (i == 0)
			r = 255;
		else
			b = 255;
		Group group(i, CParams::groupStartingEnergy, r, g, b);

		
		SVector2D groupCentroid;
		groupCentroid.x = Utilities::getInteger(CParams::WindowHeight - 100, 100);
		groupCentroid.y = Utilities::getInteger(CParams::WindowHeight - 100, 100);
		

		// Iterate through the agents
		for (size_t j = 0; j < ((numAgents) ?  numAgents[ i ] :  CParams::iNumSweepers / 2); j++)
		{
			SVector2D position;
			
			if (mode == 0)
			{
					position.x = groupCentroid.x + Utilities::getInteger(50, 0);
					position.y = groupCentroid.y + Utilities::getInteger(50, 0);
			}
			else
			{
				position.x = Utilities::getDouble(CParams::WindowWidth);
				position.y = Utilities::getDouble(CParams::WindowHeight);
			}
			CNeuralNet brain = CNeuralNet(
				CParams::iNumInputs,
				CParams::iNeuronsPerHiddenLayer,
				CParams::iNumHidden,
				CParams::iNumOutputs);
			group.agentVector.push_back(Agent(position, i, energyPerAgent, brain));
		}
		group.calculateRepresentativeANN();
		group.numberOfStartingAgents = group.agentVector.size();
		groupVector.push_back(group);
	}
}
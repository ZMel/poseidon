#include "utils.h"
#include <math.h>
#include <chrono>

//--------------------------itos------------------------------------
//	converts an integer to a string
//------------------------------------------------------------------		
string itos(int arg)
{
    ostringstream buffer;
	
	//send the int to the ostringstream
    buffer << arg;	
	
	//capture the string
    return buffer.str();		
}


//--------------------------ftos------------------------------------
//	converts a float to a string
//------------------------------------------------------------------		
string ftos(float arg)
{
    ostringstream buffer;
	
	//send the int to the ostringstream
    buffer << arg;	
	
	//capture the string
	return buffer.str();
}
//-------------------------------------Clamp()-----------------------------------------
//
//	clamps the first argument between the second two
//
//-------------------------------------------------------------------------------------
void Clamp(double &arg, double min, double max)
{
	if (arg < min)
	{
		arg = min;
	}

	if (arg > max)
	{
		arg = max;
	}
}
Utilities * Utilities::__utilities = nullptr;

Utilities::Utilities()
{
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	this->generator = std::default_random_engine(seed);

	this->d_dist = std::uniform_real_distribution<double>(0.0, DBL_MAX);
	this->c_dist = std::cauchy_distribution<double>(0.0, 1.0); // normal distribution mu=0.0 and sigma=1.0
	this->i_dist = std::uniform_int_distribution<int>(0, INT32_MAX);
}

Utilities & Utilities::operator= (const Utilities & other)
{
	if (this != &other)
	{
		this->d_dist = other.d_dist;
		this->i_dist = other.i_dist;
		this->generator = other.generator;
	}
	return *this;
}

Utilities * Utilities::getInstance()
{
	if (!__utilities)
	{
		__utilities = new Utilities();
	}
	return __utilities;
}

int Utilities::__getInteger(const int end, const int start)
{ 
	return start + this->i_dist(this->generator) % std::abs(start - end)  ;
}

double Utilities::__getDouble(const double end, const double start)
{
	return start + (this->d_dist(this->generator) / (DBL_MAX)) * std::abs(start - end);
}

double Utilities::getDouble(const double end, const double start)
{
	return Utilities::getInstance()->__getDouble(end, start);
}

int Utilities::getInteger(const int end, const int start)
{
	return Utilities::getInstance()->__getInteger(end, start);
}

double Utilities::__getCauchyDouble(const double end, const double start)
{
	double cauchy_diff = 300.0;
	double probability = this->c_dist(this->generator);
	double diff = std::abs(end - start); 
	probability = (probability / cauchy_diff);
	Clamp(probability, -0.5, 0.5);
	return ( start + (diff) /2.0) + probability * diff;
}

double Utilities::getCauchyDouble(const double end, const double start)
{
	return Utilities::getInstance()->__getCauchyDouble(end, start);
}

double Utilities::scale(double input, double max)
{
	double mean = max / 2;
	double output = 0;
	output = (input - mean) / mean;
	if (output > 1)
		return 1;
	if (output < -1)
		return -1;

	return output;
}


double Utilities::normalize(double input, double min, double max)
{
	double output;

	output = (input - min) / (max - min);

	return output;
}


void Utilities::destroyInstance()
{
	if (__utilities)
	{
		delete __utilities;
	}
	__utilities = nullptr;
}

std::vector<string> splitByDelim(string line, char delim)
{
    size_t start = 0, end = 0;
	vector<string> tokens;
	while ((end = line.find(delim, start)) != string::npos) {
		tokens.push_back(line.substr(start, end - start));
		start = end + 1;
	}
	tokens.push_back(line.substr(start));
	return tokens;
}


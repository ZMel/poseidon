#include "CController.h"



//---------------------Snapshot Restart---------------------------------
//
//  Restarts the system from the point where the snapshot left off
// 
//-----------------------------------------------------------------------


void CController::snapshotRestart()
{
	string line;
	ifstream myfile(snapshotFileName);



	int count = 0;

	if (myfile.is_open())
	{

		// Fisrt get the PARAM values
		getline(myfile, line);
		vector<string> tokens = splitByDelim(line, ',');

		CParams::dPi = atof(tokens[0].c_str());
		CParams::dHalfPi = atof(tokens[1].c_str());
		CParams::dTwoPi = atof(tokens[2].c_str());
		CParams::WindowWidth = atoi(tokens[3].c_str());
		CParams::WindowHeight = atoi(tokens[4].c_str());
		CParams::iFramesPerSecond = atoi(tokens[5].c_str());
		CParams::iNumInputs = atoi(tokens[6].c_str());
		CParams::iNumHidden = atoi(tokens[7].c_str());
		CParams::iNeuronsPerHiddenLayer = atoi(tokens[8].c_str());
		CParams::iNumOutputs = atoi(tokens[9].c_str());
		CParams::dActivationResponse = atof(tokens[10].c_str());
		CParams::dBias = atof(tokens[11].c_str());
		CParams::dEnergyCostPerTick = atof(tokens[12].c_str());
		CParams::dMaxTurnRate = atof(tokens[13].c_str());
		CParams::dMaxSpeed = atof(tokens[14].c_str());
		CParams::iSweeperScale = atof(tokens[15].c_str());
		CParams::dStartEnergy = atof(tokens[16].c_str());
		CParams::iNumSweepers = atoi(tokens[17].c_str());
		CParams::iNumMines = atoi(tokens[18].c_str());
		CParams::iNumTicks = atoi(tokens[19].c_str());
		CParams::dMineScale = atof(tokens[20].c_str());
		CParams::density = atof(tokens[21].c_str());
		CParams::numPatches = atof(tokens[22].c_str());
		CParams::patchWidth = atof(tokens[23].c_str());
		CParams::dCrossoverRate = atof(tokens[24].c_str());
		CParams::dMutationRate = atof(tokens[25].c_str());
		CParams::dMaxPerturbation = atof(tokens[26].c_str());
		CParams::iNumElite = atoi(tokens[27].c_str());
		CParams::iNumCopiesElite = atoi(tokens[28].c_str());
		CParams::relatedThreshold = atof(tokens[29].c_str());
		CParams::geneRelatedThreshold = atof(tokens[30].c_str());
		CParams::energyGainedPerResource = atoi(tokens[31].c_str());
		CParams::groupStartingEnergy = atoi(tokens[32].c_str());
		CParams::rateOfEnergyDescreasePerAgent = atof(tokens[33].c_str());
		CParams::willMove = atof(tokens[34].c_str());
		CParams::fovRadius = atof(tokens[35].c_str());
		CParams::selectionDistance = atoi(tokens[36].c_str());
		CParams::localSelection = atoi(tokens[37].c_str());
		CParams::tournamentSize = atof(tokens[38].c_str());
		CParams::numberOfChildren_perTournamentPair = atoi(tokens[39].c_str());
		CParams::selectionPressure = atof(tokens[40].c_str());
		CParams::isTournamentSelection = atoi(tokens[41].c_str());
		CParams::endGenerationNo = atoi(tokens[42].c_str());
		CParams::resetFileName = tokens[43];
		CParams::sOutFilename = tokens[44];

		int numDefaultResoursePosition = atoi(tokens[45].c_str());
		int numSetDefaultPositions = atoi(tokens[46].c_str());

		getline(myfile, line);  	// Catch the newline

		for (size_t i = 0; i < numDefaultResoursePosition; i++)
		{
			getline(myfile, line);     // Get the next param line
			tokens = splitByDelim(line, ',');

			CParams::defaultResourcePositions[i].x = atoi(tokens[0].c_str());
			CParams::defaultResourcePositions[i].y = atoi(tokens[1].c_str());
		}

		getline(myfile, line);  	// Catch the newline

		for (size_t i = 0; i < numSetDefaultPositions; i++)
		{
			getline(myfile, line);     // Get the next param line
			tokens = splitByDelim(line, ',');

			CParams::setResourcePositions.push_back(SVector2D(atoi(tokens[0].c_str()), atoi(tokens[1].c_str())));
		}

		getline(myfile, line);  	// Catch the newline

		// CONTROLLER values
		getline(myfile, line);  	// Read in the controller values
		tokens = splitByDelim(line, ',');

		numberAgents = atoi(tokens[0].c_str());
		movement = atoi(tokens[1].c_str());
		numberOfPatches = atof(tokens[2].c_str());
		resourceDensity = atof(tokens[3].c_str());
		widthOfPatch = atof(tokens[4].c_str());
		numberResources = atoi(tokens[5].c_str());
		numberOfGroupDeaths = atoi(tokens[6].c_str());
		numberOfResourcesGathered = atoi(tokens[7].c_str());
		m_NumWeightsInNN = atoi(tokens[8].c_str());
		//ticksPerGeneration = atoi(tokens[9].c_str());
		generationCounter = atoi(tokens[10].c_str());
		cxClient = atoi(tokens[11].c_str());
		cyClient = atoi(tokens[12].c_str());
		removeAgentGroup = atoi(tokens[13].c_str());
		maxDist = atof(tokens[14].c_str());
		firstGeneration = atoi(tokens[15].c_str());
		simulationSpeed = atoi(tokens[16].c_str());
		int numberOfGroups = atoi(tokens[17].c_str());

		// PATCHES values
		getline(myfile, line);  	// Catch the newline
		patchesVector.clear();

		for (size_t i = 0; i < numberOfPatches; i++)
		{
			getline(myfile, line);     // Get the next param line
			tokens = splitByDelim(line, ',');

			// Get the patch position
			SVector2D position;
			position.x = atoi(tokens[0].c_str());
			position.y = atoi(tokens[1].c_str());

			// Get the patch width
			getline(myfile, line);     // Get the next param line

			// Store the value
			patchesVector.push_back(ResourcePatch(position, atoi(line.c_str())));
		}

		// RESOURCES values
		getline(myfile, line);  	// Catch the newline
		resourcesVector.clear();

		for (size_t i = 0; i < numberResources; i++)
		{
			getline(myfile, line);     // Get the next param line
			tokens = splitByDelim(line, ',');

			// Get the resource position
			SVector2D position;
			position.x = atoi(tokens[0].c_str());
			position.y = atoi(tokens[1].c_str());

			// Store the value
			resourcesVector.push_back(Resource(position, atoi(tokens[2].c_str())));
		}

		// GROUP values
		groupVector.clear();

		for (size_t i = 0; i < numberOfGroups; i++)
		{
			getline(myfile, line);  	// Catch the newline

			// Pool group parameters
			getline(myfile, line);     // Get the next param line
			tokens = splitByDelim(line, ',');

			int groupId = atoi(tokens[0].c_str());
			int energy = atoi(tokens[1].c_str());
			int numberOfAgents = atoi(tokens[2].c_str());
			int numberResourcesGathered = atoi(tokens[3].c_str());
			double averageRelatedness = atof(tokens[4].c_str());
			double maxAgentEnergy = atof(tokens[5].c_str());
			int r = atoi(tokens[6].c_str());
			int g = atoi(tokens[7].c_str());
			int b = atoi(tokens[8].c_str());

			// REPRESENTATIVE ANN values
			getline(myfile, line);     // Get the next param line
			tokens = splitByDelim(line, ',');

			vector<Agent> groupAgentVector;      // vector of agents in this group
			int inputLayerSize = atoi(tokens[0].c_str());
			int hiddenLayerSize = atoi(tokens[1].c_str());
			int outputLayerSize = atoi(tokens[2].c_str());
			int annSize = atoi(tokens[3].c_str());

			std::vector<double> ann;              // the actual agent ann\

			getline(myfile, line);     // Get the next param line
			tokens = splitByDelim(line, ',');

			// ann values
			for (size_t i = 0; i < annSize; i++)
			{
				ann.push_back(atof(tokens[i].c_str()));
			}

			getline(myfile, line);     // Get the next param line
			tokens = splitByDelim(line, ',');

			int momentum = atoi(tokens[0].c_str());
			int o1 = atoi(tokens[1].c_str());
			int o2 = atoi(tokens[2].c_str());

			CNeuralNet representativeANN(inputLayerSize, hiddenLayerSize, outputLayerSize, ann, momentum, o1, o2);

			// AGENT values

			for (size_t i = 0; i < numberOfAgents; i++)
			{
				getline(myfile, line);  	// Catch the newline
				getline(myfile, line);  	// Read the agent values
				tokens = splitByDelim(line, ',');

				double m_dRotation = atof(tokens[0].c_str());
				double m_dSpeed = atof(tokens[1].c_str());
				double energy = atof(tokens[2].c_str());
				double startingEnergy = atof(tokens[3].c_str());
				double energyLeftForGroup = atof(tokens[4].c_str());
				double m_dScale = atof(tokens[5].c_str());
				double energyLost = atof(tokens[6].c_str());
				int closestResourceIndex = atoi(tokens[7].c_str());
				int numberResourcesGathered = atoi(tokens[8].c_str());
				int groupNumber = atoi(tokens[9].c_str());
				int parentGroupId = atoi(tokens[10].c_str());
				int parent2GroupId = atoi(tokens[11].c_str());

				//getline(myfile, line);  	// Catch the newline

				getline(myfile, line);     // Get the next param line

				tokens = splitByDelim(line, ',');
				SVector2D m_vPosition(atoi(tokens[0].c_str()), atoi(tokens[1].c_str()));

				getline(myfile, line);     // Get the next param line

				tokens = splitByDelim(line, ',');
				SVector2D m_vLookAt(atoi(tokens[0].c_str()), atoi(tokens[1].c_str()));

				getline(myfile, line);     // Get the next param line

				tokens = splitByDelim(line, ',');
				SVector2D vClosestObject(atoi(tokens[0].c_str()), atoi(tokens[1].c_str()));

				// AGENT ANN values
				getline(myfile, line);     // Get the next param line
				tokens = splitByDelim(line, ',');

				int inputLayerSize = atoi(tokens[0].c_str());
				int hiddenLayerSize = atoi(tokens[1].c_str());
				int outputLayerSize = atoi(tokens[2].c_str());
				int annSize = atoi(tokens[3].c_str());

				std::vector<double> ann;              // the actual agent ann

				getline(myfile, line);     // Get the next param line
				tokens = splitByDelim(line, ',');

				// ann values
				for (size_t i = 0; i < annSize; i++)
				{
					ann.push_back(atof(tokens[i].c_str()));
				}

				getline(myfile, line);     // Get the next param line
				tokens = splitByDelim(line, ',');

				int momentum = atoi(tokens[0].c_str());
				int o1 = atoi(tokens[1].c_str());
				int o2 = atoi(tokens[2].c_str());

				CNeuralNet * brain = new CNeuralNet(inputLayerSize, hiddenLayerSize, outputLayerSize, ann, momentum, o1, o2);

				// Create the agent object and push it to the groupAgentVector
				groupAgentVector.push_back(Agent(m_dRotation, m_dSpeed, energy, startingEnergy, energyLeftForGroup, m_dScale, energyLost, closestResourceIndex,
					numberResourcesGathered, groupNumber, parentGroupId, parent2GroupId, m_vPosition, m_vLookAt, vClosestObject, brain));
			}

			// Create the group object and push it to the CController's group vector
			groupVector.push_back(Group(groupId, energy, numberOfAgents,
				numberOfResourcesGathered, averageRelatedness, maxAgentEnergy, r, g, b, groupAgentVector));
		}

		myfile.close();
	}
	else{
		cout << "Unable to open file";
	}
}

//---------------------String Split--------------------------------------
//
//  Splits a given string by a delim and returns a vector containing contents
// 
//-----------------------------------------------------------------------
std::vector<string> CController::splitByDelim(string line, char delim)
{
	int start = 0, end = 0;
	vector<string> tokens;

	while ((end = line.find(delim, start)) != string::npos) {
		tokens.push_back(line.substr(start, end - start));
		start = end + 1;
	}

	tokens.push_back(line.substr(start));

	return tokens;
}

#include "CController.h"

int CController::setControllerVariables(ifstream * myfile)
{
	string line = "";

	getline(*myfile, line);  	// Catch the newline
	getline(*myfile, line);		// Catch the description line
	getline(*myfile, line);  	// Read in the controller values
	vector<string> tokens = splitByDelim(line, ',');

	numberAgents = atoi(tokens[0].c_str());
	movement = atoi(tokens[1].c_str());
	numberOfPatches = atof(tokens[2].c_str());
	resourceDensity = atof(tokens[3].c_str());
	widthOfPatch = atof(tokens[4].c_str());
	numberResources = atoi(tokens[5].c_str());
	numberOfActiveResources = atoi(tokens[6].c_str());
	numberOfGroupDeaths = atoi(tokens[7].c_str());
	numberOfResourcesGathered = atoi(tokens[8].c_str());
	m_NumWeightsInNN = atoi(tokens[9].c_str());
	generationCounter = atoi(tokens[10].c_str());
	cxClient = atoi(tokens[11].c_str());
	cyClient = atoi(tokens[12].c_str());
	removeAgentGroup = atoi(tokens[13].c_str());
	maxDist = atof(tokens[14].c_str());
	firstGeneration = atoi(tokens[15].c_str());
	simulationSpeed = atoi(tokens[16].c_str());
	populationDiversity = atof(tokens[17].c_str());
	modeSwitch = atoi(tokens[18].c_str());
	numberOfGroups = atoi(tokens[19].c_str());

	return atoi(tokens[20].c_str());
}

void CController::setPatchVariables(ifstream * myfile)
{
	string line = "";
	vector<string> tokens;

	getline(*myfile, line);  	// Catch the newline
	patchesVector.clear();
	
	getline(*myfile, line);     // Get the next param line

	for (size_t i = 0; i < numberOfPatches; i++)
	{
		getline(*myfile, line);		// Catch the description line
		tokens = splitByDelim(line, ',');
		
		// Get the patch position
		SVector2D position;
		position.x = atoi(tokens[0].c_str());
		position.y = atoi(tokens[1].c_str());

		// Get the patch width
		getline(*myfile, line);     // Get the next param line
		
		// Store the value
		patchesVector.push_back(ResourcePatch(position, atoi(line.c_str())));
	}
}

void CController::setResourceVariables(ifstream * myfile)
{
	string line = "";
	vector<string> tokens;

	getline(*myfile, line);  	// Catch the newline
	getline(*myfile, line);		// Catch the description line
	resourcesVector.clear();

	for (int i = 0; i < numberResources; i++)
	{
		getline(*myfile, line);     // Get the next param line
		tokens = splitByDelim(line, ',');

		// Get the resource position
		SVector2D position;
		position.x = atoi(tokens[0].c_str());
		position.y = atoi(tokens[1].c_str());

		// Store the value
		resourcesVector.push_back(Resource(position, atoi(tokens[2].c_str())));
	}
}

CNeuralNet * CController::getANNFromSnapshot(ifstream * myfile)
{
	string line = "";

	getline(*myfile, line);				 // Get the next param line
	vector<string> tokens = splitByDelim(line, '_');
	tokens = splitByDelim(tokens[1], ',');

	int inputLayerSize = atoi(tokens[0].c_str());
	int hiddenLayerSize = atoi(tokens[1].c_str());
	int outputLayerSize = atoi(tokens[2].c_str());
	int annSize = atoi(tokens[3].c_str());

    double * ann = new double [annSize];
	getline(*myfile, line);				  // Get the next param line
	tokens = splitByDelim(line, '_');
	tokens = splitByDelim(tokens[1], ',');

	// ann values
	for (int i = 0; i < annSize; i++)
	{
		ann[i] = (atof(tokens[i].c_str()));
	}

	CNeuralNet * result = new CNeuralNet(inputLayerSize, hiddenLayerSize, outputLayerSize, ann);
    delete [] ann ;
    return result;
}

Agent CController::setAgentVariables(ifstream * myfile)
{
	string line = "";
	vector<string> tokens;

	getline(*myfile, line);  	// Catch the newline
	getline(*myfile, line);		// Catch the description line
	getline(*myfile, line);  	// Read the agent values

	tokens = splitByDelim(line, ',');

	double m_dRotation = atof(tokens[0].c_str());
	double m_dSpeed = atof(tokens[1].c_str());
	double energy = atof(tokens[2].c_str());
	double startingEnergy = atof(tokens[3].c_str());
	double energyLeftForGroup = atof(tokens[4].c_str());
	double m_dScale = atof(tokens[5].c_str());
	double energyLost = atof(tokens[6].c_str());
	int closestResourceIndex = atoi(tokens[7].c_str());
	int numberResourcesGathered = atoi(tokens[8].c_str());
	int groupNumber = atoi(tokens[9].c_str());
	int parentGroupId = atoi(tokens[10].c_str());
	int parent2GroupId = atoi(tokens[11].c_str());

	getline(*myfile, line);     // Get the next param line

	tokens = splitByDelim(line, '_');
	tokens = splitByDelim(tokens[1], ',');
	SVector2D m_vPosition(atoi(tokens[0].c_str()), atoi(tokens[1].c_str()));

	getline(*myfile, line);     // Get the next param line

	tokens = splitByDelim(line, '_');
	tokens = splitByDelim(tokens[1], ',');
	SVector2D m_vLookAt(atoi(tokens[0].c_str()), atoi(tokens[1].c_str()));

	getline(*myfile, line);     // Get the next param line

	tokens = splitByDelim(line, '_');
	tokens = splitByDelim(tokens[1], ',');
	SVector2D vClosestObject(atoi(tokens[0].c_str()), atoi(tokens[1].c_str()));

	// AGENT ANN values
	CNeuralNet * brain = getANNFromSnapshot(myfile);

	// Create the agent object and push it to the groupAgentVector
	return Agent(m_dRotation, m_dSpeed, energy, startingEnergy, energyLeftForGroup, m_dScale, energyLost, closestResourceIndex,
		numberResourcesGathered, groupNumber, parentGroupId, parent2GroupId, m_vPosition, m_vLookAt, vClosestObject, brain);
}

void CController::setGroupVariables(ifstream * myfile, int numberOfGroups)
{
	string line = "";
	vector<string> tokens;
	groupVector.clear();

	getline(*myfile, line);  	// Catch the newline
	getline(*myfile, line);		// Catch the description line

	for (int i = 0; i < numberOfGroups; i++)
	{
		vector<Agent> groupAgentVector;      // vector of agents in this group

		getline(*myfile, line);  	// Catch the newline
		getline(*myfile, line);		// Catch the description line

		// Pool group parameters
		getline(*myfile, line);     // Get the next param line
		tokens = splitByDelim(line, ',');

		int groupId = atoi(tokens[0].c_str());
		int energy = atoi(tokens[1].c_str());
		int numberOfAgents = atoi(tokens[2].c_str());
		int numberOfResourcesGatheres = atoi(tokens[3].c_str());
		double averageRelatedness = atof(tokens[4].c_str());
		double maxAgentEnergy = atof(tokens[5].c_str());
		int r = atoi(tokens[6].c_str());
		int g = atoi(tokens[7].c_str());
		int b = atoi(tokens[8].c_str());
		int numberOfCooperativeAgents = atoi(tokens[9].c_str());
		double intraGroupCooperation = atof(tokens[10].c_str());
		double degreeOfCooperation = atof(tokens[11].c_str());
		double groupDiversity = atof(tokens[12].c_str());
		int numberOfStartingAgents = atoi(tokens[13].c_str());

		// REPRESENTATIVE ANN values
		CNeuralNet * representativeANN = getANNFromSnapshot(myfile);

		getline(*myfile, line);  	// Catch the newline
		getline(*myfile, line);		// Catch the description line

		// AGENT values
		for (int j = 0; j < numberOfAgents; j++)
		{
			groupAgentVector.push_back(setAgentVariables(myfile));
		}

		// Create the group object and push it to the CController's group vector
		groupVector.push_back(Group(groupId, energy,numberOfResourcesGathered, averageRelatedness, 
			maxAgentEnergy, r, g, b, numberOfStartingAgents, numberOfCooperativeAgents, intraGroupCooperation, degreeOfCooperation, groupDiversity, groupAgentVector));

		groupVector[i].representativeANN = representativeANN;
	}
}

/**
*  Reads all saved data from a snapshot file to initialize a new simulation from a "save point".
*/
void CController::snapshotRestart()
{
	string line;
	ifstream myfile(snapshotFileName);

	if (myfile.is_open())
	{
		// Fisrt get the PARAM values
		getline(myfile, line);		// Catch the description line
		getline(myfile, line);
		vector<string> tokens = splitByDelim(line, ',');

		// Load in most of the param values
		CParams::readInParameters(tokens);

		// Load the position vector param values
		int numDefaultResoursePosition = atoi(tokens[46].c_str());
		int numSetDefaultPositions = atoi(tokens[47].c_str());

		getline(myfile, line);  	// Catch the newline
		getline(myfile, line);		// Catch the description line

		for (int i = 0; i < numDefaultResoursePosition; i++)
		{
			getline(myfile, line);     // Get the next param line
			tokens = splitByDelim(line, ',');

			CParams::defaultResourcePositions[i].x = atoi(tokens[0].c_str());
			CParams::defaultResourcePositions[i].y = atoi(tokens[1].c_str());
		}

		getline(myfile, line);  	// Catch the newline
		getline(myfile, line);		// Catch the description line

		for (int i = 0; i < numSetDefaultPositions; i++)
		{
			getline(myfile, line);     // Get the next param line
			tokens = splitByDelim(line, ',');

			CParams::setResourcePositions.push_back(SVector2D(atoi(tokens[0].c_str()), atoi(tokens[1].c_str())));
		}

		// CONTROLLER values
		int numberOfGroups = setControllerVariables(&myfile);

		// PATCHES values
		setPatchVariables(&myfile);

		// RESOURCES values
		setResourceVariables(&myfile);

		// GROUP values
		setGroupVariables(&myfile, numberOfGroups);

		myfile.close();
	}
	else
	{
		cout << "Unable to open file";
	}
}

/**
*  Write a save point of all the data in the simulations current state
*/
void CController::writeSnapshot()
{
	// Generate the folderName and create the directory
	string folderName = CParams::resetFileName + "/" + std::to_string(CParams::simulationRunId) + "/";

	createDir(folderName);

	string fileName =
#ifdef _WINDOWS 
		"./"
#else
		"../"
#endif
		+ folderName + std::to_string(snapshotStatisticCount) + ".txt";

	// Print all the information to file
	fstream to_file2(fileName, ios::out);
	to_file2 << *this;

	snapshotStatisticCount++;
}

void CController::writeStats(int mode, std::ofstream & to_file)
{
	if (mode == 1)
	{
		writeSnapshot();
	}
	if (mode == 2)
	{
		using namespace statsOutstream;
		for (Group g : groupVector)
			to_file << g;
	}
	if (mode == 3)
	{
		string folderName = CParams::resetFileName + "/" + std::to_string(CParams::simulationRunId) + "/";
		createDir(folderName);
		string fileName = "./" + folderName + "jacob.out";
		using namespace statsOutstream;
		std::ofstream target(fileName, ios::app | ios::out);
		for (Group & group : groupVector)
		{
			target << group.groupId << " " << group.degreeOfCooperation;
			int count = 0;
			for (Agent & agent : group.agentVector)
			{
				target << " " << agent.getCollected();
				count++;
			}
			if (count < CParams::iNumSweepers / 2)
			{
				for (int i = 0; i < ((CParams::iNumSweepers / 2) - count) ; ++i)
					target << " " << 0;
			}
			target << endl;
		}
	}
}
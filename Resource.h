#ifndef RESOURCE_H
#define RESOURCE_H

//!  Resource class.
/*!
     Constructors, getters, and setters for individual resource objects.

	 Resources are placed in the environment for agents to collect. They give agents the energy required for movement,
	 and provide the basis for ascertaining the fitness of agents (for selection purposes). Resources can be grouped
	 into patches, the number and density of which indirectly alter the level of inter-agent and inter-group competition.
*/

#include <vector>
#include <math.h>

#include "CNeuralNet.h"
#include "utils.h"
#include "C2DMatrix.h"
#include "SVector2D.h"
#include "CParams.h"

using namespace std;

class Resource
{
private:
	SVector2D position;     // Position of this resource
	int index;              // Index into resource vector of this resource
	int inactiveTickCount ; // Counter to store the number of ticks the resource has been inactive for
	bool active;            // Whether or not this resource has been picked up

public:
	/**
	*  Default Constructor
	*/
ALSB_SIX_MEMBER_FUNCTIONS(Resource )
	/**
	*  Constructor
	*
	*  @param SVector2D:    xy coordinates where this resource should be placed
	*  @param int:          index pointing to this resource in the global resources vector
	*/
	Resource(SVector2D position_, int index_);	

	/**
	*  Constructor
	*
	* @param double:   x coord of this resource
	* @param double:   y coord of this resource
	*/
	Resource(double x, double y);
	
	/**
	*  Operator overload to allow saving data to file
	*/
	friend ostream& operator<<(ostream&, const Resource&);

	//---------------------------- Setters ----------------------------------------------
	/**
	*  Set position of a single resource to a given SVector2D.
	*
	*  @param SVector2D:   desired position
	*/
	void setPosition(SVector2D position_);

	/**
	*  Set position of a single resource to given xy coords.
	*
	*  @param double:   desired x coord
	*  @param double:   desired y coord
	*/
	void setPosition(double x, double y);

	/**
	*  Set the status of a resource to active/inactive (not picked up/picked up)
	*
	*  @param bool:   TRUE for not picked up, FALSE for picked up
	*/
	void setActiveStatus(bool activeStatus);

	/**
	*  Increments the count of currently inactive resources.
	*/
	void incrementInactiveTickCount();

	/**
	*  Resets the count of currently inactive resources to 0.
	*/
	void resetInactiveTickCount();


	//---------------------------- Getters ----------------------------------------------
	/**
	*  Get SVector2D position of this resource.
	*
	*  @retval SVector2D:   position of this resource
	*/
	SVector2D getPosition();

	/**
	*  Get index into global resources vector corresponding to this resource.
	*
	*  @retval int:   index of this resource in resources vector
	*/
	int getIndex();

	/**
	*  Get number of ticks for which this resource has been inactive (used to determine when to regrow)
	*
	*  @retval int:   number of ticks for which this resource has been inactive
	*/
	int getInactiveTickCount();

	/**
	*  Get the active status of this resource.
	*
	*  @retval bool:   TRUE if active, FALSE if inactive
	*/
	bool isActive();
};


#endif



#ifndef KDNODE_H
#define KDNODE_H

//!  KD Node class.
/*!
     These nodes form a KD tree that is used to store all resource information - necessary for efficiency of resource lookup performed by agents.
*/

#include <vector>
#include <math.h>

#include "SVector2D.h"
#include "Resource.h"
#include "CParams.h"

using namespace std;

class KdNode
{
private:

public:
	Resource * resource;       // pointer to a resource
	KdNode * left;             // pointer to left and right nodes
	KdNode * right;
	int numActive = 0;         // count of active nodes
	bool preOrderBool = false; // order flag

	/**
	*  Default constructor.
	*/
    ALSB_SIX_MEMBER_FUNCTIONS( KdNode )

	/**
	*  Constructor.
	*
	*  @param Resource:    sets the resource object stored in this node
	*/
	KdNode(Resource * resource_);
	
	/**
	*  Finds the median.
	*
	*  @param KdNode*:    start point
	*  @param KdNode*:    end point
	*  @param int:        index
	*  @retval KdNode*:   median node
	*/
	KdNode * find_median(KdNode *start, KdNode *end, int idx);

	/**
	*  Makes the tree.
	*
	*  @param KdNode*:     array of KdNode objects for each Resource
	*  @param int:         number of Nodes
	*  @param int:         the current Dimension
	*  @param int:         the number of dimensions
	*  @retval KdNode*:    root of resultant KdTree
	*/
	KdNode * make_tree(KdNode *t, int len, int i, int dim);

	/**
	*  Swap two nodes
	*
	*  @param KdNode*:   first node to be swapped
	*  @param KdNode*:   second node to be swapped
	*/
	void swap(KdNode *x, KdNode *y);

	/**
	*  Gets the nearest resource node to an agent.
	*
	*  @param KdNode*:          root of the KD tree of resources
	*  @param KdNode*:          agent position relative to resources
	*  @param int:              current Dimension
	*  @param int:              total number of dimensions
	*  @param KdNode**:         closest resource
	*  @param double*:          distance to closets resource
	*  @param SVector2D:        position of an agent
	*/
	void nearest(KdNode *& root, KdNode *& agentPosition, int i, int dim,
		KdNode *& best, double &best_dist, SVector2D agentPos);

	/**
	*  Return distance between two nodes.
	*
	*  @param KdNode*:   first node
	*  @param KdNode*:   second node
	*  @param int dim:   total number of dimensions
	*  @retval double:   distance between first and second node
	*/
	double dist(KdNode *a, KdNode *b, int dim);

	/**
	*  Prints out a preOrder traversal of the tree.
	* 
	*  @param KdNode*:       root of KD tree of resources
	*  @param bool:          print out if TRUE
	*  @param SVector2D:     current agent position
	*  @param string:        spacing to be used during printing
	*  @retval int:          number of active resources if root is equal to NULL
	*/
	int preOrder(KdNode * root, bool print, SVector2D agentPost, std::string tabAmount);
};
#endif


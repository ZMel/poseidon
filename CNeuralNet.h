#ifndef CNEURALNET_H_
#define CNEURALNET_H_

//!  Neural Network Class
/*!
     Defines the structure and methods associated with the "brain" that drives the behaviour of every agent.

	 This is a fully connected, fixed topology, feed-forward neural network.
*/

#include <vector>
#include <cmath>
#include <algorithm>
#include <stdlib.h>
#include <cstring>
#include <stdio.h>
#include <stdint.h>
#include "CParams.h"

#define SIGMOID_ACTIVATION      1.0


typedef unsigned int uint;
class EA; //forward declare EA class, which will have the power to access weight vectors
struct Group;
class CController;
class CNeuralNet
{
    friend class EA; 
    friend struct Group;
    friend class CController;
private:
    double * _network ; 


	/**
	*  Sigmoid function used during the feedforward process.
	* 
	*  @param[out] double: input into one node in the ANN
	*/
    void sigmoid( double & netinput ) ;
    
    /**
	*  Feeds inputs through the ANN to get two outputs
	*
	*  @param double*:      inputs to the ANN
    *  @param[out] double*: outputs of the ANN 
	*/
	void feedForward(double * inputs, double *& outputs);

	/**
	*  Initialize a new blank ANN (all connection weights set to null)
	*
	*  @param double*:      inputs to the ANN
	*  @param[out] double*: outputs of the ANN
	*/
    void initWeights ( double * weights=nullptr);

public:
    const uint _iInput;      
    const uint _iHidden ; 
    const uint _iNumHidden; 
    const uint _iOutput;     
	
    
    /**
     * Gets the size of the neural network
     * 
     * @retval uint:  the size of the neural (number of connection weights) network given its dimensions
     */
    uint size () const;

    /**
	*  Normal constructor. ANN with random weights.
	*
	*  @param uint:  number of nodes in the input layer
	*  @param uint:  number of nodes in the hidden layer
	*  @param uint:  number of nodes in the output layer
	*/
	CNeuralNet(uint inputLayerSize, uint hiddenLayerSize, uint outputLayerSize);

	/**
	*  Blank constructor. ANN with weights set to zero.
	*
	*  @param uint:      number of nodes in the input layer
	*  @param uint:      number of nodes in each hidden layer
    *  @param uint:      number of hidden layers
	*  @param uint:      number of nodes in the output layer
	*  @param bool:      for differentiation
	*/
	CNeuralNet(uint inputLayerSize, uint hiddenLayerSize, uint numHiddenLayers, uint outputLayerSize);

    ALSB_SIX_MEMBER_FUNCTIONS( CNeuralNet )
    	
    /**
	*  Snapshot reset constructor.
	*
	*  @param int:             number of nodes in the input layer
	*  @param int:             number of nodes in the hidden layer
	*  @param int:             number of nodes in the output layer
	*  @param vector<double>:  vector of ANN weights
	*/
	CNeuralNet(int inputLayerSize, int hiddenLayerSize, int outputLayerSize, double * ann);

	/**
	*  Feeds inputs through ann to calculate output.
	*
	*  @param vector<double>:       vector containing the two inputs to the ANN
	*  @retval uint:                index of larger output from the ANN
	*/
	uint classify(double * input);

	/**
	*  Get index of largest output.
	*  
	*  @param uint:         index of largest output
	*  @retval double:      value of largest output
	*/
	double getOutput(uint index) const;


	/**
	*  Check if this agent ann is related to another one.
	*
	*  @param CNeuralNet:   ANN of another agent
	*  @retval bool:        TRUE if ANNs are related, FALSE otherwise
	*/
	bool areRelated(CNeuralNet * otherAgent);

	/**
	*  Calculate the relatedness value (%) of this agent and another one.
	*
	*  @param CNeuralNet*:    ANN of another agent
	*  @retval double:       value showing how related the two ANNs are
	*/
	double calculateRelatedness(CNeuralNet * otherAgent);

	/**
	*  Operator overload to allow saving data to file.
	*/
	friend ostream& operator<<(ostream&, const CNeuralNet&);
};
#endif /* CNEURALNET_H_ */

#include "EA.h"

bool EA::fitnessComparison(Agent * parent1, Agent * parent2)
{
	return (parent1->getEnergy() > parent2->getEnergy());
}

void EA::withinGroupSelection(Group & group, int numberOfChildren, int modeChoice)
{
	std::vector<Agent> tempAgentVector = group.agentVector;
	group.agentVector.clear();
	std::vector<Agent> children;

	// sort the agents in the group by energy values (lowest to highest)
	std::sort(tempAgentVector.begin(), tempAgentVector.end());

	// rank based parent selection and child production
	rankBased(tempAgentVector, children, numberOfChildren);

	SVector2D groupCentroid;
	groupCentroid.x = Utilities::getInteger(CParams::WindowHeight - 100, 100);
	groupCentroid.y = Utilities::getInteger(CParams::WindowHeight - 100, 100);

	// give new children all the necessary values
	for (int i = 0; i < children.size(); ++i)
	{
		SVector2D position;
		if (modeChoice == 3)
		{
				position.x = groupCentroid.x + Utilities::getInteger(50, 0);
				position.y = groupCentroid.y + Utilities::getInteger(50, 0);
		}
		else
		{
			position.x = Utilities::getDouble(CParams::WindowWidth);
			position.y = Utilities::getDouble(CParams::WindowHeight);
		}
		children[i].setPosition(position);
		children[i].groupNumber = group.groupId;
	}

	// reinitialize the group with the new children and values
	group.agentVector = children;
}

void EA::rankBased(std::vector<Agent> & oldPop, std::vector<Agent> & children, int numberOfChildren)
{
	std::vector<Agent> parents;
	double nPlus = CParams::selectionPressure;
	double nMinus = 2 - nPlus;
	double N = (double) oldPop.size();

	for (int i = 0; i < CParams::iNumSweepers / 2; ++i)
	{
		parents.clear();

		while (parents.size() < 2)
		{
			for (int j = 0; j < N; ++j)
			{
				double probability = (1.0 / N) * (nMinus + (nPlus - nMinus) * (((double) j) / (N - 1))); // linear rank based
				if (Utilities::getDouble() < probability)
				{
					parents.push_back(oldPop[j]);
				}
			}
		}
	
		for (size_t i = 0; i < numberOfChildren; i++)
		{
			Agent * parent1 = &(parents[0]);
			Agent * parent2 = &(parents[1]);

			// produce children from selected parents
			Agent child;
			CNeuralNet brain;
			crossover(parent2->getBrain(), parent1->getBrain(), brain);
			mutate(brain, CParams::dMutationRate, CParams::dMaxPerturbation);
			child.setBrain(brain);
			children.push_back(child);
		}
	}
}

void EA::rankBased(std::vector<Group> & groupVector, std::vector<Agent> & _children, int numberOfChildren)
{
	int numberOfParents = 0;
	std::vector<int> parents;
	std::vector<Agent> children;

	// Join all agents into 1 vector
	std::vector<Agent> allAgents;
	for (Group & group : groupVector)
	{
		for (Agent & agent : group.agentVector)
		{
			allAgents.push_back(agent);
		}
	}

	// sort the agents in the group by energy values (lowest to highest)
	std::sort(allAgents.begin(), allAgents.end());


	// Rank selection equation variables
	double nPlus = CParams::selectionPressure;
	double nMinus = 2 - nPlus;
	double N = (double)allAgents.size();

	for (size_t i = 0; i < allAgents.size(); i++)
	{
		parents.clear();

		while (parents.size() < 2)
		{
			for (int j = 0; j < N; ++j)
			{
				double probability = (1.0 / N) * (nMinus + (nPlus - nMinus) * (((double)j) / (N - 1))); // linear rank based
				if (Utilities::getDouble() < probability)
				{
					// If second parent is the same as the first
					if (parents.size() == 1)
					{
						if (parents[0] == j)
						{
							numberOfParents--;
							break;
						}
					}

					parents.push_back(j);
					break;
				}
			}
		}

		// Make the children
		double selectionChance = Utilities::getDouble(1.0, 0.0);
		bool inRange = true;

		// If local selection then calculate distances
		if (CParams::localSelection)
		{
			inRange = QuickVec2DLength(allAgents[parents[0]].getPosition() - allAgents[parents[1]].getPosition()) <= CParams::selectionDistance;
		}

		// If agents are in selection range and related enough to reproduce
		if (inRange && selectionChance <= allAgents[parents[0]].getBrain()->calculateRelatedness(allAgents[parents[1]].getBrain()))
		{
			// Create 4 children per parent pair
			for (size_t j = 0; j < numberOfChildren; j++)
			{
				// produce children from selected parents
				Agent child;
				CNeuralNet brain;
				double energy = allAgents[parents[0]].getEnergy() + allAgents[parents[1]].getEnergy();

				crossover(allAgents[parents[1]].getBrain(), allAgents[parents[0]].getBrain(), brain);
				mutate(brain, CParams::dMutationRate, CParams::dMaxPerturbation);
				child.setBrain(brain);
				child.setEnergy(energy / 2);
				children.push_back(child);
			}
		}
	}

	// Generate the new groups based on the children created
	groupGeneration(children, groupVector);

	_children = children;
}

void  EA::groupGeneration(std::vector<Agent> & agents, std::vector<Group> & groupVector)
{
	// Remove all the existing agents from the group vector
	for (size_t i = 0; i < groupVector.size(); i++)
	{
		groupVector[i].agentVector.clear();
	}

	// Iterate through all the agents
	for (size_t j = 0; j < agents.size(); j++)
	{
		bool relatedToGroup = false;
		// Iterate through groups and check if agent falls under any
		for (size_t i = 0; i < groupVector.size(); i++)
		{
			// Check if agent related to group average ANN
			if (groupVector[i].representativeANN->areRelated(agents[j].getBrain()))
			{
				// If group cant give exactly 1 energy unit to each child, then child dies
				if ((int)groupVector[i].energy % (groupVector[i].agentVector.size() + 1) != (int)groupVector[i].energy)
				{
					agents[j].groupNumber = i;
					groupVector[i].agentVector.push_back(agents[j]);
				}else
				{
					// Remove the agent from the vector so their ANN doesnt get calculate for population diversity
					agents.erase(agents.begin() + j);
					j--;
				}

				// Mark that agent has been added to existing group
				relatedToGroup = true;

				// Group found so break out of inner loop
				break;
			}
		}

		if (!relatedToGroup)
		{
			int r = Utilities::getInteger(200);
			int g = Utilities::getInteger(200);
			int b = Utilities::getInteger(200);

			// Create the group
			int newGroupId = groupVector.size();
			Group group(newGroupId, agents[j].getEnergy(), r, g, b);

			// Pick a random position on the grid for the group
			SVector2D position;

			// Sets a random agenrection to move to
			position.x = Utilities::getInteger(800);
			position.y = Utilities::getInteger(800);

			agents[j].setPosition(position);
			agents[j].groupNumber = newGroupId;
			group.agentVector.push_back(agents[j]);

			// Calculate the representative ANN for the group for comparison
			group.calculateRepresentativeANN();
			groupVector.push_back(group);
		}
	}
}
void EA::crossover(CNeuralNet * genotypeA, CNeuralNet * genotypeB, CNeuralNet & out)
{
	out = CNeuralNet(CParams::iNumInputs, CParams::iNeuronsPerHiddenLayer, CParams::iNumHidden, CParams::iNumOutputs);
	uint numFromA = Utilities::getInteger(out.size());

	for (uint i = 0; i < out.size(); ++i)
	{
		if (i < numFromA)
		{
			out._network[i] = genotypeA->_network[i];
		}
		else
		{
			out._network[i] = genotypeB->_network[i];
		}
	}
}

void EA::mutate(CNeuralNet & genotypeA, double mutationRate, double perturbationAmt)
{
	for (uint i = 0; i < genotypeA.size(); ++i)
	{
		if (Utilities::getDouble () <= mutationRate)
		{
			genotypeA._network[i] += Utilities::getDouble(perturbationAmt, -perturbationAmt);
            Clamp ( genotypeA._network[i] , -1.0, 1.0);
		}
	}
}


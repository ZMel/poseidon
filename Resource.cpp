#include "Resource.h"

Resource::Resource()
{
	// Create random start position
	position = SVector2D(Utilities::getDouble( CParams::WindowWidth ),
		Utilities::getDouble( CParams::WindowHeight) );
	active = true;
    index = -1;
    inactiveTickCount = 0;
}

Resource::Resource( const Resource & other )
    : position( other.position ),
    index ( other.index), 
    inactiveTickCount( other.inactiveTickCount ), 
    active ( other.active )
{}

Resource::Resource( Resource && other )
    : position ( other.position ), 
    index( other.index ),
    inactiveTickCount ( other.inactiveTickCount ),
    active( other.active )
{}

Resource::~Resource () 
{
    index = -1;
}

Resource& Resource::operator=( const Resource & other )
{
    if ( this != &other )
    {
        position = other.position ;
        index = other.index;
        inactiveTickCount = other.inactiveTickCount;
        active = other.active;
    }
    return *this;
}

Resource& Resource::operator=( Resource && other )
{
    if ( this != &other )
    {
        position = other.position;
        index = other.index;
        inactiveTickCount = other.inactiveTickCount;
        active = other.active;
    }
    return *this;
}

Resource::Resource(SVector2D position_, int index_) :
position(position_), index(index_), inactiveTickCount(0), active(true)
{
	active = true;
}

Resource::Resource(double x, double y) : position(SVector2D(x, y)), inactiveTickCount(0), active(true)
{}

ostream& operator<<(ostream& out, const Resource & resource)
{
	out << resource.position.x
		<< "," << resource.position.y
		<< "," << resource.index;

	return out;
}

void Resource::setPosition(SVector2D position_)
{ 
    position = position_; 
}

void Resource::setPosition(double x, double y)
{ 
    position.x = x; position.y = y;
}

void Resource::setActiveStatus(bool lifeStatus)
{ 
    active = lifeStatus; 
}

void Resource::incrementInactiveTickCount()
{ 
    inactiveTickCount++; 
}

void Resource::resetInactiveTickCount()
{ 
    inactiveTickCount = 0; 
}

bool Resource::isActive()
{ 
    return active; 
}

int Resource::getIndex()
{ 
    return index; 
}

int Resource::getInactiveTickCount()
{ 
    return inactiveTickCount; 
}

SVector2D Resource::getPosition()
{ 
    return position; 
}

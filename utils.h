#ifndef UTILS_H
#define UTILS_H

#include <stdlib.h>
#include <math.h>
#include <sstream>
#include <string>
#include <iostream>
#include <vector>
#include <random>
#include <climits>
#include <cfloat>
using namespace std;

//----------------------------------------------------------------------------
//	some random number functions.
//----------------------------------------------------------------------------

//-----------------------------------------------------------------------
//	
//	some handy little functions
//-----------------------------------------------------------------------

//converts an integer to a std::string
string itos(int arg);

//converts an float to a std::string
string ftos (float arg);

//	clamps the first argument between the second two
void Clamp(double &arg, double min, double max);

/////////////////////////////////////////////////////////////////////
//
//	Point structure
//
/////////////////////////////////////////////////////////////////////
struct SPoint
{
	float x, y;
	
	SPoint(){}
	SPoint(float a, float b):x(a),y(b){}
};

class Utilities
{
private:

	double dPi = 3.14159265358979;

	Utilities();
	Utilities(const Utilities &); // copy constructor
	Utilities & operator=(const Utilities &);
	
	std::default_random_engine generator;

	std::uniform_real_distribution<double> d_dist;
	std::cauchy_distribution<double> c_dist;
	std::uniform_int_distribution<int> i_dist;

	static Utilities * getInstance();
	static Utilities * __utilities; 
	double __getDouble(const double end = 1.0, const double start = 0.0);
	double __getCauchyDouble(const double end = 1.0, const double start = 0.0);
	int	__getInteger(const int end = 1, const int start = 0);
public:
	static double getDouble( const double end = 1.0, const double start = 0.0 );
	static double getCauchyDouble(const double end = 1.0, const double start = 0.0);
	static int getInteger( const int end = 1, const int start = 0);
	static void destroyInstance();
	static double scale(double input, double max);
	static double normalize(double input, double min, double max);
};


/**
*  Used to split a string by a deliminator
*
*  @param string:          line to split
*  @param char:            char to use to split line
*  @retval vector<string>: vector of strings resulting from splitting
*/
std::vector<string> splitByDelim(string line, char delim);

/**
 * The declaration of the six member functions
 */
#define ALSB_SIX_MEMBER_FUNCTIONS(__CLASSNAME) \
    __CLASSNAME(); \
    __CLASSNAME( const __CLASSNAME & ); \
    __CLASSNAME( __CLASSNAME && ); \
    __CLASSNAME& operator=( const __CLASSNAME & ); \
    __CLASSNAME& operator=( __CLASSNAME && ); \
    ~ __CLASSNAME();


#endif

#include "CParams.h"

double CParams::dPi = 3.14159265358979;
double CParams::dHalfPi = dPi / 2;
double CParams::dTwoPi = dPi * 2;
int CParams::WindowWidth = 800;
int CParams::WindowHeight = 800;
int CParams::iFramesPerSecond = 0;
int CParams::iNumInputs = 0;
int CParams::iNumHidden = 0;
int CParams::iNeuronsPerHiddenLayer = 0;
int CParams::iNumOutputs = 0;
double CParams::dActivationResponse = 0;
double CParams::dBias = 0;
double CParams::dEnergyCostPerTick = 0;
double CParams::dMaxTurnRate = 0;
double CParams::dMaxSpeed = 0;
double CParams::dSweeperScale = 0;
double CParams::dStartEnergy = 0;
int CParams::iNumSweepers = 0;
int CParams::iNumMines = 0;
int CParams::iNumTicks = 0;
double CParams::dMineScale = 0;
double CParams::density = 0;
double CParams::numPatches = 0;
double CParams::patchWidth = 0;
double CParams::dCrossoverRate = 0;
double CParams::dMutationRate = 0;
double CParams::dMaxPerturbation = 0;
int CParams::iNumElite = 0;
int CParams::iNumCopiesElite = 0;
double CParams::relatedThreshold = 0;
double CParams::geneRelatedThreshold = 0;
int CParams::energyGainedPerResource = 0;
double CParams::groupStartingEnergy = 0;
double CParams::willMove = 0;
double CParams::fovRadius = 0;
int CParams::selectionDistance = 0;
int CParams::localSelection = 0;
double CParams::tournamentSize = 0;
int CParams::numberOfChildren_perTournamentPair = 0;
double CParams::selectionPressure = 0;
int CParams::isTournamentSelection = 0;
int	CParams::endGenerationNo = 0;
double CParams::resourceGrowthRate = 0;
int CParams::simulationRunId = 0;
string CParams::resetFileName;
vector<SVector2D> CParams::defaultResourcePositions;
vector<SVector2D> CParams::setResourcePositions;
string CParams::sOutFilename(
#ifndef _WINDOWS
        "../"
#else
        "./"
#endif
        "out/results.out");
int CParams::iNumAgents[2];
string CParams::sOutFilename2("./results.out");

//this function loads in the parameters from a given file name. Returns
//false if there is a problem opening the file.
bool CParams::LoadInParameters(std::string szFileName)
{
	ifstream grab(szFileName);
	string temp = "";

	//check file exists
	if (!grab)
	{
		return false;
	}

	//load in from the file
	char ParamDescription[40];

	grab >> ParamDescription;
	grab >> iFramesPerSecond;
	grab >> ParamDescription;
	grab >> iNumInputs;
	grab >> ParamDescription;
	grab >> iNumHidden;
	grab >> ParamDescription;
	grab >> iNeuronsPerHiddenLayer;
	grab >> ParamDescription;
	grab >> iNumOutputs;
	grab >> ParamDescription;
	grab >> dActivationResponse;
	grab >> ParamDescription;
	grab >> dBias;
	grab >> ParamDescription;
	grab >> dEnergyCostPerTick;
	grab >> ParamDescription;
	grab >> dMaxTurnRate;
	grab >> ParamDescription;
	grab >> dMaxSpeed;
	grab >> ParamDescription;
	grab >> dSweeperScale;
	grab >> ParamDescription;
	grab >> dStartEnergy;
	grab >> ParamDescription;
	grab >> iNumMines;
	grab >> ParamDescription;
	grab >> iNumSweepers;
	grab >> ParamDescription;
	grab >> iNumTicks;
	grab >> ParamDescription;
	grab >> dMineScale;
	grab >> ParamDescription;
	grab >> density;
	grab >> ParamDescription;
	grab >> numPatches;
	grab >> ParamDescription;
	grab >> patchWidth;
	grab >> ParamDescription;
	grab >> dCrossoverRate;
	grab >> ParamDescription;
	grab >> dMutationRate;
	grab >> ParamDescription;
	grab >> dMaxPerturbation;
	grab >> ParamDescription;
	grab >> iNumElite;
	grab >> ParamDescription;
	grab >> iNumCopiesElite;
	grab >> ParamDescription;
	grab >> relatedThreshold;
	grab >> ParamDescription;
	grab >> geneRelatedThreshold;
	grab >> ParamDescription;
	grab >> energyGainedPerResource;
	grab >> ParamDescription;
	grab >> groupStartingEnergy;
	grab >> ParamDescription;
	grab >> willMove;
	grab >> ParamDescription;
	grab >> fovRadius;
	grab >> ParamDescription;
	grab >> selectionDistance;
	grab >> ParamDescription;
	grab >> localSelection;
	grab >> ParamDescription;
	grab >> tournamentSize;
	grab >> ParamDescription;
	grab >> numberOfChildren_perTournamentPair;
	grab >> ParamDescription;
	grab >> selectionPressure;
	grab >> ParamDescription;
	grab >> isTournamentSelection;
	grab >> ParamDescription;
	grab >> endGenerationNo;
	grab >> ParamDescription;
	grab >> resourceGrowthRate;
	grab >> ParamDescription;
	grab >> simulationRunId;
	grab >> ParamDescription;
	grab >> resetFileName;

	// Pull all the default resource positions
	for (int i = 0; i < 10; ++i)
	{
		float x;
		float y;
		grab >> ParamDescription;
		grab >> x;
		grab >> y;
		defaultResourcePositions.push_back(SVector2D(x, y));
	}

	grab >> ParamDescription;
	for (int i = 0; i < 2; ++i)
		grab >> iNumAgents[i];

	return true;
}

void CParams::readInParameters(vector<string> tokens)
{
	// Set all of the parameters from the reset file
	dPi = atof(tokens[0].c_str());
	dHalfPi = atof(tokens[1].c_str());
	dTwoPi = atof(tokens[2].c_str());
	WindowWidth = atoi(tokens[3].c_str());
	WindowHeight = atoi(tokens[4].c_str());
	iFramesPerSecond = atoi(tokens[5].c_str());
	iNumInputs = atoi(tokens[6].c_str());
	iNumHidden = atoi(tokens[7].c_str());
	iNeuronsPerHiddenLayer = atoi(tokens[8].c_str());
	iNumOutputs = atoi(tokens[9].c_str());
	dActivationResponse = atof(tokens[10].c_str());
	dBias = atof(tokens[11].c_str());
	dEnergyCostPerTick = atof(tokens[12].c_str());
	dMaxTurnRate = atof(tokens[13].c_str());
	dMaxSpeed = atof(tokens[14].c_str());
	dSweeperScale = atof(tokens[15].c_str());
	dStartEnergy = atof(tokens[16].c_str());
	iNumSweepers = atoi(tokens[17].c_str());
	iNumMines = atoi(tokens[18].c_str());
	iNumTicks = atoi(tokens[19].c_str());
	dMineScale = atof(tokens[20].c_str());
	density = atof(tokens[21].c_str());
	numPatches = atof(tokens[22].c_str());
	patchWidth = atof(tokens[23].c_str());
	dCrossoverRate = atof(tokens[24].c_str());
	dMutationRate = atof(tokens[25].c_str());
	dMaxPerturbation = atof(tokens[26].c_str());
	iNumElite = atoi(tokens[27].c_str());
	iNumCopiesElite = atoi(tokens[28].c_str());
	relatedThreshold = atof(tokens[29].c_str());
	geneRelatedThreshold = atof(tokens[30].c_str());
	energyGainedPerResource = atoi(tokens[31].c_str());
	groupStartingEnergy = atof(tokens[32].c_str());
	willMove = atof(tokens[33].c_str());
	fovRadius = atof(tokens[34].c_str());
	selectionDistance = atoi(tokens[35].c_str());
	localSelection = atoi(tokens[36].c_str());
	tournamentSize = atof(tokens[37].c_str());
	numberOfChildren_perTournamentPair = atoi(tokens[38].c_str());
	selectionPressure = atof(tokens[39].c_str());
	isTournamentSelection = atoi(tokens[40].c_str());
	endGenerationNo = atoi(tokens[41].c_str());
	resourceGrowthRate = atoi(tokens[42].c_str());
	simulationRunId = atoi(tokens[43].c_str());
	resetFileName = tokens[44];
	sOutFilename = tokens[45];
}

string CParams::returnParamsString()
{
	string out = "Params: \n";

	out += std::to_string(CParams::dPi) + "," + std::to_string(CParams::dHalfPi) + "," + std::to_string(CParams::dTwoPi) + "," + std::to_string(CParams::WindowWidth)
		+ "," + std::to_string(CParams::WindowHeight) + "," + std::to_string(CParams::iFramesPerSecond) + "," + std::to_string(CParams::iNumInputs)
		+ "," + std::to_string(CParams::iNumHidden) + "," + std::to_string(CParams::iNeuronsPerHiddenLayer) + "," + std::to_string(CParams::iNumOutputs)
		+ "," + std::to_string(CParams::dActivationResponse) + "," + std::to_string(CParams::dBias) + "," + std::to_string(CParams::dEnergyCostPerTick)
		+ "," + std::to_string(CParams::dMaxTurnRate) + "," + std::to_string(CParams::dMaxSpeed) + "," + std::to_string(CParams::dSweeperScale)
		+ "," + std::to_string(CParams::dStartEnergy) + "," + std::to_string(CParams::iNumSweepers) + "," + std::to_string(CParams::iNumMines)
		+ "," + std::to_string(CParams::iNumTicks) + "," + std::to_string(CParams::dMineScale) + "," + std::to_string(CParams::density) + "," + std::to_string(CParams::numPatches)
		+ "," + std::to_string(CParams::patchWidth) + "," + std::to_string(CParams::dCrossoverRate) + "," + std::to_string(CParams::dMutationRate)
		+ "," + std::to_string(CParams::dMaxPerturbation) + "," + std::to_string(CParams::iNumElite) + "," + std::to_string(CParams::iNumCopiesElite)
		+ "," + std::to_string(CParams::relatedThreshold) + "," + std::to_string(CParams::geneRelatedThreshold) + "," + std::to_string(CParams::energyGainedPerResource)
		+ "," + std::to_string(CParams::groupStartingEnergy)
		+ "," + std::to_string(CParams::willMove) + "," + std::to_string(CParams::fovRadius) + "," + std::to_string(CParams::selectionDistance) + "," + std::to_string(CParams::localSelection)
		+ "," + std::to_string(CParams::tournamentSize) + "," + std::to_string(CParams::numberOfChildren_perTournamentPair) + "," + std::to_string(CParams::selectionPressure)
		+ "," + std::to_string(CParams::isTournamentSelection) + "," + std::to_string(CParams::endGenerationNo) + "," + std::to_string(CParams::resourceGrowthRate)
		+ "," + std::to_string(CParams::simulationRunId) + "," + CParams::resetFileName + "," + CParams::sOutFilename
		+ "," + std::to_string(CParams::defaultResourcePositions.size()) + "," + std::to_string(CParams::setResourcePositions.size()) + "\n";

	out += "\nDefault Resource Positions: \n";

	for (size_t i = 0; i < CParams::defaultResourcePositions.size(); i++)
	{
		out += std::to_string(CParams::defaultResourcePositions[i].x) + "," + std::to_string(CParams::defaultResourcePositions[i].y) + "\n";
	}

	out += "\nThe Set Resource Positions: \n";

	for (size_t i = 0; i < CParams::setResourcePositions.size(); i++)
	{
		out += std::to_string(CParams::setResourcePositions[i].x) + "," + std::to_string(CParams::setResourcePositions[i].y) + "\n";
	}

	return out;
}


void CParams::writeResetFileId(string fileName)
{
	string line;
	ifstream readFile(fileName);
	vector<string> data;

	if (readFile.is_open())
	{
		getline(readFile, line);     // Get the next param line
		vector<string> tokens = splitByDelim(line, ' ');

		// Continuing reading until we find the Id value
		while (tokens[0] != "simulationRunId")
		{
			data.push_back(line);
			getline(readFile, line);     // Get the next param line
			tokens = splitByDelim(line, ' ');
		}

		string temp = tokens[0] + " " + std::to_string(CParams::simulationRunId);
		data.push_back(temp);

		// Finishing reading the rest of the file
		getline(readFile, line);     // Get the next param line
		tokens = splitByDelim(line, ' ');
		
		while (tokens[0] != "end")
		{
			data.push_back(line);
			getline(readFile, line);     // Get the next param line
			tokens = splitByDelim(line, ' ');
		}

		data.push_back("end");

		readFile.close();
	}
	else{
		cout << "Unable to open file";
	}

	// Now write the new data vector to the file
	ofstream outputFile(fileName);

	for (size_t i = 0; i < data.size() - 1; i++)
	{
		outputFile << data[i] << std::endl;
	}

	outputFile << data[data.size() - 1];

	outputFile.close();
}




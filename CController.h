#ifndef CCONTROLLER_H
#define CCONTROLLER_H

//!  Main simulation controller class.
/*!
     Responsible for overall functioning of simulation. Updates agents and resources, controls ticks and generations.
*/

#include <vector>
#include <sstream>
#include <string>
#include <iostream>

#include <ostream>
#include <fstream>
#include <algorithm>    
#include <iterator>     
#include <regex>     
#include <ctime>
#include <chrono> 
#include <stdio.h>
#include <iomanip>
#include <set>

#include "Agent.h"
#include "CGenAlg.h"
#include "utils.h"
#include "C2DMatrix.h"
#include "SVector2D.h"
#include "CParams.h"
#include "Resource.h"
#include "EA.h"
#include "KdNode.h"

#ifndef _WINDOWS
typedef int* H_WINDOW;
#else
typedef HWND H_WINDOW;
#endif

#if defined _WINDOWS
#include <direct.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>

/**
*  Struct to define a path of resources
*/
struct ResourcePatch
{
	SVector2D topLeft;                                // corer of this resource patch
	double width;                                     // width of the patch
	ResourcePatch(SVector2D topLeft_, double width_); // constructor

	/**
	* Operator overload to allow saving data to file
	*/
	friend ostream& operator<<(ostream&, const ResourcePatch&);
};

/**
*  Struct to define a group of agents
*/
struct Group
{
	int r = 0;                             // RGB Colour values - determine colour of this group
	int g = 0;
	int b = 0;
	int groupId;                           // Identification number of this particular group
	int numberOfStartingAgents;		       // Stores the number of agents allocated to the group at the start of a generation
	int numberResourcesGathered;           // Number of resources gathered by this group
	int numberOfCooperativeAgents;         // How many agents in this group have collected a resource
	double groupDiversity;
	double intraGroupCooperation;	       // Measure the proportion of cooperative agents within a group, represents intra-group cooperation
	double degreeOfCooperation;		       // Used to measure the degree of cooperation of agents within a group
	double energy;                         // Total amount of energy that this group has
	double averageRelatedness;
	double maxAgentEnergy;                 // energy level of the agent with the highest energy level


	CNeuralNet * representativeANN;        // average ann of all agents in this group
	CNeuralNet * sqauredRepresentativeANN; // average ann of all agents in this group
	vector<Agent> agentVector;             // vector of agents in this group
	std::set<int> collectedResources;      // Set containing unique index's of resources collected in current generation

	#ifdef _WINDOWS
		HPEN pen = NULL;                   // Pen object
	#endif 

	/**
	*  Calculate average ann based on ann of all agents in the group
	*/
	void calculateRepresentativeANN();

	/**
	*  Calculate average relatedness between all agents
	*/
	void calculateAverageRelatedness();

	/*
	* Calculate the diversity within the group
	*/
	void calculateGroupDiversity();

	/**
	*  Group constructor.
	*
	*  @param int:        ID number of this group
	*  @param int:         collective energy value for this group
	*  @param int:              red colour value
	*  @param int:              green colour value
	*  @param int:              blue colour value
	*/
	Group(int _groupId, double _energy, int _r, int _g, int _b);
    
    ALSB_SIX_MEMBER_FUNCTIONS(Group)

    /**
	*  Group constructor.
	*
	*  @param int:                    ID number of this group
	*  @param int:                     collective energy value for this group
	*  @param int:    number of resources gathered by agents in this group
	*  @param double:      average relatedness level of all agents in this group
	*  @param double:          energy value of agent with highest amount of energy
	*  @param int:                          red colour value
	*  @param int:                          green colour value
	*  @param int:                          blue colour value
	*  @param std::vector<Agent>: vector of all agents that belong to this group
	*/
	Group(int _groupId, double _energy, int _numberResourcesGathered,
		double _averageRelatedness, double _maxAgentEnergy,
		int _r, int _g, int _b, int _numberOfStartingAgents, int _numberofCooperativeAgents, double _intraGroupCooperation, double _degreeOfCooperation, double _groupDiversity, std::vector<Agent> _agentVector);
};

namespace statsOutstream
{
	/**
	*  Operator overload to allow saving data to file
	*/
	ostream& operator<<(ostream&, const Group& group);
}

namespace restartOutstream
{
	/**
	*  Operator overload to allow saving data to file
	*/
	ostream& operator<<(ostream&, const Group& group);
}

class CController
{
private:
	KdNode * root;                                                   // KdTree root node

	string snapshotFileName = "";                                    // name of file to save snapshot data to

	int modeSwitch;                                                  // allows differentiation between the experimental modes
	int snapshotStatisticCount = 0;
	int	numberAgents;
	int	numberResources;
	int numberOfActiveResources;	   	                             // Used to store the number of active resources at the start of a generation
	int numberOfGroupDeaths = 0;
	int choice = 0;
	int numberOfResourcesGathered = 0;
	int m_NumWeightsInNN;                                            // Number of weights required for the neural net
	int	ticksPerGeneration;                                          // Cycles per generation
	int generationCounter;                                           // Generation counter
	int cxClient, cyClient;                                          // Window dimensions
	int removeAgentGroup;				                             // Track if we removed an agent and its group
	int numberOfGroups = 0;

	double populationDiversity;
	double resourceDensity;                                          // resource patch information
	double numberOfPatches;
	double widthOfPatch;
	double maxDist = (CParams::WindowWidth * CParams::WindowHeight); // maximum possible squared distance from an agent to a resource node

	bool firstRun = true;                                            // bool for to determine if user needs to be queried for input on the initial run
	bool movement;                                                   // whether or not agents should move (used for debugging)
	bool firstGeneration;
	bool simulationSpeed;                                            // Toggles the speed at which the simulation runs

	CNeuralNet * populationRepresentativeANN;                        // average ann of all agents in this group
	CNeuralNet * squaredPopulationRepresentativeANN;                 // average ann of all agents in this group

    std::vector<int> inactiveResources;                              // current count of resources that have been collected, and not yet respawned
	vector<SPoint> agentVerticies;                                   // Vertex buffer for the sweeper shape's vertices
	vector<SPoint> resourceVerticies;                                // Vertex buffer for the mine shape's vertices
	vector<double> averageGenerationFitness;                         // Stores the average fitness per generation for use in graphing
	vector<double>	m_vecBestFitness;                                // Stores the best fitness per generation
	vector<Group> groupVector;                                       // Vector of groups of agents
	vector<Resource> resourcesVector;                                // Vector of resources
	vector<ResourcePatch> patchesVector;                             // Vector of resource patches

	H_WINDOW m_hwndMain;                                             // Handle to the application window


	/**
	*  Displays basic sim stats to the console
	*/
	void PlotStats();
	
	#ifdef _WINDOWS
	HPEN m_RedPen;                         // Pens we use for the stats
	HPEN m_BluePen;
	HPEN m_GreenPen;
	HPEN m_currentPen;
	HPEN m_OldPen;

	/**
	*  Given a surface to draw on this function displays basic sim stats
	*/
	void PlotStats(HDC surface);


public:
	void Render(HDC surface);

	#endif

public:
	bool end = false;                      // if simulation is finished all its runs
	int modeChoice;                        // remember mode from previous run
	int paramChoice;                       // rememebr parameter choice from previous run

	/**
	*  Operator overload to allow saving data to file
	*/
	friend ostream& operator<<(ostream&, const CController&);

	/**
	*  CController Constructor
	*
	*  @param H_WINDOW:  main window
	*  @param int:       preset parameter choice
	*/
	CController(H_WINDOW hwndMain, bool first, int modeChoice, int paramChoice);

	/**
	*  CController destructor.
	*/
	~CController();

	/**
	*  Setup the relatedness based experiment
	*
	*  @param int: remember previous choice if this is not the first run
	*/
	void relatednessExperimentSetup(int previousChoice);

	/**
	*  Setup the competition based experiment
	*
	*  @param int: remember previous choice if this is not the first run
	*/
	void competitionExperimentSetup(int previousChoice);

	/**
	*  Setup the competition based experiment
	*/
	void populationExperimentSetup();

	/**
	*  Used to write a snapshot of the system in its current state
	*/
	void writeSnapshot();

	/**
	*  Used to restart the system from a system snapshot
	*/
	void snapshotRestart();

	/**
	*  Used to reset controller variables and return the number of groups
	*/
	int setControllerVariables(ifstream * myfile);

	/**
	*  Used to reset patch variables after a snapshot reset
	*/
	void setPatchVariables(ifstream * myfile);

	/**
	*  Used to reset resource variables after a snapshot reset
	*/
	void setResourceVariables(ifstream * myfile);

	/**
	*  Used to reset resource variables after a snapshot reset
	*/
	void setGroupVariables(ifstream * myfile, int numberOfGroups);

	/**
	*  Used to getANNFromSnapshot resource variables after a snapshot reset
	*/
	CNeuralNet * getANNFromSnapshot(ifstream * myfile);

	/**
	*  Used to return a variable set agent object
	*/
	Agent setAgentVariables(ifstream * myfile);

	/**
	*  Used to initialize the Kd resources tree
	*/
	void initializeKdTree();

	/**
	*  Initializes neural networks for run
	*/
	void InitializeLearningAlgorithm(void);

	/**
	*  Sets up the translation matrices for the resources and applies the
	*  world transform to each vertex in the vertex buffer passed to this
	*  method.
	*
	*   @param vector<SPoint>&:    vertex buffer
	*   @param SVector2D:          position vector
	*/
	void WorldTransform(vector<SPoint> &VBuffer, SVector2D vPos);

	/**
	*  Main update method. Updates all the things.
	*/
	bool Update();

	/**
	*  Update agents information (position, energy etc.)
	*
	*  @param int:  index into agent vector
	*  @param int:  index into group vector
	*/
	void agentUpdate(int agentNumber, int groupNumber);

    /**
     * Updates the groupVector each tick 
     */
    void groupUpdate ();

	/**
	*  Distributes the groups energy per its agents
	*/
	void distributeGroupEnergy();

	/**
	*  Creates the resource patches and generates the resources within
	*/
	void generateResources();

	/**
	*  Generate a preset resource configuration for competition experiment
	*
	*  @param int:       total number of resource nodes to be placed in the environment
	*/
	void generatePresetResourceConfig(int numNodes);

	/**
	*  Generate random agents
	*
	*  @param vector<Agents> &: vector of agents
	*/
	void generateAgents(std::vector<Agent> & children);

	/**
	*  Creates 2 randomly initialised groups and agent objects
	*/
	void generateRandomTeams();

	/**
	*  Creates two random teams, red and blue
	*
	*  @param const int *:    number of agents
	*  @param int:            mode switch to distinguish between different experiments that use this method
	*/
	void generateRedAndBlue(const int * numAgents = nullptr, int mode = 0);

	/**
	*  Used to create a directory on both windows and unix systems
	*
	*  @param string: name of directory to be used
	*/
	void createDir(string dir);

	/**
	* Generate multiple groups of agents, each groups agents are related to eachother
	*/
	void generateRelatedTeams();

	/**
	*  Calculate the diversity for a group
	*/
	double calculateGroupDiversity();

	/**
	*  Calculate the representative ANN for the whole population
	*/
	void calculatePopulationRepresentivieANN();

	/**
	*  Calculate the diveristy of the entire population
	*/
	void calculatePopulationDiversity(std::vector<Agent> & children);

	/**
	*  Linear scaling used to create inputs to agent ANNs
	*
	*  @param double:       input value from the world
	*  @param double:       maximim possible value for this input
	*  @retval double:      linearly scaled input based on the max
	*/
	double scale(double input, double max);
	
	// rendering stuff
	bool FastRender(){ return simulationSpeed; }
	void FastRender(bool arg){ simulationSpeed = arg; }
	void FastRenderToggle() { simulationSpeed = !simulationSpeed; }

	void writeStats(int mode, std::ofstream & to_file);
};
#endif



#ifndef CPARAMS_H
#define CPARAMS_H
//------------------------------------------------------------------------
//
//	Name: CParams.h
//
//  Author: Mat Buckland 2002
//
//  Desc: Class to hold all the parameters used in this project. The values
//        are loaded in from an ini file when an instance of the class is
//        created.
//       
//
//------------------------------------------------------------------------
#include <fstream>
#ifdef _WINDOWS
#include <windows.h>
#endif

#ifndef _WINDOWS
typedef long int LARGE_INTEGER;
typedef long long LONGLONG;
#define QueryPerformanceCounter(A)
#define QueryPerformanceFrequency(A)
#define MessageBox(A, B, C, D)
#endif
#include <sstream>
#include <string>

#include "utils.h"
#include "SVector2D.h"

using namespace std;

class CParams
{

public:

	// General
	static double dPi;
	static double dHalfPi;
	static double dTwoPi;

	static int simulationRunId;
	static int    WindowWidth;
	static int    WindowHeight;
	static int    iFramesPerSecond;
	static int	endGenerationNo;
	static std::string resetFileName;

	// Neural Network
	static int    iNumInputs;
	static int    iNumHidden;
	static int    iNeuronsPerHiddenLayer;
	static int    iNumOutputs;
	static double dActivationResponse;   // For tweeking the sigmoid function
	static double dBias;				   // Bias value

	// Agent Related
	static double dEnergyCostPerTick;
	static double dMaxTurnRate;		   // Limits how fast the Agent can turn
	static double dMaxSpeed;
	static double dSweeperScale;		   // For controlling the size
	static double dStartEnergy;		   // The amount of energy a Agent may start its life with
	static double fovRadius;

	// Energy Stuff
	static int energyGainedPerResource;
	static double groupStartingEnergy;
	static double willMove;

	// Controller Related
	static int iNumSweepers;
	static int iNumMines;
	static int iNumTicks;			  // Number of time steps we allow for each generation to live

	// Resources
	static double dMineScale;
	static double density;
	static double numPatches;
	static double patchWidth;
	static double resourceGrowthRate;
	static vector<SVector2D> defaultResourcePositions;
	static vector<SVector2D> setResourcePositions;

	// Genetic Algorithm
	static double dCrossoverRate;
	static double dMutationRate;
	static double dMaxPerturbation;	  // The maximum amount the ga may mutate each weight by
	static int    iNumElite;
	static int    iNumCopiesElite;
	static double relatedThreshold;
	static double geneRelatedThreshold;
	static int selectionDistance;
	static int localSelection;
	static double tournamentSize;
	static int numberOfChildren_perTournamentPair;
	static double selectionPressure;
	static int isTournamentSelection;

	// stats
	static string sOutFilename;
	static string sOutFilename2;

	// population experiment 
	static int iNumAgents[2];

	//ctor
	CParams()
	{
		if (!LoadInParameters( std::string (
#ifndef _WINDOWS
			"../"
#endif
			"params.ini"
			)) )
		{
			MessageBox(NULL, "Cannot find ini file!", "Error", 0);
		}
	}

	/**
	* Operator overload to allow saving data to file
	*/
	friend ostream& operator<<(ostream&, const CParams&);

	bool LoadInParameters(std::string szFileName);
	
	static void writeResetFileId(string fileName);

	static void readInParameters(vector<string> tokens);

	static string returnParamsString();
};
#endif

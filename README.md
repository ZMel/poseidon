### Honours Project ALSB (2015) - Jacob Clarkson, Paul Wanjohi, Zac Melnick

### Information
This repository contains the full source code for our honours project, an Agent-Based System designed to test three key hypotheses predicted by the Nested Tug-of-War model for the evolution of cooperation.

For information about the project and experimental findings please see http://poseidonhonours.github.io/index.html

The source code is well documented. For a useful, searchable, online version of the documentation, pease see http://poseidonhonours.github.io/documentation.html
#### DEPLOYMENT NOTES

This framework is written in C++, and was mainly developed in Microsft Visual Studio. Builds for both Windows and Linux are possible.

1. To deploy the software on Windows as a WIN32 Application open ``Framework.sln'' file with the Visual
Studio 2013 application and run the program with the Windows Debugger.

2. To deploy on Linux run the following commands:

    cd linux-build 
    cp ../params.ini ./
    cmake ../
    make
    ./alsb 

the above commands will auto generate some CMake files. To create multiple builds use different 
folders and repeat the last 3 commands within the different folders.

#### Work Breakdown:

### Zac:
-Adaptation of smart sweepers framework to introduce resources, agents, groups, EA and NE

-Creation of basic ANN.

-Introduced death functionality for both groups (and group colour representation) and agents.

-Adaption of ANN to improve input and output functionality.

-ANN input scaling (to allow agents not to move and be selfish if they cannot see a resource).

-Addition of agent field of view defining how far they can see.

-Snapshot functionality allowing snapshots of system at any point and to restart the system from a snapshot.

-Initial EA crossover, mutate and group methods algorithm allowing basic evolution of agents.

-Finalization of EA algorithm for relatedness based experiments.

-KdNode implementation to allow fast Log N resource location by agents.

-Experiment specific statistic analysis.

-Compare agents based on hamming distance based relatedness function.

-Calculation of both group and population diversity based on agent relatedness.

### Jacob
-Design, implementation, and optimization of ANN agent controllers.

-Adaptation of feedforward, mutation, crossover, and relatedness methods to work with new ANN topology.

-Rank based selection method for Evolutionary Algorithm (Linear Rank Based Selection).

-Experimental run automation.

-Calculations for parameter tuning.

-Experiments to determine general baseline parameters.

-Alternative group generation system (maintains fixed number of groups with fixed population size).

-Additional constructors for snapshot restart system.

-Experiment specific resource configuration system (design and testing of different resource environments with different patchiness - determined with a density metric)

-Experiment specific data collection system.

-Experiment specific statistical analysis.

-Code cleanup.

-Documentation.


### Paul
-Adaptation of windows framework to work on linux.

-Addition of special member functions to all classes.

-Improved memory management.

-Removed compile time warning.

-Adaptation of flattened ANN to introduce pointers and improve speed.

-Improvement of tournament selection and addition of rank based selection.

-Improved group and agent energy update.

-Experiment specific statistic analysis.

-Improved Utilities class containing common mathematical methods and random number generation.
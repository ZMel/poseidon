#pragma once

//!  Evolutionary Algorithm Class
/*!
     All methods involved in the evolutionary algorithm. Parent selection operator (rank based), fitness functions, and genetic operators.

	 This is Conventional Neuro-Evolution (CNE) that operates only on the weights of the agent ANNs. Parent selection is rank based. Fitness is
	 based on the energy level of an agent, mutation is done via random perturbations of connection weight values, crossover is single-point.
*/

#include "CNeuralNet.h"
#include "Agent.h"
#include "CController.h"
#include <vector>
#include <assert.h>
#include <algorithm>
#include <random>
#include <unordered_map>
#include <set>
#include <cfloat>

class EA
{
private:

public:
	/**
	*  Compare the fitness of two agents.
	*
	*  @param Agent*:         first agent
	*  @param Agent*:         second agent
	*  @retval bool:          TRUE if first agent fitter than second, FALSE otherwise
	*/
	static bool fitnessComparison(Agent * parent1, Agent * parent2);


	/**
	*  Mutation method.
	*
	*  @param CNeuralNet*:  neural network to be mutated
	*  @param double:       probability of single gene being mutated
	*  @param double:       maximum range by which a gene can be mutated
	*/
	static void mutate(CNeuralNet & genotypeA, double mutationChance, double perturbationAmt);


	/**
	*  Crossover method.
	*
	*  @param const CNeuralNet&:      first ANN
	*  @param const  CNeuralNet&:     second ANN
	*  @retval CNeuralNet*:           ANN resulting from crossover operation
	*/
	static void crossover(CNeuralNet * genotypeA, CNeuralNet * genotypeB, CNeuralNet & out);

	/**
	*  Generate new groups based on agent information.
	*
	*  @param vector<Agent>&:           vector of agents
	*  @param vector<struct Group>&:    vector of groups
	*/
	static void groupGeneration(std::vector<Agent> & agents, std::vector<struct Group>  & groupVector);

	/**
	*  Evolutionary algorithm to be used where two groups of equal sized are required.
	*
	*  @param Group &:         group of agents to undergo evolution
	*  @param double:          amount of energy to be given to each new agent in the next generation
	*  @param double:          shared energy collected by agents in the last generation
	*/
	static void withinGroupSelection(Group & group, int numberOfChildren, int modeChoice);

	/**
	*  Rank based selection. Given the old population, creates a new population of children.
	*
	*  @param std::vector<Agent>:     vector of agents from previous generation
	*  @param std::vector<Agent>:     vector to be populted with new agents
	*  @param int:                    number of children to be created by crossover
	*
	*/
	static void rankBased(std::vector<Agent> & oldPop, std::vector<Agent> & children, int numberOfChildren);

	/**
	*   Ranked based selection, including multiple children and relatedness measures
	*
	*   @param std::vector<Agent>:     vector to be populted with new agents
	*   @param int:                    number of children to be created by crossover
	*/
	static void rankBased(std::vector<Group> & groupVector, std::vector<Agent> & children, int numberOfChildren);

};


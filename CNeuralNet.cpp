#include "CNeuralNet.h"
#include "utils.h"
#include <random>

CNeuralNet::CNeuralNet ()
    : _iInput (0), _iHidden (0) , _iNumHidden(0) , _iOutput(0)
{
    _network = nullptr;
}

CNeuralNet::CNeuralNet(uint inputLayerSize, uint hiddenLayerSize, uint numHiddenLayers, uint outputLayerSize)
    : _iInput (inputLayerSize), _iHidden(hiddenLayerSize), _iNumHidden(numHiddenLayers), _iOutput(outputLayerSize)
{
    initWeights( );
}
CNeuralNet::CNeuralNet( const CNeuralNet & other ) :
    _iInput(other._iInput), 
    _iHidden(other._iHidden),  
    _iNumHidden(other._iNumHidden),
    _iOutput(other._iOutput)
{
    initWeights (other._network);
}

CNeuralNet::CNeuralNet(CNeuralNet && other )
    : _iInput (other._iInput), _iHidden(other._iHidden), _iNumHidden(other._iNumHidden),
    _iOutput(other._iOutput)
{
    if ( other._network )
    {
        this->_network = other._network;
        other._network = nullptr;
    }
    const_cast<uint&>(other._iInput) = 0;
    const_cast<uint&>(other._iOutput) = 0;
    const_cast<uint&>(other._iNumHidden) = 0;
    const_cast<uint&>(other._iHidden) = 0;
}

CNeuralNet & CNeuralNet::operator=( const CNeuralNet & other )
{
    if ( this != &other ) 
    {
        const_cast<uint&>(this->_iInput) = other._iInput; 
        const_cast<uint&>(this->_iOutput) = other._iOutput;
        const_cast<uint&>(this->_iHidden) = other._iHidden;
        const_cast<uint&>(this->_iNumHidden)  = other._iNumHidden;
        
        if ( this->_network)
        {
            delete [] this->_network;
        }
        initWeights(other._network);
    }
    return *this;
}

CNeuralNet & CNeuralNet::operator=( CNeuralNet && other )
{
    if ( this != &other ) 
    {
        const_cast<uint&>(this->_iInput) = other._iInput; 
        const_cast<uint&>(this->_iOutput) = other._iOutput;
        const_cast<uint&>(this->_iHidden) = other._iHidden;
        const_cast<uint&>(this->_iNumHidden)  = other._iNumHidden;
        
        const_cast<uint&>(other._iInput) = 0;
        const_cast<uint&>(other._iOutput) = 0;
        const_cast<uint&>(other._iNumHidden) = 0;
        const_cast<uint&>(other._iHidden) = 0;

        if ( other._network ) 
        {
            if ( this->_network ) 
            {
                delete [] this->_network;
            }
            
            this->_network = other._network ;
            other._network = nullptr;
        }
    }
    return *this;
}

CNeuralNet::~CNeuralNet()
{
    if ( this->_network )
    {
        delete[] this->_network;
    }
}

uint CNeuralNet::size () const 
{
    if (this->_iNumHidden) 
    {
        return 
            this->_iInput * this->_iHidden + 
            this->_iHidden * this->_iHidden * (this->_iNumHidden - 1) + 
            this->_iOutput * this->_iHidden;
    }
    else 
    {
        return this->_iInput * this->_iOutput;
    }
}

void CNeuralNet::feedForward( double * inputs, double *& outputs )
{
    if ( !_network ) 
    {
        return;
    }
    uint max_dim = (_iInput > _iOutput ? 
                (( _iHidden > _iInput ? _iHidden : _iInput )) : 
                (( _iHidden > _iOutput ? _iHidden : _iOutput ))) ;

    double * buffer = new double [ max_dim ];
    double * layer_input = inputs; 
    
    uint head = 0 ;

    // input to hidden 
    for ( uint hidden = 0; hidden < _iHidden ; hidden++)
    {
        buffer[hidden] = 0.0;
        for ( uint input = 0; input < _iInput ; head ++, input ++ ) 
        {
            buffer[hidden] += _network[head] * layer_input[input] ;  
        }
        sigmoid ( buffer[hidden] );
    }
    layer_input = buffer; 
    buffer = new double [ max_dim ];
    
    // hidden to hidden 
    if ( _iNumHidden )
    {
        for ( uint layers = 0; layers < _iNumHidden - 1 ; layers ++ ) 
        {
            for ( uint next = 0 ; next < _iHidden ; next++ )
            {   
                buffer[next] = 0.0;
                for ( uint prev = 0 ; prev < _iHidden ; prev++,head++ ) 
                {
                    buffer[next] += _network[head] * layer_input[prev];
                }
                sigmoid ( buffer[next] );
            }
            delete [] layer_input;
            layer_input = buffer;
            buffer = new double[ max_dim ] ; 
        }
    }

    for ( uint output = 0 ; output < _iOutput ; output++ )
    {
        buffer[output] = 0.0;
        for ( uint hidden = 0; hidden < _iHidden ; hidden++, head++ )
        {
            buffer[output] += _network[head] * layer_input[hidden];
        }
        sigmoid ( buffer[output] );
    }

    if ( outputs ) 
    {
        delete [] outputs ;
    }    
    delete [] layer_input;
    outputs = buffer;
    buffer = nullptr; 
}

void CNeuralNet::sigmoid ( double & netinput ) 
{
    netinput = ( 1.0 / (1.0 + exp (- (SIGMOID_ACTIVATION * netinput))) ) ;  
}

void CNeuralNet::initWeights(double * weights)
{
	double sum = 0;

    uint size = this->size () ;
    this->_network = new double[size];
    for ( uint i = 0; i < size; i++ )
    {
        if ( weights )
        {
            this->_network[i] = weights[i];
        }
        else
        {
			this->_network[i] = Utilities::getCauchyDouble(1.0, -1.0);
        }
    }
}

CNeuralNet::CNeuralNet(uint inputLayerSize, uint hiddenLayerSize, uint outputLayerSize) :
    _iInput(inputLayerSize), _iHidden(hiddenLayerSize), _iNumHidden(1), _iOutput(outputLayerSize)
{
    initWeights() ;
}

CNeuralNet::CNeuralNet(int inputLayerSize, int hiddenLayerSize, int outputLayerSize, double * _ann) :
    _iInput(inputLayerSize), _iHidden(hiddenLayerSize), _iNumHidden(1), _iOutput(outputLayerSize)
{
    initWeights(_ann);
}

uint CNeuralNet::classify( double * input)
{
    double * outputs = nullptr;
    feedForward(input, outputs);

    if (outputs[0] >= 0.5 ) 
    {
        delete [] outputs;
        return 0;
    }
    else 
    {
        delete [] outputs ;
        return 1;
    }
}

bool CNeuralNet::areRelated(CNeuralNet * otherAgent)
{
	if (!otherAgent)
	{
		return false;
	}

	double relatedness = calculateRelatedness(otherAgent);

	return (relatedness >= CParams::relatedThreshold);
}

double CNeuralNet::calculateRelatedness(CNeuralNet * otherAgent)
{
	double relatednessPercentage = 0;
	double numSame = 0;
    
    if ( this->size() > otherAgent->size()) 
    {
        return -1.0;
    }

	for (uint i = 0; i < this->size(); ++i)
	{
		if (std::abs(_network[i] - otherAgent->_network[i]) <= CParams::geneRelatedThreshold)
		{
			numSame+=1.0;
		}
	}
	relatednessPercentage = numSame / (double)this->size();

	return relatednessPercentage;
}

ostream& operator<<(ostream& out, const CNeuralNet & neuralNet)
{
	out << "ANN Variables_"
		<< neuralNet._iInput
		<< "," << neuralNet._iHidden
		<< "," << neuralNet._iOutput
		<< "," << neuralNet.size()
    	<< std::endl;

	out << "ANN Weight Values_";
	for (size_t i = 0; i < neuralNet.size(); i++)
	{
		if (i != 0)
		{
			out << ",";
		}
			
		out << neuralNet._network[i];
	}

	return out;
}


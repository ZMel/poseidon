#include "CController.h"

/**
*  Updates all agent values per tick (position, energy level, resource collection count, live/dead status).
*/
void CController::agentUpdate(int agentNumber, int groupNumber)
{
	Agent * agent = &(groupVector[groupNumber].agentVector[agentNumber]);

	SVector2D closestResourcePosition;
	if (agent->closestResourceIndex != -1)
	{
		// Vector to closest resource
		closestResourcePosition = resourcesVector[agent->closestResourceIndex].getPosition();
	}
	else
	{
		//If no resource found in FOV move randomly
		closestResourcePosition.x = Utilities::getInteger(800);
		closestResourcePosition.y = Utilities::getInteger(800);

		// Set the closest resource in the agents class
		agent->vClosestObject = closestResourcePosition;
	}

	// First input: Normalized distance to closest resource to this agent
	SVector2D pos = agent->getPosition(); // position of this agent
	double distToClosest = QuickVec2DLength(closestResourcePosition - pos);
	// make agents think that resources outside their FOV are far away
	if (agent->closestResourceIndex == -1)
	{
		distToClosest += CParams::fovRadius + DBL_EPSILON;
	}

	uint idx = 0;
	double * inputs = new double[CParams::iNumInputs];
	inputs[idx++] = ((-scale(distToClosest, CParams::fovRadius)) / 2.0);

	// Second input: Normalized agent energy
	double agentEnergy = agent->getEnergy();
	double agentStartEnergy = agent->getStartingEnergy();
	inputs[idx++] = (-scale(agentEnergy, agentStartEnergy));

	// Update ANN and get feedback
	// If the agent favours resource collection over saving energy
	if (agent->getBrain()->classify(inputs) != 1)
	{
        agent->decreaseEnergy();
		// Turn towards the resource point
		SPoint pt(closestResourcePosition.x, closestResourcePosition.y);
		agent->turn(pt, 1, true);
		agent->setSpeed(CParams::dMaxSpeed);
	}
	else
	{
		// If the agent doesn't want to expend energy, stop moving
		agent->setSpeed(0);
	}

	delete[] inputs;
	inputs = nullptr;

	if (agent->getEnergy() > groupVector[groupNumber].maxAgentEnergy)
	{
		groupVector[groupNumber].maxAgentEnergy = agent->getEnergy();
	}
}
